export namespace LocalStorage {
  export const ServerUrl = 'server:url';

  export const App = {
    Id: 'app:id',
    Build: 'app:build',
    Version: 'app:version',
    Environment: 'app:environment',
  };

  export const FIRST_LIKE_FOR_JOB = '@FIRST_LIKE_FOR_JOB';
  export const REDIRECT_AFTER_LOGIN_TO_JOB = '@REDIRECT_AFTER_LOGIN_TO_JOB';
  export const DONT_SHOW_POPUP_ON_DECLINE_STORAGE_ITEM = 'DONT_SHOW_POPUP_ON_DECLINE_STORAGE_ITEM';
  export const REDIRECT_AFTER_LOGIN_TO_SWOP = '@REDIRECT_AFTER_LOGIN_TO_SWOP';
  export const OPEN_CHAT_BOT = 'openChatbot';
  export const REDIRECT_TO_SOFIA_AFTER_LOGIN_OR_REGISTRATION = 'redirect-to-sofia-after-login-or-registration';
  export const CHAT_BOT_CHAT_SID_STORAGE_ITEM = 'CHAT_BOT_CHAT_SID_STORAGE_ITEM';
  export const SEARCH = 'SEARCH';
  export const TRANSLATION_DATA = 'TRANSLATION_DATA';
  export const TRANSLATION_VERSION = 'TRANSLATION_VERSION';
  export const LANGUAGE = 'language';
  export const USER_ID = 'USER_ID';
  export const USER_DOMAIN = 'USER_domain';
  export const USER_TICKET = 'USER_ticket';
  export const REFRESH_TOKEN = 'refreshToken';
  export const EXPIRES_AT = 'expires_at';
  export const INVITE_CODE = 'INVITE_CODE';
  export const FIRST_LOGIN = 'FIRST_LOGIN';
  export const TIMEOUT_RESEND = 'TIMEOUT_RESEND';
  export const SERVER_URL = 'SERVER_url';
  export const APP_BUILD = 'APP_build';
  export const USER_REGISTRATION_ID = 'USER_registrationId';
  export const USER_DEVICE_PREVIOUSLY_REGISTERED = 'USER_devicePreviouslyRegistered';
  export const USER_OLD_REGISTRATIONS_IDS = 'USER_oldRegistrationIds';
  export const FIRST_TIME_RUN_LOCAL_STORAGE_ITEM = 'FIRST_TIME_RUN_LOCAL_STORAGE_ITEM';
  export const REDIRECT_TO_VACANCY = 'REDIRECT_TO_VACANCY';
  export const REDIRECT_TO_CHAT = 'REDIRECT_TO_CHAT';
  export const AFTER_LOGIN_LIKE_JOB = 'AFTER_LOGIN_LIKE_JOB';
  export const TranslationsData = 'TranslationsData';
  export const TranslationsVersion = 'TranslationsVersion';
  export const Language = 'Language';
  export const REGISTRATION_USER_NAME = 'RegistrationInfo';
}
