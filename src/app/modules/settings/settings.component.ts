import { take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { WebViewService } from './../../services/webview.service';
import { AuthService } from './../../services/auth.service';
import { PushNotificationService } from './../../services/push-notification.service';
import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output, ChangeDetectorRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslationsService } from 'app/services/translations.service';
import { PopoverService } from 'app/services/popover.service';

@Component({
  selector: 'app-settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingsComponent implements OnInit {
  private currentLanguage: string;
  public languageName: string;
  @Output()
  public onChangeLanguage: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private modalController: ModalController,
    private translationService: TranslationsService,
    private pushNotificationService: PushNotificationService,
    private authService: AuthService,
    private webViewService: WebViewService,
    private translationsService: TranslationsService,
    private popoverService: PopoverService,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.currentLanguage = this.translationService.language;
    // this.changeDetectorRef.markForCheck();
    this.languageName = this.translationService._(`language.${this.currentLanguage}`);
  }

  public close(): void {
    this.modalController.dismiss();
  }

  public logout(): void {
    this.pushNotificationService.unregister();
    this.authService
      .logout()
      .pipe(take(1))
      .subscribe(() => {
        console.log('welcome');
        this.router.navigate(['']);
      });
    this.modalController.dismiss();
  }

  openPrivacyPolicy(): void {
    this.webViewService.openLink('https://www.gighouse.be/nl-be/freelancers/bedrijf/privacy-beleid').subscribe();
  }

  changeLanguage(): void {
    const languages = this.translationsService.localization.languages.map((language) => ({
      key: language.code,
      name: this.translationsService._(`language.${language.code}`),
    }));
    this.popoverService.showSelectorPopover({ options: languages, title: '' }).then((result) => {
      if (result) {
        this.changeDetectorRef.markForCheck();
        const { data } = result;
        this.translationsService.setLanguage(data.key);
        this.currentLanguage = data.key;
        this.onChangeLanguage.emit();
        this.languageName = this.translationService._(`language.${this.currentLanguage}`);
      }
    });
  }
}
