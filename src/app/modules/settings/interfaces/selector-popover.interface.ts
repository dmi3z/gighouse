export interface SelectorPopoverOptions {
  title?: string;
  backdropDismiss?: boolean;
  options: {
    key?: string;
    name: string;
  }[];
}
