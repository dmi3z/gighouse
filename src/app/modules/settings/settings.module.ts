import { SelectorPopoverComponent } from './selector-popover/selector-popover.component';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SettingsComponent } from './settings.component';

@NgModule({
  declarations: [SettingsComponent, SelectorPopoverComponent],
  imports: [CommonModule, IonicModule, TranslateModule],
  exports: [SettingsComponent],
})
export class SettingsModule {}
