import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { SelectorPopoverOptions } from '../interfaces/selector-popover.interface';

@Component({
  selector: 'app-selector-popover',
  templateUrl: 'selector-popover.component.html',
  styleUrls: ['./selector-popover.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectorPopoverComponent {
  @Input() public data: SelectorPopoverOptions;

  constructor(private popoverController: PopoverController) {}

  public saveAnswer(value: any): void {
    this.popoverController.dismiss(value);
  }
}
