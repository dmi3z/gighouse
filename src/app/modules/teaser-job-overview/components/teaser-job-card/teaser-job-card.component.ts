import { ModalController } from '@ionic/angular';
import { ChatService } from './../../../../services/chat.service';
import { map } from 'rxjs/operators';
import { MessagesListComponent } from './../../../chat-list/components/message-list/messages-list.component';
import { LoadingRepository } from './../../../../services/loading.repository';
import { StorageService } from './../../../../services/storage.service';
import { ConversationBody } from './../../../chat-list/interfaces/chat.interface';
import { Job } from './../../../../interfaces/models/job';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-teaser-job-card',
  templateUrl: 'teaser-job-card.component.html',
  styleUrls: ['teaser-job-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeaserJobCardComponent implements OnInit {
  @Input()
  public job: Job;

  public name: string;
  public text1: string;
  public text2: string;
  public text3: string;
  public location: string;
  public daysAWeek: number;

  constructor(
    private loadingRepository: LoadingRepository,
    private storageService: StorageService,
    private chatService: ChatService,
    private modalController: ModalController
  ) {}

  public ngOnInit(): void {
    this.name = this.job.getName();
    this.text1 = this.job.getTeaserText1();
    this.text2 = this.job.getTeaserText2();
    this.text3 = this.job.getTeaserText3();
    this.location = this.job.getLocation();
    this.daysAWeek = this.job.getDaysAWeek();
  }

  public openChat(event: Event): void {
    event.stopPropagation();

    const conversationBody: ConversationBody = {
      contactName: '',
      customerId: null,
      projectId: this.job.getId(),
    };

    const freelancerId = this.storageService.getFreelancerId();
    this.loadingRepository.presentLoading();
    this.chatService
      .getConversation(freelancerId, conversationBody)
      .pipe(map((data) => data.conversationSid))
      .subscribe(async (conversationSid) => {
        const modal = await this.modalController.create({
          component: MessagesListComponent,
          componentProps: { conversationBody, conversationSid },
        });
        await modal.present();
      });
  }
}
