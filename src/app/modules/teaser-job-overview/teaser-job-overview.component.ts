import { tap } from 'rxjs/operators';
import { LoadingRepository } from './../../services/loading.repository';
import { Job } from './../../interfaces/models/job';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { StorageService } from 'app/services/storage.service';
import { JobService } from 'app/services/job.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teaser-job-overview',
  templateUrl: 'teaser-job-overview.component.html',
  styleUrls: ['teaser-job-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeaserJobOverviewComponent {
  public teaserJobs$: Observable<Job[]>;

  constructor(
    private storageService: StorageService,
    private jobService: JobService,
    private loadingRepository: LoadingRepository,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.fetchTeaserJobs();
  }

  private fetchTeaserJobs(): void {
    const freelancerId = this.storageService.getFreelancerId();
    this.loadingRepository.presentLoading();
    this.teaserJobs$ = this.jobService
      .getTeaserJobsForFreelancer(freelancerId)
      .pipe(tap(() => this.loadingRepository.stopLoading()));
  }

  public completeProfile() {
    this.router.navigate(['tabs']);
  }
}
