import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TeaserJobOverviewComponent } from './teaser-job-overview.component';
import { PipesModule } from 'app/pipes/pipes.module';
import { TeaserJobCardComponent } from './components/teaser-job-card/teaser-job-card.component';
import { JobService } from 'app/services/job.service';

@NgModule({
  declarations: [TeaserJobOverviewComponent, TeaserJobCardComponent],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    PipesModule,
    RouterModule.forChild([
      {
        path: '',
        component: TeaserJobOverviewComponent,
      },
    ]),
  ],
  providers: [JobService],
})
export class TeaserJobOverviewModule {}
