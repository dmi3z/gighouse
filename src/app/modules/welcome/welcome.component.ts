import { RecoverOnboardingFlowService } from './../../services/recover-onboarding-flow.service';
import { take, flatMap, map } from 'rxjs/operators';
import { AuthService } from './../../services/auth.service';
import { ChangeDetectionStrategy, Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { AccountStateService } from '../../services/account-state.service';
import { of } from 'rxjs';
import { AccountState } from '../../interfaces/models/account-state';
import { Plugins } from '@capacitor/core';
const { SplashScreen } = Plugins;

@Component({
  selector: 'app-welcome',
  templateUrl: 'welcome.component.html',
  styleUrls: ['welcome.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WelcomeComponent {
  public features: Array<{ image: string; text: string }> = [
    {
      image: 'icon-fast-free.svg',
      text: 'landing-page.first.usp',
    },
    {
      image: 'icon-time.svg',
      text: 'landing-page.second.usp',
    },
    {
      image: 'icon-puzzle.svg',
      text: 'landing-page.third.usp',
    },
  ];

  constructor(
    private authService: AuthService,
    private accountStateService: AccountStateService,
    private onboardFlow: RecoverOnboardingFlowService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ionViewDidEnter(): void {
    SplashScreen.hide();
  }

  public openLoginPage(authType: string) {
    this.authService
      .login(false, { auth_type: authType })
      .pipe(
        take(1),
        flatMap((succeeded: boolean) => {
          if (succeeded) {
            return this.accountStateService.getState();
          } else {
            return of(null);
          }
        })
      )
      .subscribe((state: AccountState) => {
        if (state !== null) {
          this.changeDetectorRef.markForCheck();
          this.onboardFlow.navigateToCorrectScreenInRegistrationFlow(state);
        }
      });
  }
}
