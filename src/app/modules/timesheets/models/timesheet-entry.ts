export class TimesheetEntry {
  private readonly id: string;
  private readonly clientName: string;
  private readonly rate: number;
  private readonly rateId: string;
  private readonly projectName: string;
  private readonly projectId: string;
  private readonly editable: boolean;
  private readonly duration: number;
  private readonly freelancerComment: string;

  constructor(id: string,
              clientName: string, rate: number,
              rateId: string,
              projectName: string,
              projectId: string,
              editable: boolean,
              duration: number,
              freelancerComment: string) {
    this.id = id;
    this.clientName = clientName;
    this.rate = rate;
    this.rateId = rateId;
    this.projectName = projectName;
    this.projectId = projectId;
    this.editable = editable;
    this.duration = duration;
    this.freelancerComment = freelancerComment;
  }

  getId(): string {
    return this.id;
  }

  getClientName(): string {
    return this.clientName;
  }

  getRate(): number {
    return this.rate;
  }

  getRateId(): string {
    return this.rateId;
  }

  getProjectName(): string {
    return this.projectName;
  }

  getProjectId(): string {
    return this.projectId;
  }

  isEditable(): boolean {
    return this.editable;
  }

  getDuration(): number {
    return this.duration;
  }

  getFreelancerComment(): string {
    return this.freelancerComment;
  }
}

export interface TimesheetEntryParams {
  id: string;
  clientName: string;
  rate: number;
  rateId: string;
  projectName: string;
  projectId: string;
  editable: boolean;
  duration: number;
  freelancerComment: string;
}
