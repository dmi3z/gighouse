import {BookableDay, BookableDayParams} from '@app/shared/models/bookable-day';

export class Timesheet {
  private readonly _startDate: Date;
  private readonly _endDate: Date;
  private readonly _bookableDays: BookableDay[];
  private readonly _id: string;
  private readonly _status: string;

  constructor(startDate: Date, endDate: Date, bookableDays: BookableDay[], id: string, status: string) {
    this._startDate = startDate;
    this._endDate = endDate;
    this._bookableDays = bookableDays;
    this._id = id;
    this._status = status;
  }

  getStartDate(): Date {
    return this._startDate;
  }

  getEndDate(): Date {
    return this._endDate;
  }

  getBookableDays(): BookableDay[] {
    return this._bookableDays;
  }

  getId(): string {
    return this._id;
  }

  getStatus(): string {
    return this._status;
  }
}

export interface TimesheetParams {
  startDate: Date;
  endDate: Date;
  bookableDays: BookableDayParams[];
  id: string;
  status: string;
}
