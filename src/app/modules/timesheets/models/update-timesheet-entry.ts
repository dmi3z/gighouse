export class UpdateTimesheetEntryRequest {
  private readonly id: string;
  private readonly rateId: string;
  private readonly projectId: string;
  private readonly duration: number;
  private readonly freelancerComment: string;

  constructor(id: string, rateId: string, projectId: string, duration: number, freelancerComment: string) {
    this.id = id;
    this.rateId = rateId;
    this.projectId = projectId;
    this.duration = duration;
    this.freelancerComment = freelancerComment;
  }
}
