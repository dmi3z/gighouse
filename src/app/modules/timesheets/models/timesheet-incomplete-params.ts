export class TimesheetIncompleteParams {
  private readonly foreground: boolean;

  constructor(foreground: boolean) {
    this.foreground = foreground;
  }

  isOnForeground(): boolean {
    return this.foreground;
  }

  isOnBackground(): boolean {
    return !this.foreground;
  }
}
