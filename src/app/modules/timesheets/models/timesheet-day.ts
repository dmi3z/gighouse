import {TimesheetEntry, TimesheetEntryParams} from '../models/timesheet-entry';

export class TimesheetDay {
  private readonly date: Date;
  private readonly timesheetEntries: TimesheetEntry[];

  constructor(date: Date, timesheetEntries: TimesheetEntry[]) {
    this.date = date;
    this.timesheetEntries = timesheetEntries;
  }

  getDate(): Date {
    return this.date;
  }

  getTimesheetEntries() {
    return this.timesheetEntries;
  }
}

export interface TimesheetDayParams {
  date: Date;
  timesheetEntries: TimesheetEntryParams[];
}
