export enum TimesheetStatus {
  draft = 'Draft',
  submitted = 'Submitted',
  approved = 'Approved',
  rejected = 'Rejected',
}
