import { EditTimesheetComponent } from './../edit-timesheet/edit-timesheet.component';
import { ModalController } from '@ionic/angular';
import { EventsService } from './../../../services/events.service';
import { GighouseEvent } from './../../../interfaces/models/gighouse-event';
import { LoadingRepository } from './../../../services/loading.repository';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ChangeDetectorRef,
} from '@angular/core';
import { StorageService } from 'app/services/storage.service';
import { ToastService } from 'app/services/toast.service';
import { TimesheetSingle } from '../interfaces/timesheet.interface';
import { TimesheetStatus } from '../models/timesheet-status';
import { TimesheetService } from '../services/timesheet.service';

@Component({
  selector: 'app-timesheet',
  templateUrl: 'timesheet.component.html',
  styleUrls: ['timesheet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimesheetComponent implements OnInit {
  @Input()
  public timesheet: TimesheetSingle;
  @Output()
  public openView = new EventEmitter<string>();
  @Output()
  public sendSubmit = new EventEmitter<string>();
  public isDisabled: boolean;
  public normalizeDate: string;
  public submitButtonText: string;

  constructor(
    private storageService: StorageService,
    private timesheetService: TimesheetService,
    private toastService: ToastService,
    private loading: LoadingRepository,
    private events: EventsService,
    private modalController: ModalController,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.updateStatus();
  }

  public updateStatus(): void {
    this.changeDetectorRef.markForCheck();
    if (this.timesheet.status === TimesheetStatus.approved || this.timesheet.status === TimesheetStatus.submitted) {
      this.isDisabled = true;
      this.submitButtonText = `timesheet.button.${this.timesheet.status}`;
    } else {
      this.isDisabled = false;
      this.submitButtonText = 'timesheet.button.submit';
    }
  }

  public submitTimesheet() {
    this.loading.presentLoading();
    this.isDisabled = true;
    this.timesheetService.submitTimesheet(this.storageService.getFreelancerId(), [this.timesheet.id]).subscribe(
      () => {
        this.events.publish(GighouseEvent.timesheetUpdated);
        this.toastService.presentSuccessToast();
        this.timesheet.status = TimesheetStatus.submitted;
        this.sendSubmit.next(this.timesheet.id);
        this.updateStatus();
        this.changeDetectorRef.markForCheck();
        this.loading.stopLoading();
      },
      () => {
        this.isDisabled = false;
        this.changeDetectorRef.markForCheck();
        this.loading.stopLoading();
      }
    );
  }

  public openViewPage(): void {
    this.openView.next(this.timesheet.id);
  }

  public async openEditPage(): Promise<void> {
    const modal = await this.modalController.create({
      component: EditTimesheetComponent,
      componentProps: { timesheet: this.timesheet },
    });
    modal.present();
  }
}
