import { ViewTimesheetComponent } from './view-timesheet/view-timesheet.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TimesheetsComponent } from './timesheets.component';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { EditTimesheetComponent } from './edit-timesheet/edit-timesheet.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TimesheetService } from './services/timesheet.service';
import { FormatDatePipe } from 'app/pipes/format-date.pipe';

const routes: Routes = [
  { path: '', component: TimesheetsComponent },
  { path: 'edit-timesheet', component: EditTimesheetComponent },
  { path: 'view-timesheet', component: ViewTimesheetComponent },
];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    TimesheetsComponent,
    TimesheetComponent,
    EditTimesheetComponent,
    FormatDatePipe,
    ViewTimesheetComponent,
  ],
  providers: [TimesheetService],
})
export class TimesheetsModule {}
