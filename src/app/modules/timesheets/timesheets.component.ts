import { BackAlertService } from './../../services/back-alert.service';
import { ViewTimesheetComponent } from './view-timesheet/view-timesheet.component';
import { LoadingRepository } from './../../services/loading.repository';
import { SettingsComponent } from './../settings/settings.component';
import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { StorageService } from 'app/services/storage.service';
import { Observable } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { TimesheetParams, Timesheets, TimesheetSingle } from './interfaces/timesheet.interface';
import { TimesheetService } from './services/timesheet.service';
import { ToastService } from 'app/services/toast.service';
import * as moment from 'moment';
import { TimesheetStatus } from './models/timesheet-status';
import { finalize, tap, map } from 'rxjs/operators';
import { EditTimesheetComponent } from './edit-timesheet/edit-timesheet.component';
import { Router } from '@angular/router';
import { EventsService } from 'app/services/events.service';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { PHONE_NUMBER } from 'app/constants/phone-number';
import { Store, select } from '@ngrx/store';
import { AppState } from '@capacitor/core';
import { getTotalUnreadMessages } from 'app/store/chat/chat.selectors';

@Component({
  selector: 'app-timesheets',
  templateUrl: 'timesheets.component.html',
  styleUrls: ['timesheets.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimesheetsComponent implements OnInit {
  private readonly FREELANCER_ID: string;
  private readonly MESSAGE_SUBMIT_ALL: string = 'alert-service.confirm.cancel.submit.all';
  readonly phoneNumber: string = PHONE_NUMBER;
  public timesheetIds: string[];
  public timesheets$: Observable<Timesheets>;
  public isHaveUnreadMessages$: Observable<boolean>;

  public readonly filterItems = [
    { countDays: 7, value: 'timesheet.week' },
    { countDays: 30, value: 'timesheet.month' },
    { countDays: 365, value: 'timesheet.year' },
    { countDays: 0, value: 'timesheet.allTime' },
  ];
  public selectedFilter = 0;
  public isEnabledSubmitAll: boolean;

  constructor(
    private storageService: StorageService,
    private timesheetService: TimesheetService,
    private router: Router,
    private modalController: ModalController,
    private events: EventsService,
    private toastService: ToastService,
    private loading: LoadingRepository,
    private store$: Store<AppState>,
    private backAlertService: BackAlertService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.FREELANCER_ID = this.storageService.getFreelancerId();
  }

  public ngOnInit(): void {
    this.fetchLastTimesheets();
    this.isHaveUnreadMessages$ = this.store$.pipe(
      select(getTotalUnreadMessages()),
      map((res) => res > 0)
    );
  }

  public ionViewDidEnter(): void {}

  public async openView(timesheetId: string): Promise<any> {
    const modal = await this.modalController.create({
      component: ViewTimesheetComponent,
      componentProps: {
        timesheetId,
      },
    });
    return await modal.present();
  }

  public async addNewTimesheet(): Promise<any> {
    const modal = await this.modalController.create({
      component: EditTimesheetComponent,
    });
    modal.onDidDismiss().then(() => {
      this.fetchLastTimesheets();
    });
    return await modal.present();
  }

  public async openSettingsPage(): Promise<void> {
    const modal = await this.modalController.create({
      component: SettingsComponent,
    });
    modal.present();
  }

  public openChatList(): void {
    this.router.navigate(['tabs/chat-list']);
  }

  public fetchLastTimesheets(): void {
    this.loading.presentLoading();
    const timesheetQueryParams = this.getFilterParams();
    this.changeDetectorRef.markForCheck();
    this.timesheets$ = this.timesheetService.getLastTimesheets(this.FREELANCER_ID, timesheetQueryParams).pipe(
      tap((timesheets) => {
        this.changeDetectorRef.markForCheck();
        this.timesheetIds = Object.keys(timesheets);
        this.updateSubmitAllDisableStatus(timesheets);
      }),
      finalize(() => {
        this.loading.stopLoading();
      })
    );
  }

  public updateSubmitAllDisableStatus(timesheets: Timesheets): void {
    this.isEnabledSubmitAll = !!Object.values(timesheets).find(
      (timesheet: TimesheetSingle) =>
        timesheet.status === TimesheetStatus.draft || timesheet.status === TimesheetStatus.rejected
    );
  }

  public submitAllTimesheets(): void {
    this.backAlertService.showAlert(this.MESSAGE_SUBMIT_ALL).then((response) => {
      if (response) {
        this.loading.presentLoading();
        this.timesheetService.submitTimesheet(this.FREELANCER_ID, this.timesheetIds).subscribe(
          () => {
            this.loading.stopLoading();
            this.toastService.presentSuccessToast();
            this.fetchLastTimesheets();
          },
          () => this.loading.stopLoading()
        );
      }
    });
  }

  public submitReceiver(id: string, timesheets: Timesheets): void {
    timesheets[id].status = TimesheetStatus.submitted;
    this.updateSubmitAllDisableStatus(timesheets);
  }

  private getFilterParams(): TimesheetParams {
    let mapFromTo = new Map();

    if (!!this.selectedFilter) {
      mapFromTo
      .set(7, {
        fromDate: moment()
          .weekday(1)
          .format('YYYY-MM-DD'),
        toDate: moment()
          .weekday(7)
          .format('YYYY-MM-DD')
      })
      .set(30, {
        fromDate: moment()
          .startOf('month')
          .format('YYYY-MM-DD'),
        toDate: moment()
          .endOf('month')
          .format('YYYY-MM-DD')
      })
      .set(365, {
        fromDate: moment()
          .format('YYYY-01-01'),
        toDate: moment()
          .format('YYYY-12-31')
      })
      return { ...mapFromTo.get(this.selectedFilter) };
    }

  }
}
