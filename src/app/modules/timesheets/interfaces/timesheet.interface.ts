export interface TimesheetApi {
  id: string;
  date: string;
  projectName: string;
  duration: string;
  status: string;
  rate: number;
  comment: string;
}

export interface TimesheetParams {
  fromDate?: string;
  toDate?: string;
}

export interface TimesheetSingle {
  id: string;
  date: number[];
  projectName: string;
  duration: string;
  status: string;
  rate: number;
  comment: string;
}

export interface TimesheetDescription {
  id?: string;
  date: number[];
  projectId: string;
  duration: string;
  status?: string;
  rateId: number;
  comment: string;
}

export interface Timesheets {
  [id: string]: TimesheetSingle;
}

export interface OngoingJob {
  jobId: string;
  name: string;
  rates: JobRate[];
}

export interface JobRate {
  rateId: string;
  fee: string;
}
