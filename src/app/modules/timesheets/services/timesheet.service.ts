import {
  OngoingJob,
  TimesheetApi,
  TimesheetDescription,
  TimesheetParams,
  Timesheets,
} from '../interfaces/timesheet.interface';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map, reduce } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { TimesheetDay, TimesheetDayParams } from '../models/timesheet-day';
import { TimesheetEntry, TimesheetEntryParams } from '../models/timesheet-entry';
import { TranslatableError } from 'app/interfaces/models/translatable-error';
import { resources } from 'app/app.resources';
import { UpdateTimesheetEntryRequest } from '../models/update-timesheet-entry';
import { DateUtils } from 'app/interfaces/models/date-utils';


const FREELANCER_RESOURCE_URL = resources.freelancerResourceBaseUrl;
const TIMESHEETS_RESOURCE_POST_FIX = '/timesheetentries'; // timesheets
const ONGOING_JOBS = '/ongoing-jobs';

@Injectable()
export class TimesheetService {
  constructor(private http: HttpClient) { }

  public getLastTimesheets(freelancerId: string, timesheetParams?: TimesheetParams): Observable<Timesheets> {
    return this.http
      .get<TimesheetApi[]>(FREELANCER_RESOURCE_URL.concat('/', freelancerId, TIMESHEETS_RESOURCE_POST_FIX), {
        params: { ...timesheetParams },
      })
      .pipe(
        map((data) => data.map((item) => ({ [item.id]: item }))),
        reduce((acc, item: any) => Object.assign(acc, ...item), {}),
        catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))),
      );
  }

  /* FIXME: Old endpoint */
  public getTimesheetDay(freelancerId: string, timesheetId: string, date: Date): Observable<TimesheetDay> {
    const TIMESHEETS_URL =
      FREELANCER_RESOURCE_URL +
      '/' +
      freelancerId +
      TIMESHEETS_RESOURCE_POST_FIX +
      '/' +
      timesheetId +
      '/' +
      DateUtils.formatDate(date);
    return this.http.get<TimesheetDayParams>(TIMESHEETS_URL).pipe(
      map(
        (timesheetDay: TimesheetDayParams) =>
          new TimesheetDay(
            timesheetDay.date,
            timesheetDay.timesheetEntries.map(
              (timesheetEntry: TimesheetEntryParams) =>
                new TimesheetEntry(
                  timesheetEntry.id,
                  timesheetEntry.clientName,
                  timesheetEntry.rate,
                  timesheetEntry.rateId,
                  timesheetEntry.projectName,
                  timesheetEntry.projectId,
                  timesheetEntry.editable,
                  timesheetEntry.duration,
                  timesheetEntry.freelancerComment,
                ),
            ),
          ),
      ),
      catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))),
    );
  }

  /* FIXME: Old endpoint */
  public saveTimesheetEntries(
    freelancerId: string,
    timesheetId: string,
    date: Date,
    updateTimesheetEntryRequests: UpdateTimesheetEntryRequest[],
  ): Observable<void> {
    const TIMESHEETS_URL =
      FREELANCER_RESOURCE_URL +
      '/' +
      freelancerId +
      TIMESHEETS_RESOURCE_POST_FIX +
      '/' +
      timesheetId +
      '/' +
      DateUtils.formatDate(date);
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http
      .put<void>(TIMESHEETS_URL, updateTimesheetEntryRequests, { headers: headers })
      .pipe(catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))));
  }

  public submitTimesheet(freelancerId: string, timesheetEntryIds: string[]): Observable<void> {
    const TIMESHEETS_URL = FREELANCER_RESOURCE_URL + '/' + freelancerId + TIMESHEETS_RESOURCE_POST_FIX + '/submit';
    return this.http
      .put<void>(TIMESHEETS_URL, {
        timesheetEntryIds,
      })
      .pipe(catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))));
  }

  public getTimesheet(freelancerId: string, timesheetId: string): Observable<TimesheetDescription> {
    return this.http.get<TimesheetDescription>(
      FREELANCER_RESOURCE_URL.concat('/', freelancerId, TIMESHEETS_RESOURCE_POST_FIX, '/', timesheetId),
    );
  }

  public getOngoingJobs(freelancerId: string): Observable<OngoingJob[]> {
    return this.http.get<OngoingJob[]>(FREELANCER_RESOURCE_URL.concat('/', freelancerId, ONGOING_JOBS));
  }

  public updateTimesheet(freelancerId: string, timesheetEntryJsonRequest: TimesheetDescription): Observable<any> {
    return this.http.put(FREELANCER_RESOURCE_URL.concat('/', freelancerId, TIMESHEETS_RESOURCE_POST_FIX), {
      ...timesheetEntryJsonRequest,
    });
  }
}
