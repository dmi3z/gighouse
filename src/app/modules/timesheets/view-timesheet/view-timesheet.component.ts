import { TimesheetStatus } from './../models/timesheet-status';
import { EditTimesheetComponent } from './../edit-timesheet/edit-timesheet.component';
import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { StorageService } from 'app/services/storage.service';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { JobRate, OngoingJob, TimesheetDescription } from '../interfaces/timesheet.interface';
import { TimesheetService } from '../services/timesheet.service';
import { LoadingRepository } from 'app/services/loading.repository';
import { EventsService } from 'app/services/events.service';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';

@Component({
  selector: 'app-view-timesheet',
  templateUrl: 'view-timesheet.component.html',
  styleUrls: ['view-timesheet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewTimesheetComponent implements OnInit, OnDestroy {
  @Input() timesheetId: string;

  public timesheet: any;
  public isDisabled = false;
  public projects$: Observable<OngoingJob[]>;
  public project$: Observable<OngoingJob>;
  public rate$: Observable<JobRate>;

  private freelancerId: string;

  private subs: Subscription;

  constructor(
    private storageService: StorageService,
    private timesheetService: TimesheetService,
    private modalController: ModalController,
    private loading: LoadingRepository,
    private events: EventsService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.freelancerId = this.storageService.getFreelancerId();

    if (this.timesheetId) {
      this.loadTimesheetData(this.timesheetId).subscribe((result) => {
        this.timesheet = result;
        this.getProject();
        this.getRate();
        if (this.timesheet.status === TimesheetStatus.submitted) {
          this.isDisabled = true;
        }
      });
    }
  }

  public ngOnDestroy(): void {
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }

  public close(): void {
    this.modalController.dismiss();
  }

  public async openEdit(): Promise<any> {
    const modal = await this.modalController.create({
      component: EditTimesheetComponent,
      componentProps: {
        timesheetId: this.timesheetId,
      },
      animated: false,
    });
    return await modal.present();
  }

  public submitTimesheet() {
    this.loading.presentLoading();
    this.isDisabled = true;
    this.timesheetService.submitTimesheet(this.storageService.getFreelancerId(), [this.timesheet.id]).subscribe(
      () => {
        this.events.publish(GighouseEvent.timesheetUpdated);
        this.loading.stopLoading();
        this.modalController.dismiss();
      },
      () => {
        this.loading.stopLoading();
        this.modalController.dismiss();
      }
    );
  }

  private loadTimesheetData(timesheetId: string): Observable<TimesheetDescription> {
    return this.timesheetService.getTimesheet(this.freelancerId, timesheetId).pipe(take(1));
  }

  private getProject(): void {
    this.projects$ = this.timesheetService.getOngoingJobs(this.freelancerId);
    this.project$ = this.projects$.pipe(
      map((project) => project.find((item) => item.jobId === this.timesheet.projectId))
    );
  }

  private getRate(): void {
    this.rate$ = this.project$.pipe(map((item) => item.rates.find((itm) => itm.rateId === this.timesheet.rateId)));
  }
}
