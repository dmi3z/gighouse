import { HelperService } from 'app/services/helper.service';
import { BackAlertService } from 'app/services/back-alert.service';
import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController, NavController, NavParams, Platform } from '@ionic/angular';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { EventsService } from 'app/services/events.service';
import { StorageService } from 'app/services/storage.service';
import { ToastService } from 'app/services/toast.service';
import * as moment from 'moment';
import { Observable, of, Subscription } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { JobRate, OngoingJob, TimesheetDescription } from '../interfaces/timesheet.interface';
import { TimesheetService } from '../services/timesheet.service';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';

@Component({
  selector: 'app-edit-timesheet',
  templateUrl: 'edit-timesheet.component.html',
  styleUrls: ['edit-timesheet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditTimesheetComponent implements OnInit, OnDestroy {
  @Input() timesheetId: string;
  public timesheetForm: FormGroup;
  public projects$: Observable<OngoingJob[]>;
  public rates$: Observable<JobRate[]>;
  public dateFrom: string;
  public dateTo: string;

  private oldForm: TimesheetDescription;
  private freelancerId: string;

  private subs: Subscription = new Subscription();

  constructor(
    private navParams: NavParams,
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private timesheetService: TimesheetService,
    private toastService: ToastService,
    private events: EventsService,
    private modalController: ModalController,
    private backAlertService: BackAlertService,
    private helperService: HelperService,
    private platform: Platform,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.dateFrom = moment().format('YYYY-MM-DD');
    this.dateTo = moment().add(5, 'y').format('YYYY-MM-DD');
  }

  public ngOnInit(): void {
    this.freelancerId = this.storageService.getFreelancerId();
    this.initializeBackButtonHandler();

    if (this.timesheetId) {
      this.loadTimesheetData(this.timesheetId).subscribe((result) => {
        this.getRates(result.projectId);
        this.timesheetForm = this.createForm(result);
      });
    } else {
      const timesheet = this.createNewTimesheet();
      this.timesheetForm = this.createForm(timesheet);
    }
    this.projects$ = this.getProjects();
  }

  private loadTimesheetData(timesheetId: string): Observable<TimesheetDescription> {
    return this.timesheetService.getTimesheet(this.freelancerId, timesheetId).pipe(take(1));
  }

  private getProjects(): Observable<OngoingJob[]> {
    return this.timesheetService.getOngoingJobs(this.freelancerId);
  }

  private createForm(timesheet: TimesheetDescription): FormGroup {
    const { comment, date, projectId, duration, rateId } = timesheet;
    const dateString = this.convertToISOString(date);
    const form = this.formBuilder.group({
      date: [dateString, [Validators.required]],
      projectId: [projectId, Validators.required],
      time: [moment(duration, 'hh:mm').format(), Validators.required],
      rateId: [rateId, Validators.required],
      comment,
    });
    this.oldForm = form.value;
    this.subs.add(
      form.controls.projectId.valueChanges.subscribe((jobId) => {
        form.controls.rateId.setValue('');
        this.getRates(jobId);
      })
    );

    return form;
  }
  private getRates(jobId: string): void {
    this.rates$ = this.projects$.pipe(map((projects) => projects.find((project) => project.jobId === jobId).rates));
  }

  private createNewTimesheet(): TimesheetDescription {
    const date = moment()
      .format('YYYY,MM,DD')
      .split(',')
      .map((item) => Number(item));

    const timesheet: TimesheetDescription = {
      comment: '',
      duration: '00:30',
      date,
      projectId: '',
      rateId: null,
      status: '',
    };

    return timesheet;
  }

  private convertToISOString(date: number[]): string {
    const isoString = moment(`${date[3]}/${date[2]}/${date[1]}`, 'DD/MM/YYYY').format();
    return isoString;
  }

  public saveChanges(): void {
    const { date, projectId, time, rateId, comment } = this.timesheetForm.value;
    const timesheet: TimesheetDescription = {
      date: date.slice(0, 10),
      projectId,
      duration: moment(time).format('HH:mm'),
      rateId,
      comment,
    };
    if (this.timesheetId) {
      timesheet.id = this.timesheetId;
    }
    this.timesheetService
      .updateTimesheet(this.freelancerId, timesheet)
      .pipe(take(1))
      .subscribe(
        () => {
          this.modalController.dismiss();
          this.toastService.presentSuccessToast();
          this.events.publish(GighouseEvent.timesheetUpdated);
        },
        () => {
          this.modalController.dismiss();
          this.toastService.presentErrorToast('timesheet.incorrect.data');
          this.events.publish(GighouseEvent.timesheetUpdated);
        }
      );
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.close();
    });
    this.subs.add(subscription);
  }

  public close(): void {
    if (this.helperService.objectIsEqual(this.oldForm, this.timesheetForm.value) && this.timesheetId) {
      this.backAlertService.showAlert();
    } else {
      this.modalController.dismiss();
    }
  }

  public ngOnDestroy(): void {
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }
}
