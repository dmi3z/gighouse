import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children: [
      {
        path: 'skills',
        loadChildren: () => import('./components/skills/skills.module').then((m) => m.SkillsModule),
      },
      {
        path: 'aspired',
        loadChildren: () => import('./components/aspired/aspired.module').then((m) => m.AspiredModule),
      },
      {
        path: 'biography',
        loadChildren: () => import('./components/biography/biography.module').then((m) => m.BiographyModule),
      },
      {
        path: 'preferences',
        loadChildren: () => import('./components/preferences/preferences.module').then((m) => m.PreferencesModule),
      },
      {
        path: '',
        redirectTo: 'skills',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
