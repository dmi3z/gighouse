import { ProfileDetailsComponent } from './components/details/details.component';
import { ProfileCompletenessComponent } from './components/completeness/profile-completeness.component';
import { PipesModule } from './../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile.component';
import { ProfileRoutingModule } from './profile-routing.module';

@NgModule({
  imports: [
    IonicModule,
    TranslateModule,
    CommonModule,
    FormsModule,
    ProfileRoutingModule,
    PipesModule,
    ReactiveFormsModule,
  ],
  declarations: [ProfileComponent, ProfileDetailsComponent, ProfileCompletenessComponent],
})
export class ProfileModule {}
