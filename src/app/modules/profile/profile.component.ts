import { select, Store } from '@ngrx/store';
import { ProfileDetailsComponent } from './components/details/details.component';
import { LoadingRepository } from 'app/services/loading.repository';
import { PushNotificationService } from './../../services/push-notification.service';
import { take, map } from 'rxjs/operators';
import { FreelancerDetails } from './../../interfaces/models/freelancer-details';
import { FreelancerRepository } from './../../services/freelancer.repository';
import { Router } from '@angular/router';
import { ModalController, PopoverController } from '@ionic/angular';
import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { SettingsComponent } from '../settings/settings.component';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { StorageService } from 'app/services/storage.service';
import { FreelancerService } from 'app/services/freelancer.service';
import { ProfileCompleteness } from 'app/interfaces/models/profile-completeness';
import { EventsService } from 'app/services/events.service';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { ProfileCompletenessComponent } from './components/completeness/profile-completeness.component';
import { getTotalUnreadMessages } from 'app/store/chat/chat.selectors';
import { AppState } from '@capacitor/core';
import { Plugins } from '@capacitor/core';
const { SplashScreen } = Plugins;

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent implements OnInit {
  public freelancerDetails$: Observable<FreelancerDetails>;
  public isExistingUser$: Subject<boolean> = new BehaviorSubject(false);
  public profileCompleteness: ProfileCompleteness;
  public isHaveUnreadMessages$: Observable<boolean>;

  public tabs = [
    {
      text: 'preferences.tab.title',
      path: 'preferences',
      dataCy: 'preferences-tab',
    },
    {
      text: 'skills.tab.title',
      path: 'skills',
      dataCy: 'skills-tab',
    },
    {
      text: 'aspired-freelancer-profiles.tab.title',
      path: 'aspired',
      dataCy: 'aspired-profiles-tab',
    },
    {
      text: 'biography.tab.title',
      path: 'biography',
      dataCy: 'biography-tab',
    },
  ];

  private freelancerId: string;

  constructor(
    private modalController: ModalController,
    private router: Router,
    private freelancerRepository: FreelancerRepository,
    private freelancerService: FreelancerService,
    private storageService: StorageService,
    private events: EventsService,
    private pushNotificationService: PushNotificationService,
    private loadingRepository: LoadingRepository,
    private store$: Store<AppState>,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.freelancerId = this.storageService.getFreelancerId();
    this.freelancerDetails$ = this.freelancerRepository.getFreelancerDetails().pipe(take(1));

    this.isExistingUser(this.freelancerId);

    this.fetchProfileCompleteness();
    this.events.subscribe(GighouseEvent.profileCompletenessChanged, () => {
      this.fetchProfileCompleteness();
    });

    // this.pushNotificationService.initialisePush();
    // this.pushNotificationService.registerDevice();
    // this.pushNotificationService.addNotificationListener();

    this.loadingRepository.stopLoading();

    this.isHaveUnreadMessages$ = this.store$.pipe(
      select(getTotalUnreadMessages()),
      map((res) => res > 0)
    );
  }

  public ionViewDidEnter(): void {
    SplashScreen.hide();
  }

  public async openSettingsPage(): Promise<void> {
    const modal = await this.modalController.create({
      component: SettingsComponent,
    });
    modal.present();
  }

  public async openDetailPage(): Promise<void> {
    const modal = await this.modalController.create({
      component: ProfileDetailsComponent,
    });
    modal.present();
  }

  public async openProfileCompletenessPopover(): Promise<void> {
    const modal = await this.modalController.create({
      component: ProfileCompletenessComponent,
      componentProps: {
        profileCompleteness: this.profileCompleteness,
      },
      cssClass: 'profile-completeness__popover',
      showBackdrop: true,
    });
    return await modal.present();
  }

  public openChatList(): void {
    this.router.navigate(['tabs/chat-list']);
  }

  private isExistingUser(freelancerId: string): void {
    if (freelancerId) {
      this.isExistingUser$.next(true);
    } else {
      this.isExistingUser$.next(false);
    }
  }

  private fetchProfileCompleteness() {
    this.freelancerService
      .getProfileCompleteness(this.freelancerId)
      .pipe(take(1))
      .subscribe((profileCompleteness: ProfileCompleteness) => {
        this.changeDetectorRef.markForCheck();
        this.profileCompleteness = profileCompleteness;
      });
  }
}
