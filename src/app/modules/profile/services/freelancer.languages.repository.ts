import { TranslatableError } from './../../../interfaces/models/translatable-error';
import { FreelancerService } from 'app/services/freelancer.service';
import { Observable, Subject } from 'rxjs';

import { Injectable } from '@angular/core';
import { flatMap, take } from 'rxjs/operators';
import { FreelancerLanguage } from 'app/interfaces/models/freelancer-language';
import { StorageService } from 'app/services/storage.service';
import { HelperService } from 'app/services/helper.service';

@Injectable()
export class FreelancerLanguagesRepository {
  private freelancerLanguagesHasBeenChanged$: Subject<void> = new Subject();
  private freelancerLanguages: FreelancerLanguage[];
  private originalFreelancerLanguages: FreelancerLanguage[];

  constructor(
    private freelancerService: FreelancerService,
    private storageService: StorageService,
    private helper: HelperService
  ) {}

  public initialiseFreelancerLanguages(): void {
    const freelancerId = this.storageService.getFreelancerId();
    this.freelancerService
      .getFreelancerLanguages(freelancerId)
      .pipe(take(1))
      .subscribe((freelancerLanguages: FreelancerLanguage[]) => {
        this.freelancerLanguages = freelancerLanguages;
        this.setDefaultSkillLevelValueWhenUndefined();
        this.originalFreelancerLanguages = this.helper.cloneDeepArray(freelancerLanguages);
        this.freelancerLanguagesHasBeenChanged$.next();
      });
  }

  private clearData(): void {
    this.freelancerLanguages = null;
    this.originalFreelancerLanguages = null;
  }

  private setDefaultSkillLevelValueWhenUndefined() {
    this.freelancerLanguages.forEach((language: FreelancerLanguage) => {
      if (!language.languageSkillLevelValue) {
        language.languageSkillLevelValue = 'None';
      }
    });
  }

  public addFreelancerLanguage(newLanguage: FreelancerLanguage): void {
    this.freelancerLanguages.forEach((oldLanguage) => {
      if (oldLanguage.languageValue === newLanguage.languageValue) {
        this.removeOldLanguageAndAddNewLanguage(oldLanguage, newLanguage);
        this.freelancerLanguagesHasBeenChanged$.next();
      }
    });
  }

  private removeOldLanguageAndAddNewLanguage(oldLanguage: FreelancerLanguage, newLanguage: FreelancerLanguage) {
    this.freelancerLanguages.splice(this.freelancerLanguages.indexOf(oldLanguage), 1, newLanguage);
  }

  public getFreelancerLanguages(): FreelancerLanguage[] {
    return this.freelancerLanguages;
  }

  public getFreelancerLanguagesHasBeenChanged(): Observable<void> {
    return this.freelancerLanguagesHasBeenChanged$;
  }

  public hasChanges(): boolean {
    return this.helper.isEqual(this.originalFreelancerLanguages, this.freelancerLanguages);
  }

  public revertChanges() {
    this.freelancerLanguages = this.helper.cloneDeepArray(this.originalFreelancerLanguages);
    this.freelancerLanguagesHasBeenChanged$.next();
  }

  public saveLanguages(): Observable<void | TranslatableError> {
    const languagesToSave = this.filterLanguagesToBeDeleted(this.freelancerLanguages);
    const freelancerId = this.storageService.getFreelancerId();
    return this.freelancerService.updateFreelancerLanguages(freelancerId, languagesToSave);
  }

  private filterLanguagesToBeDeleted(freelancerLanguages: FreelancerLanguage[]): FreelancerLanguage[] {
    const languagesToSave: FreelancerLanguage[] = [];
    freelancerLanguages.forEach((language: FreelancerLanguage) => {
      if (language.languageSkillLevelValue !== 'None') {
        languagesToSave.push(language);
      }
    });
    return languagesToSave;
  }

  public countKnownLanguages(): number {
    if (!this.freelancerLanguages) {
      return 0;
    }
    return this.freelancerLanguages.filter((language) => language.languageSkillLevelValue !== 'None').length;
  }
}
