import { FreelancerSkillRating } from './../../../interfaces/models/freelancer-skill-rating';
import { TranslatableError } from './../../../interfaces/models/translatable-error';
import { FreelancerService } from 'app/services/freelancer.service';
import { Injectable } from '@angular/core';
import { FreelancerSkill } from 'app/interfaces/models/freelancer-skill';
import { Observable, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { StorageService } from 'app/services/storage.service';
import { HelperService } from 'app/services/helper.service';

@Injectable()
export class FreelancerSkillsRepository {
  private freelancerSkillsHasBeenChanged$: Subject<void> = new Subject();
  private freelancerSkills: FreelancerSkill[];
  private originalFreelancerSkills: FreelancerSkill[];

  constructor(
    private freelancerService: FreelancerService,
    private storageService: StorageService,
    private helper: HelperService
  ) {
    // events.subscribe(GighouseEvent.freelancerLogout, () => this.clearData()); !!!!
  }

  public initialiseFreelancerSkills(): void {
    const freelancerId = this.storageService.getFreelancerId();
    this.freelancerService
      .getFreelancerSkills(freelancerId)
      .pipe(take(1))
      .subscribe((freelancerSkills: FreelancerSkill[]) => {
        this.freelancerSkills = freelancerSkills;
        this.originalFreelancerSkills = this.helper.cloneDeepArray(freelancerSkills);
        this.freelancerSkillsHasBeenChanged$.next();
      });
  }

  private clearData(): void {
    this.freelancerSkills = null;
    this.originalFreelancerSkills = null;
  }

  getFreelancerSkills(): FreelancerSkill[] {
    return this.freelancerSkills;
  }

  getFreelancerSkillsHasBeenChanged(): Observable<void> {
    return this.freelancerSkillsHasBeenChanged$;
  }

  public hasChanges(): boolean {
    return this.helper.isEqual(this.originalFreelancerSkills, this.freelancerSkills);
  }

  public revertChanges(): void {
    if (this.hasChanges()) {
      this.freelancerSkills = this.helper.cloneDeepArray(this.originalFreelancerSkills);
      this.freelancerSkillsHasBeenChanged$.next();
    }
  }

  saveSkills(): Observable<void | TranslatableError> {
    const skillRatings = this.extractSelectedSkillIds(this.freelancerSkills);
    const freelancerId = this.storageService.getFreelancerId();

    return this.freelancerService.updateFreelancerSkills(freelancerId, skillRatings);
  }

  private extractSelectedSkillIds(freelancerSkills: FreelancerSkill[]): FreelancerSkillRating[] {
    const skillRatings: FreelancerSkillRating[] = [];
    freelancerSkills.forEach((skill: FreelancerSkill) => {
      if (skill.rating > 0) {
        skillRatings.push(new FreelancerSkillRating(skill.id, skill.rating));
      }
      skill.extractActiveFreelancerSkillIds(skillRatings);
    });
    return skillRatings;
  }
}
