import { take } from 'rxjs/operators';
import { GighouseEvent } from './../../../interfaces/models/gighouse-event';
import { EventsService } from './../../../services/events.service';
import { HelperService } from './../../../services/helper.service';
import { AspiredFreelancerProfilesByExpertise } from './../../../interfaces/models/aspired-freelancer-profiles-by-expertise';
import { StorageService } from './../../../services/storage.service';
import { FreelancerService } from './../../../services/freelancer.service';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class AspiredFreelancerProfilesRepository {
  private aspiredFreelancerProfilesChanged$: Subject<void> = new Subject();
  private aspiredFreelancerProfiles: AspiredFreelancerProfilesByExpertise[];
  private originalAspiredFreelancerProfiles: AspiredFreelancerProfilesByExpertise[];

  constructor(
    private freelancerService: FreelancerService,
    private storageService: StorageService,
    private helper: HelperService,
    private events: EventsService
  ) {
    this.events.subscribe(GighouseEvent.freelancerLogout, () => this.clearData());
  }

  public initialiseAspiredFreelancerProfiles(): void {
    const freelancerId = this.storageService.getFreelancerId();
    this.freelancerService
      .getAspiredFreelancerProfiles(freelancerId)
      .pipe(take(1))
      .subscribe((aspiredFreelancerProfiles: AspiredFreelancerProfilesByExpertise[]) => {
        this.aspiredFreelancerProfiles = aspiredFreelancerProfiles;
        this.originalAspiredFreelancerProfiles = this.helper.cloneDeepArray(aspiredFreelancerProfiles);
        this.aspiredFreelancerProfilesChanged$.next();
      });
  }

  public rebuildArray(
    originalItem: AspiredFreelancerProfilesByExpertise,
    changedItem: AspiredFreelancerProfilesByExpertise
  ): void {
    this.aspiredFreelancerProfiles = this.aspiredFreelancerProfiles.map((freelancerProfile) => {
      if (this.helper.isEqual(freelancerProfile, originalItem)) {
        return changedItem;
      }
      return freelancerProfile;
    });
  }

  public getAspiredFreelancerProfiles(): AspiredFreelancerProfilesByExpertise[] {
    return this.aspiredFreelancerProfiles;
  }

  public getAspiredFreelancerProfilesHasBeenChanged(): Observable<void> {
    return this.aspiredFreelancerProfilesChanged$;
  }

  public save(): Observable<void> {
    const ids = this.extractSelectedProfileIds(this.aspiredFreelancerProfiles);
    const freelancerId = this.storageService.getFreelancerId();
    return this.freelancerService.updateAspiredFreelancerProfiles(freelancerId, ids);
  }

  public hasChanges(): boolean {
    return this.helper.isEqual(this.originalAspiredFreelancerProfiles, this.aspiredFreelancerProfiles);
  }

  public revertChanges(): void {
    if (this.hasChanges()) {
      this.aspiredFreelancerProfiles = this.helper.cloneDeepArray(this.originalAspiredFreelancerProfiles);
      this.aspiredFreelancerProfilesChanged$.next();
    }
  }

  private clearData(): void {
    this.aspiredFreelancerProfiles = null;
    this.originalAspiredFreelancerProfiles = null;
  }

  private extractSelectedProfileIds(freelancerSkills: AspiredFreelancerProfilesByExpertise[]): string[] {
    const ids: string[] = [];
    freelancerSkills.forEach((aspiredFreelancerProfileTree: AspiredFreelancerProfilesByExpertise) => {
      aspiredFreelancerProfileTree.profiles.forEach((profile) => {
        if (profile.active) {
          ids.push(profile.id);
        }
      });
    });
    return ids;
  }
}
