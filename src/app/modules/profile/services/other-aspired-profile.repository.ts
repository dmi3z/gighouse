import { GighouseEvent } from './../../../interfaces/models/gighouse-event';
import { EventsService } from './../../../services/events.service';
import { StorageService } from './../../../services/storage.service';
import { FreelancerService } from './../../../services/freelancer.service';
import { OtherAspiredProfile } from './../../../interfaces/models/other-aspired-profile';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class OtherAspiredProfileRepository {
  protected _otherAspiredProfileInitialized: boolean;
  protected _otherAspiredProfile$: Subject<OtherAspiredProfile>;

  constructor(
    private freelancerService: FreelancerService,
    private storageService: StorageService,
    private events: EventsService
  ) {
    this.events.subscribe(GighouseEvent.freelancerLogout, () => this.removeOtherAspiredProfileData());
  }

  getOtherAspiredProfile(refresh?: boolean): Observable<OtherAspiredProfile> {
    if (refresh || !this._otherAspiredProfileInitialized) {
      this._otherAspiredProfile$ = new ReplaySubject(1);
      this._otherAspiredProfileInitialized = true;
      const freelancerId = this.storageService.getFreelancerId();
      this.freelancerService.getOtherAspiredProfile(freelancerId).subscribe((otherAspiredProfile) => {
        this._otherAspiredProfile$.next(otherAspiredProfile);
        this.events.publish(GighouseEvent.otherAspiredProfileRefreshed);
      });
    }
    return this._otherAspiredProfile$.asObservable();
  }

  saveOtherAspiredProfile(other: string): Observable<OtherAspiredProfile> {
    const freelancerId = this.storageService.getFreelancerId();
    return this.freelancerService
      .updateOtherAspiredProfile(freelancerId, other)
      .pipe(switchMap(() => this.getOtherAspiredProfile(true)));
  }

  removeOtherAspiredProfileData(): void {
    this._otherAspiredProfileInitialized = false;
    this._otherAspiredProfile$.complete();
  }
}
