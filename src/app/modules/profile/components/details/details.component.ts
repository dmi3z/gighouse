import { TranslateService } from '@ngx-translate/core';
import { TranslationsService } from './../../../../services/translations.service';
import { ToastService } from './../../../../services/toast.service';
import { FreelancerRepository } from './../../../../services/freelancer.repository';
import { LoadingRepository } from './../../../../services/loading.repository';
import { BackAlertService } from 'app/services/back-alert.service';
import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FreelancerDetails } from 'app/interfaces/models/freelancer-details';
import { Platform, ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FreelancerService } from 'app/services/freelancer.service';
import { StorageService } from 'app/services/storage.service';
import { Profile } from 'app/interfaces/models/profile';
import { Expertise } from 'app/interfaces/models/expertise';
import { map, switchMap, take } from 'rxjs/operators';
import {
  UpdateFreelancerCompanyRequest,
  UpdateFreelancerProfileDetailsRequest,
} from 'app/interfaces/models/update-freelancer';
import { CompanyForm } from 'app/interfaces/models/company-form';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { CacheableMetadataService } from 'app/services/cacheable-metadata.service';
import { HelperService } from 'app/services/helper.service';
import { forkJoin, of, Subscriber, Subscription } from 'rxjs';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';
import { EventsService } from 'app/services/events.service';

@Component({
  selector: 'app-profile-details',
  templateUrl: 'details.component.html',
  styleUrls: ['details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileDetailsComponent implements OnInit {
  public isFormChanged: boolean = false;

  public expertises: Expertise[];
  public profilesForExpertise: Profile[];
  public companyForms: CompanyForm[];

  private freelancerDetails: FreelancerDetails;

  public formInitialized = false;
  public profileDetailsForm: FormGroup;
  private profileDetailsFormFields;
  private subs: Subscription = new Subscription();

  constructor(
    public translate: TranslateService,
    private events: EventsService,
    private toastService: ToastService,
    private freelancerRepository: FreelancerRepository,
    private loadingRepository: LoadingRepository,
    private platform: Platform,
    private translationService: TranslationsService,
    private formBuilder: FormBuilder,
    private backAlertService: BackAlertService,
    private storageService: StorageService,
    private freelancerService: FreelancerService,
    private cacheableMetadataService: CacheableMetadataService,
    private helperService: HelperService,
    private modalController: ModalController,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.loadingRepository.presentLoading();
    this.initializeBackButtonHandler();
    const freelancerId = this.storageService.getFreelancerId();
    this.freelancerService
      .getFreelancerDetails(freelancerId)
      .pipe(
        switchMap((freelancerDetails) => {
          this.freelancerDetails = freelancerDetails;
          return forkJoin([
            this.cacheableMetadataService.getExpertises(),
            this.cacheableMetadataService.getProfilesFor(this.freelancerDetails.profile.getExpertise().getValue()),
            this.cacheableMetadataService.getCompanyForms(),
          ]);
        }),
        map(([expertises, profiles, companyForms]: [Expertise[], Profile[], CompanyForm[]]) => {
          this.expertises = expertises;
          this.profilesForExpertise = profiles;
          this.companyForms = companyForms;
        }),
        take(1)
      )
      .subscribe(() => {
        this.changeDetectorRef.markForCheck();
        this.loadingRepository.stopLoading();
        this.initializeForm();
      });
  }

  public navigateBack(): void {
    if (this.isFormChanged) {
      this.backAlertService.showAlert();
    } else {
      this.modalController.dismiss();
    }
  }

  public saveData(): void {
    if (this.profileDetailsForm.invalid) {
      this.toastService.presentErrorToast(this.translationService._('profile-details.toast.error-fields-incomplete'));
      this.triggerValidation();
    } else {
      this.loadingRepository.presentLoading();
      this.freelancerRepository
        .saveFreelancerDetails(this.createUpdateFreelancerProfileDetailsRequestFromForm())
        .pipe(take(1))
        .subscribe((success: boolean) => {
          if (success) {
            this.events.publish(GighouseEvent.profileCompletenessChanged);
            this.loadingRepository.stopLoading();
            this.modalController.dismiss();
            this.toastService.presentSuccessToast();
          } else {
            this.toastService.presentErrorToast('profile-details.toast.error-save-data');
            this.loadingRepository.stopLoading();
          }
        });
    }
  }

  private triggerValidation(): void {
    Object.values(this.profileDetailsForm.controls).forEach((control) => {
      control.markAsTouched();
    });
  }

  public onExpertiseSelected(): void {
    this.profilesForExpertise = [];
    this.profileDetailsForm.controls.profile.setValue(null);
    this.filterProfilesByExpertise();
  }

  private initializeForm(): void {
    this.profileDetailsForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.maxLength(40)]],
      lastName: ['', [Validators.required, Validators.maxLength(80)]],
      email: [
        { value: '', disabled: true },
        [Validators.required, Validators.pattern('^([a-zA-Z0-9_\\-\\.+]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,63})$')],
      ],
      mobilePhone: ['', [Validators.required, Validators.pattern('^\\+[1-9]\\d{10,14}$')]],
      expertise: ['', [Validators.required, Validators.maxLength(100)]],
      profile: ['', [Validators.required, Validators.maxLength(100)]],
      companyName: ['', [Validators.required, Validators.maxLength(100)]],
      companyForm: ['', [Validators.required, Validators.maxLength(100)]],
      vatNumber: ['', [Validators.required, Validators.pattern('^BE0[0-9]{9}$')]],
      street: ['', [Validators.required, Validators.maxLength(100)]],
      postalCode: ['', [Validators.required, Validators.pattern('^\\d{4}$')]],
      city: ['', [Validators.required, Validators.maxLength(100)]],
      bankAccount: ['', [Validators.required, Validators.maxLength(100)]],
    });
    this.setValueIntoForm();
    this.formInitialized = true;
  }

  private setValueIntoForm(): void {
    this.profileDetailsFormFields = {
      firstName: this.checkForNull(this.freelancerDetails.firstName),
      lastName: this.checkForNull(this.freelancerDetails.lastName),
      email: this.checkForNull(this.freelancerDetails.email),
      mobilePhone: this.checkForNull(this.freelancerDetails.mobilePhone),
      expertise: this.checkForNull(this.freelancerDetails.profile.getExpertise().getValue()),
      profile: this.checkForNull(this.freelancerDetails.profile.getId()),
      companyName: this.checkForNull(this.freelancerDetails.company.name),
      companyForm: this.checkForNull(this.freelancerDetails.company.companyForm.getValue()),
      street: this.checkForNull(this.freelancerDetails.company.street),
      postalCode: this.checkForNull(this.freelancerDetails.company.postalCode),
      city: this.checkForNull(this.freelancerDetails.company.city),
      vatNumber: this.checkForNull(this.freelancerDetails.company.vatNumber),
      bankAccount: this.checkForNull(this.freelancerDetails.company.bankAccount),
    };
    this.profileDetailsForm.patchValue(this.profileDetailsFormFields);
    this.checkEqualFields();
  }

  private checkForNull(valueToCheck: string | object): string | object {
    return valueToCheck === null ? '' : valueToCheck;
  }

  private checkEqualFields(): void {
    const subscription = this.profileDetailsForm.valueChanges.subscribe((fields) => {
      this.isFormChanged = this.helperService.objectIsEqual(fields, this.profileDetailsFormFields);
    });
    this.subs.add(subscription);
  }

  private createUpdateFreelancerProfileDetailsRequestFromForm(): UpdateFreelancerProfileDetailsRequest {
    return new UpdateFreelancerProfileDetailsRequest(
      this.freelancerDetails,
      this.profileDetailsForm.controls.firstName.value,
      this.profileDetailsForm.controls.lastName.value,
      this.profileDetailsForm.controls.mobilePhone.value,
      this.freelancerDetails.email,
      this.profileDetailsForm.controls.profile.value,
      new UpdateFreelancerCompanyRequest(
        this.profileDetailsForm.controls.companyName.value,
        this.profileDetailsForm.controls.companyForm.value,
        this.profileDetailsForm.controls.vatNumber.value,
        this.profileDetailsForm.controls.street.value,
        this.profileDetailsForm.controls.postalCode.value,
        this.profileDetailsForm.controls.city.value,
        this.profileDetailsForm.controls.bankAccount.value
      )
    );
  }

  private filterProfilesByExpertise(): void {
    if (this.profileDetailsForm.controls.expertise !== undefined) {
      this.cacheableMetadataService
        .getProfilesFor(this.profileDetailsForm.controls.expertise.value)
        .pipe()
        .subscribe((profilesForExpertise: Profile[]) => (this.profilesForExpertise = profilesForExpertise));
    }
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.navigateBack();
    });
    this.subs.add(subscription);
  }

  public ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
