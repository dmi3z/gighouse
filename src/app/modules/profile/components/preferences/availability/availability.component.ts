import { HelperService } from 'app/services/helper.service';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController, Platform } from '@ionic/angular';
import { FreelancerDetails } from 'app/interfaces/models/freelancer-details';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { JobPreferences } from 'app/interfaces/models/job-preferences';
import { UpdateFreelancerJobPreferencesRequest } from 'app/interfaces/models/update-freelancer';
import { EventsService } from 'app/services/events.service';
import { FreelancerRepository } from 'app/services/freelancer.repository';
import { LoadingRepository } from 'app/services/loading.repository';
import { ToastService } from 'app/services/toast.service';
import { take } from 'rxjs/operators';
import { BackAlertService } from 'app/services/back-alert.service';
import { Subscription } from 'rxjs';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';

@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AvailabilityComponent implements OnInit, OnDestroy {
  public freelancerDetails: FreelancerDetails;
  public availabilityForm: FormGroup;
  public originalAvailabilityFormData: any;
  public currentYear: number;

  private subs: Subscription = new Subscription();
  private formInitialized = false;

  constructor(
    private platform: Platform,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private backAlertService: BackAlertService,
    private freelancerRepository: FreelancerRepository,
    private loadingRepository: LoadingRepository,
    private events: EventsService,
    private modalController: ModalController,
    private helperService: HelperService
  ) {
    this.currentYear = new Date().getFullYear();
    this.createForm();
  }

  public ngOnInit(): void {
    this.initializeBackButtonHandler();
    this.freelancerRepository
      .getFreelancerDetails()
      .pipe(take(1))
      .subscribe((freelancerDetails) => {
        this.freelancerDetails = freelancerDetails;
        this.initializeFormWithData();
      });

    this.emptyDaysAWeekAndStartDateWhenAvailableIsSetToFalse();
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.close();
    });
    this.subs.add(subscription);
  }

  public ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  public close(): void {
    if (!this.isSavingDisabled()) {
      this.backAlertService.showAlert();
    } else {
      this.modalController.dismiss();
    }
  }

  public isSavingDisabled(): boolean {
    return (
      this.availabilityForm.invalid ||
      (this.availabilityForm.touched && this.availabilityForm.invalid) ||
      (this.originalAvailabilityFormData && !this.isFormChanged())
    );
  }

  public saveAvailability(): void {
    this.loadingRepository.presentLoading();
    const { available, availabilityDate, daysAWeek, teleworking, ownCar } = this.availabilityForm.getRawValue();

    const {
      willingToTravelAbroad,
      maximumTravelTime,
      maximumTravelDistance,
      dayFee,
      dayFeeNegotiable,
      hourlyFee,
      feeComment,
    } = this.freelancerDetails.jobPreferences;
    const jobPreferences = new JobPreferences(
      available,
      availabilityDate.slice(0, 10),
      daysAWeek,
      willingToTravelAbroad,
      maximumTravelTime,
      maximumTravelDistance,
      teleworking,
      ownCar,
      dayFee,
      dayFeeNegotiable,
      hourlyFee,
      feeComment
    );

    const updateJobPreferencesRequest = new UpdateFreelancerJobPreferencesRequest(
      this.freelancerDetails,
      jobPreferences
    );
    this.freelancerRepository
      .saveFreelancerDetails(updateJobPreferencesRequest)
      .pipe(take(1))
      .subscribe(
        (success: boolean) => {
          if (success) {
            this.events.publish(GighouseEvent.profileCompletenessChanged);
            this.loadingRepository.stopLoading();
            this.modalController.dismiss().then(() => this.toastService.presentSuccessToast());
          } else {
            this.loadingRepository.stopLoading();
            this.toastService.presentErrorToast('availability.toast.error-update-data');
          }
        },
        () => this.loadingRepository.stopLoading()
      );
  }

  private initializeFormWithData(): void {
    if (this.freelancerDetails) {
      this.availabilityForm.controls['available'].setValue(this.freelancerDetails.jobPreferences.available);
      this.availabilityForm.controls['availabilityDate'].setValue(
        this.freelancerDetails.jobPreferences.availabilityDate
      );
      this.availabilityForm.controls.daysAWeek.setValue(`${this.freelancerDetails.jobPreferences.daysAWeek}`);
      this.availabilityForm.controls['ownCar'].setValue(this.freelancerDetails.jobPreferences.hasOwnCar);
      this.availabilityForm.controls['teleworking'].setValue(this.freelancerDetails.jobPreferences.teleworking);
    }

    this.originalAvailabilityFormData = this.availabilityForm.getRawValue();
    this.formInitialized = true;
  }

  private createForm(): void {
    this.availabilityForm = this.formBuilder.group({
      available: [false, Validators.required],
      availabilityDate: [null, Validators.required],
      daysAWeek: [null, Validators.required],
      ownCar: [false, Validators.required],
      teleworking: [false, Validators.required],
    });
  }

  private isFormChanged(): boolean {
    return (
      this.formInitialized &&
      !this.helperService.isEqual(this.originalAvailabilityFormData, this.availabilityForm.getRawValue())
    );
  }

  private emptyDaysAWeekAndStartDateWhenAvailableIsSetToFalse(): void {
    this.availabilityForm.get('available').valueChanges.subscribe((available: boolean) => {
      if (!available) {
        this.availabilityForm.controls['availabilityDate'].setValue(null);
        this.availabilityForm.controls['daysAWeek'].setValue(null);
      }
    });
  }
}
