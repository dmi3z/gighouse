import { chainedInstruction } from '@angular/compiler/src/render3/view/util';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController, Platform } from '@ionic/angular';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';
import { FreelancerDetails } from 'app/interfaces/models/freelancer-details';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { JobPreferences } from 'app/interfaces/models/job-preferences';
import { MaximumTravelTime } from 'app/interfaces/models/maximum-travel-time';
import { UpdateFreelancerJobPreferencesRequest } from 'app/interfaces/models/update-freelancer';
import { BackAlertService } from 'app/services/back-alert.service';
import { CacheableMetadataService } from 'app/services/cacheable-metadata.service';
import { EventsService } from 'app/services/events.service';
import { FreelancerRepository } from 'app/services/freelancer.repository';
import { HelperService } from 'app/services/helper.service';
import { LoadingRepository } from 'app/services/loading.repository';
import { ToastService } from 'app/services/toast.service';
import { TranslationsService } from 'app/services/translations.service';
import { zip, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-mobility',
  templateUrl: './mobility.component.html',
  styleUrls: ['./mobility.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MobilityComponent implements OnInit, OnDestroy {
  public freelancerDetails: FreelancerDetails;
  public maximumTravelTimes: MaximumTravelTime[];
  public mobilityForm: FormGroup;
  public originalMobilityFormData: FormGroup;

  private formInitialized = false;
  private subs: Subscription = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private backAlertService: BackAlertService,
    private freelancerRepository: FreelancerRepository,
    private loadingRepository: LoadingRepository,
    private events: EventsService,
    private cacheableMetadataService: CacheableMetadataService,
    private platform: Platform,
    private modalController: ModalController,
    private helperService: HelperService
  ) {
    this.createForm();
  }

  private createForm() {
    this.mobilityForm = this.formBuilder.group({
      maximumTravelTime: [null],
      maximumTravelDistance: [null, Validators.required],
      travelAbroad: [false],
    });
    this.originalMobilityFormData = this.mobilityForm.getRawValue();
  }

  public ngOnInit(): void {
    this.initializeBackButtonHandler();
    zip(this.freelancerRepository.getFreelancerDetails(), this.cacheableMetadataService.getMaximumTravelTimes())
      .pipe(
        take(1),
        map(([freelancerDetails, maxTravelTimes]: [FreelancerDetails, MaximumTravelTime[]]) => {
          this.maximumTravelTimes = maxTravelTimes;
          this.freelancerDetails = freelancerDetails;
          this.initializeFormWithData();
        })
      )
      .subscribe(() => {
        this.originalMobilityFormData = this.mobilityForm.getRawValue();
      });
  }

  public saveMobility(): void {
    this.loadingRepository.presentLoading();
    const {
      available,
      availabilityDate,
      daysAWeek,
      teleworking,
      hasOwnCar,
      dayFee,
      dayFeeNegotiable,
      hourlyFee,
      feeComment,
    } = this.freelancerDetails.jobPreferences;
    const { travelAbroad, maximumTravelTime, maximumTravelDistance } = this.mobilityForm.getRawValue();
    const jobPreferences = new JobPreferences(
      available,
      availabilityDate,
      daysAWeek,
      travelAbroad,
      maximumTravelTime,
      maximumTravelDistance,
      teleworking,
      hasOwnCar,
      dayFee,
      dayFeeNegotiable,
      hourlyFee,
      feeComment
    );

    const updateJobPreferencesRequest = new UpdateFreelancerJobPreferencesRequest(
      this.freelancerDetails,
      jobPreferences
    );
    this.freelancerRepository
      .saveFreelancerDetails(updateJobPreferencesRequest)
      .pipe(take(1))
      .subscribe((success: boolean) => {
        if (success) {
          this.events.publish(GighouseEvent.profileCompletenessChanged);
          this.loadingRepository.stopLoading();
          this.modalController.dismiss().then(() => this.toastService.presentSuccessToast());
        } else {
          this.loadingRepository.stopLoading();
          this.toastService.presentErrorToast('mobility.toast.error-update-data');
        }
      });
  }

  private initializeFormWithData(): void {
    if (this.freelancerDetails) {
      this.mobilityForm.controls['maximumTravelTime'].setValue(this.freelancerDetails.jobPreferences.maximumTravelTime);
      const numberOfDecimals = 0;
      this.mobilityForm.controls['maximumTravelDistance'].setValue(
        this.freelancerDetails.jobPreferences.maximumTravelDistance
      );
      this.mobilityForm.controls['travelAbroad'].setValue(this.freelancerDetails.jobPreferences.willingToTravelAbroad);
    }
    this.formInitialized = true;
  }

  public isSavingDisabled(): boolean {
    return this.originalMobilityFormData && !this.isFormChanged();
  }

  private isFormChanged(): boolean {
    return (
      this.formInitialized &&
      !this.helperService.isEqual(this.originalMobilityFormData, this.mobilityForm.getRawValue())
    );
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.close();
    });
    this.subs.add(subscription);
  }

  public close(): void {
    if (!this.isSavingDisabled()) {
      this.backAlertService.showAlert();
    } else {
      this.modalController.dismiss();
    }
  }

  public ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
