import { HelperService } from './../../../../../services/helper.service';
import { ModalController, Platform } from '@ionic/angular';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { EventsService } from 'app/services/events.service';
import { FreelancerRepository } from 'app/services/freelancer.repository';
import { LoadingRepository } from 'app/services/loading.repository';
import { ToastService } from 'app/services/toast.service';
import { take } from 'rxjs/operators';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { UpdateFreelancerJobPreferencesRequest } from 'app/interfaces/models/update-freelancer';
import { JobPreferences } from 'app/interfaces/models/job-preferences';
import { FreelancerDetails } from 'app/interfaces/models/freelancer-details';
import { BackAlertService } from 'app/services/back-alert.service';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RateComponent implements OnInit, OnDestroy {
  public freelancerDetails: FreelancerDetails;
  public rateForm: FormGroup;
  public originalRateFormData: any;

  private formInitialized = false;
  private subs: Subscription = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private freelancerRepository: FreelancerRepository,
    private backAlertService: BackAlertService,
    private loadingRepository: LoadingRepository,
    private events: EventsService,
    private toastService: ToastService,
    private modalController: ModalController,
    private helperService: HelperService,
    private platform: Platform
  ) {}

  public ngOnInit(): void {
    this.initializeBackButtonHandler();
    this.createForm();
    this.freelancerRepository
      .getFreelancerDetails()
      .pipe(take(1))
      .subscribe((freelancerDetails) => {
        this.freelancerDetails = freelancerDetails;
        this.initializeFormWithData();
      });
  }

  public isSavingDisabled(): boolean {
    return (this.originalRateFormData && !this.isFormChanged()) || (this.rateForm.touched && this.rateForm.invalid);
  }

  public saveRate(): void {
    this.loadingRepository.presentLoading();
    const {
      available,
      availabilityDate,
      daysAWeek,
      willingToTravelAbroad,
      maximumTravelTime,
      maximumTravelDistance,
      teleworking,
      hasOwnCar,
    } = this.freelancerDetails.jobPreferences;

    const { dayFee, dayFeeNegotiable, hourlyFee, comment } = this.rateForm.getRawValue();
    const jobPreferences = new JobPreferences(
      available,
      availabilityDate,
      daysAWeek,
      willingToTravelAbroad,
      maximumTravelTime,
      maximumTravelDistance,
      teleworking,
      hasOwnCar,
      dayFee,
      dayFeeNegotiable,
      hourlyFee,
      comment
    );
    const updateJobPreferencesRequest = new UpdateFreelancerJobPreferencesRequest(
      this.freelancerDetails,
      jobPreferences
    );

    this.freelancerRepository
      .saveFreelancerDetails(updateJobPreferencesRequest)
      .pipe(take(1))
      .subscribe((success: boolean) => {
        if (success) {
          this.events.publish(GighouseEvent.profileCompletenessChanged);
          this.loadingRepository.stopLoading();
          this.modalController.dismiss().then(() => this.toastService.presentSuccessToast());
        } else {
          this.loadingRepository.stopLoading();
          this.toastService.presentErrorToast('rate.toast.error-update-data');
        }
      });
  }

  private initializeFormWithData(): void {
    if (this.freelancerDetails && this.freelancerDetails.jobPreferences) {
      this.rateForm.controls.dayFee.setValue(this.freelancerDetails.jobPreferences.dayFee);
      this.rateForm.controls.dayFeeNegotiable.setValue(this.freelancerDetails.jobPreferences.dayFeeNegotiable);
      this.rateForm.controls.hourlyFee.setValue(this.freelancerDetails.jobPreferences.hourlyFee);
      if (!this.freelancerDetails.jobPreferences.feeComment) {
        this.rateForm.controls.comment.setValue('');
      } else {
        this.rateForm.controls.comment.setValue(this.freelancerDetails.jobPreferences.feeComment);
      }
    }

    this.originalRateFormData = this.rateForm.getRawValue();
    this.formInitialized = true;
  }

  private createForm(): void {
    this.rateForm = this.formBuilder.group({
      dayFee: ['', [Validators.required, Validators.pattern('^[0-9]*([,.][0-9]*)?$')]],
      dayFeeNegotiable: [''],
      hourlyFee: [''],
      comment: [''],
    });
  }

  private isFormChanged(): boolean {
    return this.formInitialized && !this.helperService.isEqual(this.originalRateFormData, this.rateForm.getRawValue());
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.close();
    });
    this.subs.add(subscription);
  }

  public close(): void {
    if (!this.isSavingDisabled()) {
      this.backAlertService.showAlert();
    } else {
      this.modalController.dismiss();
    }
  }

  public ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
