import { PipesModule } from 'app/pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { PreferencesComponent } from './preferences.component';
import { RateComponent } from './rate/rate.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AvailabilityComponent } from './availability/availability.component';
import { MobilityComponent } from './mobility/mobility.component';

@NgModule({
  declarations: [PreferencesComponent, RateComponent, AvailabilityComponent, MobilityComponent],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    PipesModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: PreferencesComponent,
      },
    ]),
  ],
})
export class PreferencesModule {}
