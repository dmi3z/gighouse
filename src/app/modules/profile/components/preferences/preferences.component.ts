import { Router } from '@angular/router';
import { FreelancerRepository } from './../../../../services/freelancer.repository';
import { FreelancerDetails } from './../../../../interfaces/models/freelancer-details';
import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RateComponent } from './rate/rate.component';
import { AvailabilityComponent } from './availability/availability.component';
import { MobilityComponent } from './mobility/mobility.component';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreferencesComponent {
  freelancerDetails$: Observable<FreelancerDetails>;

  constructor(
    private freelancerRepository: FreelancerRepository,
    private router: Router,
    private modalController: ModalController
  ) {}

  public ngOnInit(): void {
    this.freelancerDetails$ = this.freelancerRepository.getFreelancerDetails();
  }

  public ngOnDestroy(): void {}

  public async openAvailabilityComponent(): Promise<void> {
    const modal = await this.modalController.create({
      component: AvailabilityComponent,
    });
    modal.present();
  }

  public async openRateComponent(): Promise<void> {
    const modal = await this.modalController.create({
      component: RateComponent,
    });
    modal.present();
  }

  public async openMobilityComponent(): Promise<void> {
    const modal = await this.modalController.create({
      component: MobilityComponent,
    });
    modal.present();
  }
}
