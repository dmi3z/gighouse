import {
  Component,
  ElementRef,
  ViewChildren,
  QueryList,
  OnChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';

@Component({
  selector: 'app-biography-content',
  templateUrl: './biography-content.component.html',
  styleUrls: ['./biography-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BiographyContentComponent implements OnChanges {
  @ViewChildren('biographyContent') biographyContent: QueryList<ElementRef>;
  @Input()
  public freelancerBiography: string;
  @Input()
  public showBiography = false;
  @Output()
  public showFullButton = new EventEmitter<boolean>();

  constructor() {}

  public ngOnChanges(): void {
    setTimeout(() => this.showFullButton.emit(this.updateHeight()), 100);
  }

  private updateHeight(): boolean {
    if (this.biographyContent) {
      return this.biographyContent.toArray()[0].nativeElement.clientHeight < 200 ? false : true;
    }
  }
}
