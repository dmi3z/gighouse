import { BackAlertService } from '../../../../../services/back-alert.service';
import { ToastService } from './../../../../../services/toast.service';
import { take } from 'rxjs/operators';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FreelancerRepository } from 'app/services/freelancer.repository';
import { FreelancerDetails } from 'app/interfaces/models/freelancer-details';
import { Platform, ModalController } from '@ionic/angular';
import { LoadingRepository } from 'app/services/loading.repository';
import { FreelancerBiography } from 'app/interfaces/models/freelancer-biography';
import { UpdateBiographyRequest } from 'app/interfaces/models/update-biography';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { HelperService } from 'app/services/helper.service';
import { EventsService } from 'app/services/events.service';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-biography-edit',
  templateUrl: './biography-edit.component.html',
  styleUrls: ['./biography-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BiographyEditComponent implements OnInit, OnDestroy {
  public biographyForm: FormGroup;
  private originalBiographyFormData: FormGroup;
  private backButtonSubscription: Subscription;

  freelancerDetails: FreelancerDetails;
  biography: FreelancerBiography;

  formInitialized = false;

  public currentTextLength = 0;
  public maxTextLength = 2000;

  constructor(
    private formBuilder: FormBuilder,
    private freelancerRepository: FreelancerRepository,
    private backAlertService: BackAlertService,
    private loadingRepository: LoadingRepository,
    private events: EventsService,
    private toastService: ToastService,
    private platform: Platform,
    private helperService: HelperService,
    private modalController: ModalController,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initializeBackButtonHandler();

    this.freelancerRepository
      .getFreelancerDetails()
      .pipe(take(1))
      .subscribe((freelancerDetails) => {
        this.freelancerDetails = freelancerDetails;
      });
    this.freelancerRepository
      .getBiography()
      .pipe(take(1))
      .subscribe((biography) => {
        this.biography = biography;
        this.currentTextLength = biography.biography.length;
        this.changeDetectorRef.markForCheck();
        this.initializeForm();
      });
  }

  public async navigateBack(): Promise<void> {
    if (!this.isSavingDisabled()) {
      this.backAlertService.showAlert();
    } else {
      this.modalController.dismiss();
    }
  }

  public saveBiography(): void {
    this.loadingRepository.presentLoading();
    const updateBiographyRequest = new UpdateBiographyRequest(this.biographyForm.controls.biography.value);
    this.freelancerRepository
      .saveBiography(this.freelancerDetails.freelancerId, updateBiographyRequest)
      .pipe(take(1))
      .subscribe((success: boolean) => {
        if (success) {
          this.events.publish(GighouseEvent.profileCompletenessChanged);
          this.loadingRepository.stopLoading();
          this.modalController.dismiss();
          this.toastService.presentSuccessToast();
        } else {
          this.toastService.presentErrorToast('biography-edit.toast.error-save-data');
          this.loadingRepository.stopLoading();
        }
      });
  }

  public isSavingDisabled(): boolean {
    return this.originalBiographyFormData && !this.isFormChanged();
  }

  private initializeForm(): void {
    if (this.biography) {
      this.biographyForm = this.formBuilder.group({
        biography: [this.biography.biography, Validators.maxLength(2000)],
      });
      this.originalBiographyFormData = this.biographyForm.getRawValue();
      this.formInitialized = true;
    }
    this.biographyForm.controls.biography.valueChanges.pipe().subscribe((value) => {
      this.currentTextLength = value.length;
    });
  }

  private isFormChanged(): boolean {
    return (
      this.formInitialized &&
      !this.helperService.isEqual(this.originalBiographyFormData, this.biographyForm.getRawValue())
    );
  }

  private initializeBackButtonHandler(): void {
    this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.navigateBack();
    });
  }

  public ngOnDestroy(): void {
    this.backButtonSubscription.unsubscribe();
  }
}
