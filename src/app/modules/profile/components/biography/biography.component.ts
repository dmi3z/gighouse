import { ModalController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { FreelancerRepository } from './../../../../services/freelancer.repository';
import { FreelancerBiography } from './../../../../interfaces/models/freelancer-biography';
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BiographyEditComponent } from './biography-edit/biography-edit.component';

@Component({
  selector: 'app-biography',
  templateUrl: 'biography.component.html',
  styleUrls: ['biography.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BiographyComponent implements OnInit, OnDestroy {
  freelancerBiography: FreelancerBiography;
  public showBiography: boolean;
  public showFullButton: boolean;
  public isBiographyLoading = true;

  constructor(
    private freelancerRepository: FreelancerRepository,
    private modalController: ModalController,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.loadBiography();
  }

  ngOnDestroy(): void {}

  public updateShowFullButton(value): void {
    this.showFullButton = value;
  }

  async openBiographyComponent(): Promise<void> {
    const modal = await this.modalController.create({
      component: BiographyEditComponent,
    });
    modal.present();

    modal.onDidDismiss().then(() => this.loadBiography());
  }

  public hasBiography(): boolean {
    return (
      this.freelancerBiography != null &&
      this.freelancerBiography.biography != null &&
      this.freelancerBiography.biography.length > 0
    );
  }

  public showFullBiography(): void {
    this.showBiography = true;
  }

  private loadBiography(): void {
    this.freelancerRepository
      .getBiography()
      .pipe(take(1))
      .subscribe((biography) => {
        this.changeDetectorRef.markForCheck();
        this.freelancerBiography = biography;
        this.isBiographyLoading = false;
      });
  }
}
