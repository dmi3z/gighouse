import { BiographyEditComponent } from './biography-edit/biography-edit.component';
import { PipesModule } from 'app/pipes/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BiographyContentComponent } from './biography-content/biography-content.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { BiographyComponent } from './biography.component';

@NgModule({
  declarations: [BiographyComponent, BiographyContentComponent, BiographyEditComponent],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    PipesModule,
    RouterModule.forChild([
      {
        path: '',
        component: BiographyComponent,
      },
    ]),
    ReactiveFormsModule
  ],
})
export class BiographyModule {}
