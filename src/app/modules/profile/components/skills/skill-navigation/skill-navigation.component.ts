import { LoadingRepository } from './../../../../../services/loading.repository';
import { BackAlertService } from 'app/services/back-alert.service';
import { Component, Input, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { FreelancerSkill } from 'app/interfaces/models/freelancer-skill';
import { FreelancerSkillsRepository } from 'app/modules/profile/services/freelancer.skills.repository';
import { take } from 'rxjs/operators';
import { EventsService } from 'app/services/events.service';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { ToastService } from 'app/services/toast.service';
import { HelperService } from 'app/services/helper.service';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-skill-navigation',
  templateUrl: 'skill-navigation.component.html',
  styleUrls: ['skill-navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkillNavigationComponent implements OnInit, OnDestroy {
  @Input() skill: FreelancerSkill;

  public copySkill: FreelancerSkill[];
  public copyTools: FreelancerSkill[];
  public isTools: boolean;
  public stars: any;

  private subs: Subscription = new Subscription();

  constructor(
    private modalController: ModalController,
    private backAlertService: BackAlertService,
    private freelancerSkillsRepository: FreelancerSkillsRepository,
    private loadingRepository: LoadingRepository,
    private events: EventsService,
    private toastService: ToastService,
    private helper: HelperService,
    private platform: Platform,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.initializeBackButtonHandler();
    this.copySkill = this.helper.cloneDeepArray(this.skill.skills);
    this.copyTools = this.helper.cloneDeepArray(this.skill.tools);
  }

  public isSavingDisabled(): boolean {
    return this.freelancerSkillsRepository.hasChanges();
  }

  public saveData(): void {
    this.loadingRepository.presentLoading();
    this.freelancerSkillsRepository
      .saveSkills()
      .pipe(take(1))
      .subscribe(() => {
        this.events.publish(GighouseEvent.profileCompletenessChanged);
        this.freelancerSkillsRepository.initialiseFreelancerSkills();
        this.loadingRepository.stopLoading();
        this.modalController.dismiss().then(() => this.toastService.presentSuccessToast());
      });
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.close();
    });
    this.subs.add(subscription);
  }

  public close(): void {
    if (!this.isSavingDisabled()) {
      this.backAlertService.showAlert().then((data) => {
        if (data) {
          this.changeDetectorRef.markForCheck();
          this.skill.skills.forEach(
            (skill) => (skill.rating = this.copySkill.find((copySkill) => (copySkill.id = skill.id)).rating)
          );
          this.skill.tools = this.helper.cloneDeepArray(this.copyTools);
        }
      });
    } else {
      this.modalController.dismiss();
    }
  }

  public changeIsTools(sign: boolean): void {
    this.isTools = sign;
  }

  public ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
