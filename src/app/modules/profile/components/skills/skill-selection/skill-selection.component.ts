import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FreelancerSkill } from 'app/interfaces/models/freelancer-skill';

@Component({
  selector: 'app-skill-selection',
  templateUrl: './skill-selection.component.html',
  styleUrls: ['./skill-selection.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkillSelectionComponent {
  @Input() skillList: FreelancerSkill[];

  constructor() {}
}
