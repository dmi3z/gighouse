import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StarRatingComponent implements OnInit {
  readonly CHECKED_STAR_CLASSNAME = 'checked-star';
  readonly UNCHECKED_STAR_CLASSNAME = 'unchecked-star';

  @Input() rating: number;
  @Input() amountOfStars: number;

  @Output() rateEvent = new EventEmitter();
  amountOfStarsArray: number[];

  ngOnInit(): void {
    this.amountOfStarsArray = Array(this.amountOfStars)
      .fill(null, 0, this.amountOfStars)
      .map((x, i) => i + 1);
  }

  rate(clickedStar: number) {
    if (this.rating === clickedStar) {
      this.rating -= 1;
    } else {
      this.rating = clickedStar;
    }
    this.rateEvent.emit(this.rating);
  }

  private isBelowOrEqualToRating(clickedStar: number): boolean {
    return clickedStar <= this.rating;
  }
}
