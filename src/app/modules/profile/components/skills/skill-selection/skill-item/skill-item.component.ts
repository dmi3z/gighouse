import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FreelancerSkill } from 'app/interfaces/models/freelancer-skill';

@Component({
  selector: 'app-skill-item',
  templateUrl: './skill-item.component.html',
  styleUrls: ['./skill-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkillItemComponent {
  @Input()
  skill: FreelancerSkill;

  readonly AMOUNT_OF_STARS = 5;
  categoryOpen = false;

  public updateFreelancerSkillRating(rating: number): void {
    this.skill.rating = rating;
  }

  public toggleCategoryOpen(): void {
    this.categoryOpen = !this.categoryOpen;
  }

  public isSkillACategory(): boolean {
    return this.skill.skills.length > 0 || this.skill.tools.length > 0;
  }

  public getSubSkills(): FreelancerSkill[] {
    return this.skill.skills.concat(this.skill.tools);
  }
}
