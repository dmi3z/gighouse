import { HelperService } from 'app/services/helper.service';
import { Component, Input, OnDestroy, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';
import { FreelancerLanguage } from 'app/interfaces/models/freelancer-language';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { LanguageSkillLevel } from 'app/interfaces/models/language-skill-level';
import { FreelancerLanguagesRepository } from 'app/modules/profile/services/freelancer.languages.repository';
import { BackAlertService } from 'app/services/back-alert.service';
import { CacheableMetadataService } from 'app/services/cacheable-metadata.service';
import { EventsService } from 'app/services/events.service';
import { LoadingRepository } from 'app/services/loading.repository';
import { ToastService } from 'app/services/toast.service';
import { TranslationsService } from 'app/services/translations.service';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LanguagesComponent implements OnInit, OnDestroy {
  @Input() languages: FreelancerLanguage[];

  public languageSkillLevels: LanguageSkillLevel[];

  private subs: Subscription = new Subscription();

  constructor(
    private cacheableMetadataService: CacheableMetadataService,
    private freelancerLanguagesRepository: FreelancerLanguagesRepository,
    private loadingRepository: LoadingRepository,
    private toastService: ToastService,
    private backAlertService: BackAlertService,
    private events: EventsService,
    private platform: Platform,
    private modalController: ModalController,
    private helper: HelperService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.initializeBackButtonHandler();
    this.cacheableMetadataService
      .getLanguageSkillLevels()
      .pipe(take(1))
      .subscribe((languageSkillLevels: LanguageSkillLevel[]) => {
        this.changeDetectorRef.markForCheck();
        this.languageSkillLevels = languageSkillLevels;
      });
  }

  public selectionChanged(language: FreelancerLanguage): void {
    this.freelancerLanguagesRepository.addFreelancerLanguage(language);
  }

  public isSavingDisabled(): boolean {
    return this.freelancerLanguagesRepository.hasChanges();
  }

  public saveData(): void {
    this.loadingRepository.presentLoading();

    this.freelancerLanguagesRepository
      .saveLanguages()
      .pipe(take(1))
      .subscribe(() => {
        this.events.publish(GighouseEvent.profileCompletenessChanged);
        this.freelancerLanguagesRepository.initialiseFreelancerLanguages();
        this.loadingRepository.stopLoading();
        this.modalController.dismiss().then(() => this.toastService.presentSuccessToast());
      });
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.close();
    });
    this.subs.add(subscription);
  }

  public close(): void {
    if (!this.isSavingDisabled()) {
      this.backAlertService.showAlert().then((data) => {
        if (data) {
          this.freelancerLanguagesRepository.revertChanges();
        }
      });
    } else {
      this.modalController.dismiss();
    }
  }

  public ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
