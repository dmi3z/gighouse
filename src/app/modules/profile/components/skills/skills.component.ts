import { LoadingRepository } from './../../../../services/loading.repository';
import { Observable, pipe } from 'rxjs';
import { ChangeDetectionStrategy, Component, ChangeDetectorRef } from '@angular/core';
import { FreelancerLanguage } from 'app/interfaces/models/freelancer-language';
import { FreelancerSkill } from 'app/interfaces/models/freelancer-skill';
import { FreelancerLanguagesRepository } from '../../services/freelancer.languages.repository';
import { FreelancerSkillsRepository } from '../../services/freelancer.skills.repository';
import { map } from 'rxjs/operators';
import { ModalController } from '@ionic/angular';
import { LanguagesComponent } from './languages/languages.component';
import { SkillNavigationComponent } from './skill-navigation/skill-navigation.component';

@Component({
  selector: 'app-skills',
  templateUrl: 'skills.component.html',
  styleUrls: ['skills.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkillsComponent {
  public freelancerSkills$: Observable<FreelancerSkill[]>;
  public knownLanguages = 0;
  public freelancerLanguages: FreelancerLanguage[];
  constructor(
    private freelancerSkillsRepository: FreelancerSkillsRepository,
    private freelancerLanguagesRepository: FreelancerLanguagesRepository,
    private modalController: ModalController,
    private loadingRepository: LoadingRepository,
    private changeDetectorRef: ChangeDetectorRef
  ) {}
  public ngOnInit(): void {
    this.loadingRepository.presentLoading();
    this.freelancerSkills$ = this.freelancerSkillsRepository.getFreelancerSkillsHasBeenChanged().pipe(
      map(() => {
        this.loadingRepository.stopLoading();
        return this.freelancerSkillsRepository.getFreelancerSkills();
      })
    );

    this.freelancerLanguagesRepository.getFreelancerLanguagesHasBeenChanged().subscribe(() => {
      this.changeDetectorRef.markForCheck();
      this.freelancerLanguages = this.freelancerLanguagesRepository.getFreelancerLanguages();
      this.knownLanguages = this.freelancerLanguagesRepository.countKnownLanguages();
    });
    this.freelancerSkillsRepository.initialiseFreelancerSkills();
    this.freelancerLanguagesRepository.initialiseFreelancerLanguages();
  }

  public ngOnDestroy(): void {}
  public ionViewWillEnter() {
    this.knownLanguages = this.freelancerLanguagesRepository.countKnownLanguages();
  }
  public async openSkill(skill: FreelancerSkill): Promise<void> {
    const modal = await this.modalController.create({
      component: SkillNavigationComponent,
      componentProps: {
        skill,
      },
    });

    modal.present();
  }
  public async openLanguages(): Promise<void> {
    const modal = await this.modalController.create({
      component: LanguagesComponent,
      componentProps: {
        languages: this.freelancerLanguages,
      },
    });

    modal.present();
  }
}
