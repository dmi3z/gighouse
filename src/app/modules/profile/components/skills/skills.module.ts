import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { SkillsComponent } from './skills.component';
import { SkillCategoryComponent } from './skill-categoty/skill-category.component';
import { FreelancerSkillsRepository } from '../../services/freelancer.skills.repository';
import { FreelancerLanguagesRepository } from '../../services/freelancer.languages.repository';
import { LanguagesComponent } from './languages/languages.component';
import { FormBuilder, FormsModule } from '@angular/forms';
import { SkillNavigationComponent } from './skill-navigation/skill-navigation.component';
import { SkillSelectionComponent } from './skill-selection/skill-selection.component';
import { SkillItemComponent } from './skill-selection/skill-item/skill-item.component';
import { StarRatingComponent } from './skill-selection/star-rating/star-rating.component';

@NgModule({
  declarations: [
    SkillsComponent,
    LanguagesComponent,
    SkillCategoryComponent,
    SkillSelectionComponent,
    SkillItemComponent,
    StarRatingComponent,
    SkillNavigationComponent,


  ],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    FormsModule,
    // SkillNavigationComponent,
    RouterModule.forChild([
      {
        path: '',
        component: SkillsComponent,
      },
    ]),
  ],
  providers: [FreelancerSkillsRepository, FreelancerLanguagesRepository],
})
export class SkillsModule {}
