import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FreelancerSkill } from 'app/interfaces/models/freelancer-skill';

@Component({
  selector: 'app-skill-category',
  templateUrl: './skill-category.component.html',
  styleUrls: ['./skill-category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkillCategoryComponent implements OnInit {
  @Input() public skill: FreelancerSkill;
  @Output() public openSkillEvent = new EventEmitter<FreelancerSkill>();
  public amountOfSkills: number;

  constructor() {}

  ngOnInit(): void {
    this.amountOfSkills = this.skill.getAmountOfSkills();
  }

  public openSkill(): void {
    this.openSkillEvent.next(this.skill);
  }
}
