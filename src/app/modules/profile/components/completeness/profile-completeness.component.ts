import { ChangeDetectionStrategy, Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ProfileCompleteness } from 'app/interfaces/models/profile-completeness';

@Component({
  selector: 'app-profile-completeness',
  templateUrl: './profile-completeness.component.html',
  styleUrls: ['profile-completeness.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileCompletenessComponent {
  profileCompleteness: ProfileCompleteness;
  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.changeDetectorRef.markForCheck();
    this.profileCompleteness = this.navParams.get('profileCompleteness');
  }
  closePopover() {
    this.modalController.dismiss();
  }
}
