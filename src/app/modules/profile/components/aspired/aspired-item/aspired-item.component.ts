import { AspiredFreelancerProfilesByExpertise } from './../../../../../interfaces/models/aspired-freelancer-profiles-by-expertise';
import { Expertise } from './../../../../../interfaces/models/expertise';
import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-aspired-item',
  templateUrl: './aspired-item.component.html',
  styleUrls: ['./aspired-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AspiredItemComponent implements OnInit {
  @Input() public expertise: AspiredFreelancerProfilesByExpertise;
  @Output() public openExpertise = new EventEmitter<AspiredFreelancerProfilesByExpertise>();
  public counter = 0;

  constructor() {}

  public ngOnInit(): void {
    this.counter = this.expertise.countSelectedProfiles();
  }

  public open(): void {
    this.openExpertise.next(this.expertise);
  }
}
