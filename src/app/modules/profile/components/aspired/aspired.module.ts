import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { OtherAspiredProfileRepository } from './../../services/other-aspired-profile.repository';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { AspiredComponent } from './aspired.component';
import { AspiredFreelancerProfilesRepository } from '../../services/aspired.freelancer.profiles.repository';
import { AspiredItemComponent } from './aspired-item/aspired-item.component';
import { AspiredFreelancerProfilesOtherComponent } from './aspired-other/aspired-freelancer-profiles-other.component';
import { AspiredFreelancerProfilesSelectionComponent } from './aspired-section/aspired-freelancer-profiles-selection.component';

@NgModule({
  declarations: [
    AspiredComponent,
    AspiredItemComponent,
    AspiredFreelancerProfilesOtherComponent,
    AspiredFreelancerProfilesSelectionComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: AspiredComponent,
      },
    ]),
  ],
  providers: [AspiredFreelancerProfilesRepository, OtherAspiredProfileRepository],
})
export class AspiredModule {}
