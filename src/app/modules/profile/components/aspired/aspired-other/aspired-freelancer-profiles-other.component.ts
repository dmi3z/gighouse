import { BackAlertService } from '../../../../../services/back-alert.service';
import { ModalController, Platform } from '@ionic/angular';
import { HelperService } from './../../../../../services/helper.service';
import { GighouseEvent } from './../../../../../interfaces/models/gighouse-event';
import { LoadingRepository } from './../../../../../services/loading.repository';
import { EventsService } from './../../../../../services/events.service';
// import { TranslationsService } from './../../../../../services/translations.service';
import { ToastService } from './../../../../../services/toast.service';
import { OtherAspiredProfileRepository } from './../../../services/other-aspired-profile.repository';
import { StringControl } from './../../../../registration/controls/string.control';
import { OtherAspiredProfile } from './../../../../../interfaces/models/other-aspired-profile';
import { Component, OnInit, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';

import { tap, take } from 'rxjs/operators';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';

@Component({
  selector: 'app-aspired-freelancer-profiles-other',
  templateUrl: './aspired-freelancer-profiles-other.component.html',
  styleUrls: ['aspired-freelancer-profiles-other.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AspiredFreelancerProfilesOtherComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public otherAspiredProfile$: Observable<OtherAspiredProfile>;

  private originalFormData: FormGroup;
  private subs: Subscription = new Subscription();

  constructor(
    private otherAspiredProfileRepository: OtherAspiredProfileRepository,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private loadingRepository: LoadingRepository,
    private events: EventsService,
    private helper: HelperService,
    private modalController: ModalController,
    private backAlertService: BackAlertService,
    private platform: Platform,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.initializeBackButtonHandler();
    this.form = this.formBuilder.group({
      other: [''],
    });

    this.otherAspiredProfile$ = this.otherAspiredProfileRepository.getOtherAspiredProfile().pipe(
      take(1),
      tap((otherAspiredProfile) => {
        if (otherAspiredProfile.other) {
          this.form.controls.other.patchValue(otherAspiredProfile.other);
        } else {
          this.form.controls.other.patchValue('');
        }
        this.originalFormData = this.form.getRawValue();
        this.changeDetectorRef.detectChanges();
      })
    );
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.close();
    });
    this.subs.add(subscription);
  }

  public close(): void {
    if (this.hasChanges()) {
      this.backAlertService.showAlert();
    } else {
      this.modalController.dismiss();
    }
  }

  public hasChanges(): boolean {
    return !this.helper.isEqual(this.form.getRawValue(), this.originalFormData);
  }

  public saveData(): void {
    this.loadingRepository.presentLoading();

    this.otherAspiredProfileRepository
      .saveOtherAspiredProfile(this.form.controls.other.value)
      .pipe(take(1))
      .subscribe(() => {
        this.events.publish(GighouseEvent.profileCompletenessChanged);
        this.loadingRepository.stopLoading();
        this.modalController.dismiss();
        this.toastService.presentSuccessToast();
      });
  }

  public ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
