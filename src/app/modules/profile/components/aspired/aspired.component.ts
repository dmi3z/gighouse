import { LoadingRepository } from './../../../../services/loading.repository';
import { ModalController } from '@ionic/angular';
import { EventsService } from './../../../../services/events.service';
import { OtherAspiredProfileRepository } from './../../services/other-aspired-profile.repository';
import { AspiredFreelancerProfilesByExpertise } from './../../../../interfaces/models/aspired-freelancer-profiles-by-expertise';
import { Subject } from 'rxjs';
import { ChangeDetectionStrategy, Component, ChangeDetectorRef } from '@angular/core';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { takeUntil } from 'rxjs/operators';
import { AspiredFreelancerProfilesRepository } from '../../services/aspired.freelancer.profiles.repository';
import { AspiredFreelancerProfilesOtherComponent } from './aspired-other/aspired-freelancer-profiles-other.component';
import { AspiredFreelancerProfilesSelectionComponent } from './aspired-section/aspired-freelancer-profiles-selection.component';

@Component({
  selector: 'app-aspired',
  templateUrl: 'aspired.component.html',
  styleUrls: ['aspired.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AspiredComponent {
  public aspiredFreelancerProfilesByExpertise: AspiredFreelancerProfilesByExpertise[];
  public otherAspiredProfileFilledIn = false;

  private destroy$ = new Subject();

  constructor(
    private aspiredFreelancerProfilesRepository: AspiredFreelancerProfilesRepository,
    private otherAspiredProfileRepository: OtherAspiredProfileRepository,
    private events: EventsService,
    private modalController: ModalController,
    private changeDetectorRef: ChangeDetectorRef,
    private loadingRepository: LoadingRepository
  ) {}

  ngOnInit(): void {
    this.loadingRepository.presentLoading();
    this.aspiredFreelancerProfilesRepository
      .getAspiredFreelancerProfilesHasBeenChanged()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.aspiredFreelancerProfilesByExpertise = this.aspiredFreelancerProfilesRepository.getAspiredFreelancerProfiles();
        this.loadingRepository.stopLoading();
        this.changeDetectorRef.markForCheck();
      });
    this.aspiredFreelancerProfilesRepository.initialiseAspiredFreelancerProfiles();
    this.otherAspiredProfileRepository.getOtherAspiredProfile();

    this.events.subscribe(GighouseEvent.otherAspiredProfileRefreshed, () => {
      this.checkIfOtherIsFilledIn();
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.events.destroy(GighouseEvent.otherAspiredProfileRefreshed);
  }

  private checkIfOtherIsFilledIn(): void {
    this.otherAspiredProfileRepository
      .getOtherAspiredProfile()
      .pipe(takeUntil(this.destroy$))
      .subscribe((otherAspiredProfile) => (this.otherAspiredProfileFilledIn = !otherAspiredProfile.other));
  }

  public async openAspiredFreelancerProfiles(data: AspiredFreelancerProfilesByExpertise): Promise<void> {
    const modal = await this.modalController.create({
      component: AspiredFreelancerProfilesSelectionComponent,
      componentProps: { data },
    });
    modal.present();
  }

  public async openOtherAspiredFreelancerProfile(): Promise<void> {
    const modal = await this.modalController.create({
      component: AspiredFreelancerProfilesOtherComponent,
    });
    modal.present();
  }
}
