import { HelperService } from './../../../../../services/helper.service';
import { AspiredFreelancerProfilesOtherComponent } from './../aspired-other/aspired-freelancer-profiles-other.component';
import { ModalController, Platform } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { GighouseEvent } from './../../../../../interfaces/models/gighouse-event';
import { EventsService } from './../../../../../services/events.service';
import { LoadingRepository } from './../../../../../services/loading.repository';
import { ToastService } from './../../../../../services/toast.service';
import { AspiredFreelancerProfilesRepository } from './../../../services/aspired.freelancer.profiles.repository';
import { AspiredFreelancerProfilesByExpertise } from './../../../../../interfaces/models/aspired-freelancer-profiles-by-expertise';
import { Component, Input, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { BackAlertService } from 'app/services/back-alert.service';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-aspired-freelancer-profiles-selection',
  templateUrl: './aspired-freelancer-profiles-selection.component.html',
  styleUrls: ['aspired-freelancer-profiles-selection.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AspiredFreelancerProfilesSelectionComponent implements OnInit, OnDestroy {
  @Input() public data: AspiredFreelancerProfilesByExpertise;
  private subs: Subscription = new Subscription();

  public copyData: AspiredFreelancerProfilesByExpertise;

  constructor(
    private platform: Platform,
    private aspiredFreelancerProfilesRepository: AspiredFreelancerProfilesRepository,
    private toastService: ToastService,
    private loadingRepository: LoadingRepository,
    private events: EventsService,
    private modalController: ModalController,
    private backAlertService: BackAlertService,
    private helper: HelperService
  ) {}

  public ngOnInit(): void {
    this.copyData = AspiredFreelancerProfilesByExpertise.from(this.data);
    this.initializeBackButtonHandler();
  }

  public isSavingDisabled(): boolean {
    return this.helper.isEqual(this.data, this.copyData);
  }

  public saveData(): void {
    this.loadingRepository.presentLoading();
    this.aspiredFreelancerProfilesRepository.rebuildArray(this.data, this.copyData);
    this.aspiredFreelancerProfilesRepository
      .save()
      .pipe(take(1))
      .subscribe(() => {
        this.events.publish(GighouseEvent.profileCompletenessChanged);
        this.aspiredFreelancerProfilesRepository.initialiseAspiredFreelancerProfiles();
        this.loadingRepository.stopLoading();
        this.toastService.presentSuccessToast();
        this.modalController.dismiss();
      });
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.close();
    });
    this.subs.add(subscription);
  }

  public close(): void {
    if (!this.isSavingDisabled()) {
      this.backAlertService.showAlert();
    } else {
      this.modalController.dismiss();
    }
  }

  public async openOtherAspiredProfilePage(): Promise<void> {
    const modal = await this.modalController.create({
      component: AspiredFreelancerProfilesOtherComponent,
    });
    await modal.present();
  }

  public ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
