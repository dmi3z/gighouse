import { Router } from '@angular/router';
import { SettingsComponent } from './../settings/settings.component';
import { JobType } from 'app/interfaces/job-type';
import { Job } from './../../interfaces/models/job';
import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { StorageService } from 'app/services/storage.service';
import { JobService } from 'app/services/job.service';
import { ModalController } from '@ionic/angular';
import { JobDetailComponent } from '../assignments/components/job-detail/job-detail.component';
import { LoadingRepository } from 'app/services/loading.repository';
import { tap, map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { AppState } from '@capacitor/core';
import { getTotalUnreadMessages } from 'app/store/chat/chat.selectors';

@Component({
  selector: 'app-interests',
  templateUrl: 'interests.page.html',
  styleUrls: ['interests.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InterestsPage implements OnInit {
  public interestedJobs$: Observable<Job[]>;
  public isHaveUnreadMessages$: Observable<boolean>;

  constructor(
    private storageService: StorageService,
    private jobService: JobService,
    private modalController: ModalController,
    private router: Router,
    private loading: LoadingRepository,
    private store$: Store<AppState>,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.isHaveUnreadMessages$ = this.store$.pipe(
      select(getTotalUnreadMessages()),
      map((res) => res > 0)
    );
  }

  public ionViewDidEnter(): void {
    this.fetchInterestedJobs();
  }

  public async openSettingsPage(): Promise<void> {
    const modal = await this.modalController.create({
      component: SettingsComponent,
    });
    modal.present();
  }

  public openChatList(): void {
    this.router.navigate(['tabs/chat-list']);
  }

  public async openJobDetail(jobId: string) {
    const jobType = JobType.possibleJob;
    const modal = await this.modalController.create({
      component: JobDetailComponent,
      componentProps: {
        jobId,
        jobType,
        isCanRate: false,
      },
    });

    modal.onWillDismiss().then(({ data }) => {
      if (data && data.isUpdate) {
        this.fetchInterestedJobs();
      }
    });

    return await modal.present();
  }

  private fetchInterestedJobs(): void {
    this.loading.presentLoading();
    const freelancerId = this.storageService.getFreelancerId();
    this.changeDetectorRef.markForCheck();
    this.interestedJobs$ = this.jobService.getInterestedJobsForFreelancer(freelancerId).pipe(
      tap(() => {
        this.loading.stopLoading();
      })
    );
  }
}
