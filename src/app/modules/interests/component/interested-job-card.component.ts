import { ChatParticipantsComponent } from './../../chat-list/components/chat-participants/chat-participants.component';
import { MessagesListComponent } from './../../chat-list/components/message-list/messages-list.component';
import { LoadingRepository } from './../../../services/loading.repository';
import { ModalController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { StorageService } from './../../../services/storage.service';
import { ConversationBody, ChatItem } from './../../chat-list/interfaces/chat.interface';
import { ChatService } from './../../../services/chat.service';
import { Job } from './../../../interfaces/models/job';
import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-interested-job-card',
  templateUrl: './interested-job-card.component.html',
  styleUrls: ['./interested-job-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InterestedJobCardComponent {
  @Input()
  public job: Job;
  @Output() public openEvent = new EventEmitter<string>();

  public name: string;
  public teaserText1: string;
  public teaserText2: string;
  public teaserText3: string;
  public location: string;
  public fee: number;
  public startDate: string;
  public daysAWeek: number;

  constructor(
    private chatService: ChatService,
    private storageService: StorageService,
    private modalController: ModalController,
    private loadingRepository: LoadingRepository
  ) {}

  public ngOnInit(): void {
    this.name = this.job.getName();
    this.teaserText1 = this.job.getTeaserText1();
    this.teaserText2 = this.job.getTeaserText2();
    this.teaserText3 = this.job.getTeaserText3();
    this.location = this.job.getLocation();
    this.fee = this.job.getFee();
    this.startDate = this.job.getStartDate();
    this.daysAWeek = this.job.getDaysAWeek();
  }

  public openDescription(): void {
    this.openEvent.next(this.job.getId());
  }

  public async openChat(event: Event): Promise<void> {
    event.stopPropagation();

    const project: ChatItem = {
      projectId: this.job.getId(),
      projectName: this.job.getName(),
      accountName: '',
    };

    const modal = await this.modalController.create({
      component: ChatParticipantsComponent,
      componentProps: {
        project,
      },
    });
    await modal.present();
  }
}
