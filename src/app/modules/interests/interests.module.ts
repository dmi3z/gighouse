import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InterestsPage } from './interests.page';

import { InterestsPageRoutingModule } from './interests-routing.module';
import { InterestedJobCardComponent } from './component/interested-job-card.component';
import { PipesModule } from 'app/pipes/pipes.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    InterestsPageRoutingModule,
    TranslateModule,
    PipesModule,
  ],
  declarations: [InterestsPage, InterestedJobCardComponent],
})
export class InterestsPageModule {}
