import { RegistrationTeaserJobComponent } from './components/registration-teaser-job/registration-teaser-job.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration.component';
import { NgModule } from '@angular/core';
import { RegistrationNameComponent } from './components/registration-name/registration-name.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RegistrationEmailComponent } from './components/registration-email/registration-email.component';
import { RegistrationPhoneComponent } from './components/registration-phone/registration-phone.component';
import { RegistrationInfoComponent } from './components/registration-info/registration-info.component';

@NgModule({
  declarations: [
    RegistrationComponent,
    RegistrationNameComponent,
    RegistrationEmailComponent,
    RegistrationPhoneComponent,
    RegistrationInfoComponent,
    RegistrationTeaserJobComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: RegistrationComponent,
        children: [
          {
            path: 'registration-name',
            component: RegistrationNameComponent,
          },
          {
            path: 'registration-email',
            component: RegistrationEmailComponent,
          },
          {
            path: 'registration-phone',
            component: RegistrationPhoneComponent,
          },
          {
            path: 'registration-info',
            component: RegistrationInfoComponent,
          },
          {
            path: 'registration-teaser-job',
            component: RegistrationTeaserJobComponent,
          },
        ],
      },
    ]),
  ],
})
export class RegistrationModule {}
