import { AbstractControl } from '@angular/forms';

export const trimLengthValidator = (minLength: number) => (
  control: AbstractControl,
): { [key: string]: boolean } | null => {
  if (!control || !control.value) {
    return null;
  }
  return control.value.trim().length >= minLength ? null : { errorShort: true };
};
