import { ValidatorFn, Validators } from '@angular/forms';
import { AbstractFormControl } from './abstract.form.control';

export class PasswordControl extends AbstractFormControl<string> {
  private readonly validators: ValidatorFn[] = [];

  constructor() {
    super();
    this.validators = [
      Validators.required,
      Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$'),
    ];

    this.initialiseFormControlWithValidators('', this.validators);
  }

  getPatternErrorMessage(): string {
    return 'validation.password';
  }
}
