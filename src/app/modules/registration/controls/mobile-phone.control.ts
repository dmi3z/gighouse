import { ValidatorFn, Validators } from '@angular/forms';
import { AbstractFormControl } from './abstract.form.control';

export class MobilePhoneControl extends AbstractFormControl<string> {
  private readonly validators: ValidatorFn[] = [];

  constructor(initialValue: string, required: boolean) {
    super();

    this.validators = [Validators.pattern('^\\+[1-9]\\d{10,14}$')];

    if (required) {
      this.validators.push(Validators.required);
    }

    this.initialiseFormControlWithValidators(initialValue, this.validators);
  }

  getPatternErrorMessage(): string {
    return 'validation.invalid.mobile.phone';
  }
}

export class MobilePhoneControlBuilder {
  private _initialValue: string;
  private _required: boolean;

  static builder(): MobilePhoneControlBuilder {
    const mobilePhoneControlBuilder = new MobilePhoneControlBuilder();
    mobilePhoneControlBuilder._initialValue = '';
    mobilePhoneControlBuilder._required = false;
    return mobilePhoneControlBuilder;
  }

  initialValue(value: string): MobilePhoneControlBuilder {
    if (value) {
      // isNil
      this._initialValue = value;
    } else {
      this._initialValue = '';
    }
    return this;
  }

  required(value: boolean): MobilePhoneControlBuilder {
    this._required = value;
    return this;
  }

  build(): MobilePhoneControl {
    return new MobilePhoneControl(this._initialValue, this._required);
  }
}
