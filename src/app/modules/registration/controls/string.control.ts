import { trimLengthValidator } from '../validators/trim-length.validator';
import { ValidatorFn, Validators } from '@angular/forms';
import { AbstractFormControl } from '../controls/abstract.form.control';

export class StringControl extends AbstractFormControl<string> {
  private readonly validators: ValidatorFn[] = [];

  constructor(
    initialValue: string,
    required: boolean,
    maxLength: number,
    trimLength: boolean
  ) {
    super();
    if (required) {
      this.validators.push(Validators.required);
    }
    if (!!maxLength) {
      // !!!! isNull
      this.validators.push(Validators.maxLength(maxLength));
    }

    if (trimLength) {
      this.validators.push(trimLengthValidator(1));
    }

    this.initialiseFormControlWithValidators(initialValue, this.validators);
  }
}

export class StringControlBuilder {
  private _initialValue: string;
  private _required: boolean;
  private _maxLength: number;
  private _disabled: boolean;
  private _trimLength: boolean;

  static builder(): StringControlBuilder {
    const stringControlBuilder = new StringControlBuilder();
    stringControlBuilder._initialValue = '';
    stringControlBuilder._required = false;
    stringControlBuilder._maxLength = null;
    stringControlBuilder._disabled = false;
    return stringControlBuilder;
  }

  initialValue(value: string): StringControlBuilder {
    if (value) {
      this._initialValue = value;
    } else {
      this._initialValue = '';
    }
    return this;
  }

  required(value: boolean): StringControlBuilder {
    this._required = value;
    return this;
  }

  maxLength(value: number): StringControlBuilder {
    this._maxLength = value;
    return this;
  }

  trimLength(): StringControlBuilder {
    this._trimLength = true;
    return this;
  }

  build(): StringControl {
    return new StringControl(
      this._initialValue,
      this._required,
      this._maxLength,
      this._trimLength
    );
  }
}
