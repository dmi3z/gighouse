import { ValidatorFn, Validators } from '@angular/forms';
import { AbstractFormControl } from './abstract.form.control';

export class EmailControl extends AbstractFormControl<string> {
  private readonly validators: ValidatorFn[] = [];

  constructor(initialValue: string, required: boolean) {
    super();

    this.validators = [
      Validators.maxLength(80),
      Validators.pattern(
        '^([a-zA-Z0-9_\\-\\.+]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,63})$'
      ),
    ];

    if (required) {
      this.validators.push(Validators.required);
    }

    this.initialiseFormControlWithValidators(initialValue, this.validators);
  }
}

export class EmailControlBuilder {
  private _initialValue: string;
  private _required: boolean;

  static builder(): EmailControlBuilder {
    const emailControlBuilder = new EmailControlBuilder();
    emailControlBuilder._initialValue = '';
    emailControlBuilder._required = false;
    return emailControlBuilder;
  }

  initialValue(value: string): EmailControlBuilder {
    if (value) {
      // isNil
      this._initialValue = value;
    } else {
      this._initialValue = '';
    }
    return this;
  }

  required(value: boolean): EmailControlBuilder {
    this._required = value;
    return this;
  }

  build(): EmailControl {
    return new EmailControl(this._initialValue, this._required);
  }
}
