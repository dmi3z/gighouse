import { FormControl, ValidatorFn } from '@angular/forms';

export class AbstractFormControl<T> {
  control: FormControl;

  initialiseFormControl(initialValue: T) {
    this.control = new FormControl(initialValue);
  }

  initialiseFormControlWithValidators(initialValue: T, validators: ValidatorFn[]) {
    this.control = new FormControl(initialValue, validators);
  }

  getTrimLengthErrorMessage(): string {
    return 'validation.required';
  }

  isInvalidAndDirty(): boolean {
    return this.control.invalid && this.control.dirty;
  }

  getRequiredErrorMessage(): string {
    return 'validation.required';
  }

  getPatternErrorMessage(): string {
    return 'validation.invalid.format';
  }

  getMaxLengthErrorMessage(): string {
    return 'validation.maxlength';
  }

  getErrorMessage(): string {
    return this.control.hasError('required')
      ? this.getRequiredErrorMessage()
      : this.control.hasError('maxlength')
      ? this.getMaxLengthErrorMessage()
      : this.control.hasError('pattern')
      ? this.getPatternErrorMessage()
      : this.control.hasError('errorShort')
      ? this.getTrimLengthErrorMessage()
      : '';
  }

  isInvalidAndTouched(): boolean {
    return this.control.invalid && this.control.touched;
  }
}
