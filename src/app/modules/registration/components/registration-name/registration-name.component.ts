import { AuthService } from './../../../../services/auth.service';
import { WebViewService } from './../../../../services/webview.service';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StringControlBuilder } from '../../controls/string.control';
import { LocalStorage } from 'app/constants/local-storage';

@Component({
  selector: 'app-registration-name',
  templateUrl: 'registration-name.component.html',
  styleUrls: ['registration-name.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationNameComponent implements OnInit {
  public userNameForm: FormGroup;
  public isSocial: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private webViewService: WebViewService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) {}

  public ngOnInit(): void {
    this.isSocial = !!this.activatedRoute.snapshot.queryParams.isSocial;
    this.userNameForm = this.createForm();
    this.initializeForm();
  }

  public openPrivacyPolicy(): void {
    this.webViewService.openLink('https://www.gighouse.be/nl-be/freelancers/bedrijf/privacy-beleid').subscribe();
  }

  public openServiceAgreement(): void {
    this.webViewService.openLink('https://www.gighouse.be/nl-be/freelancers/bedrijf/gebruiksvoorwaarden').subscribe();
  }

  public next(): void {
    if (this.checkDisableState()) {
      return;
    }
    const email = this.authService.getUserEmail();
    localStorage.setItem(
      LocalStorage.REGISTRATION_USER_NAME,
      JSON.stringify({
        ...this.userNameForm.value,
        email,
      })
    );
    this.router.navigate(['registration/registration-email'], {
      queryParams: { isSocial: this.isSocial },
    });
  }

  public back(): void {
    this.router.navigate(['']);
  }

  private createForm(): FormGroup {
    const form = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.maxLength(40)]],
      lastName: ['', [Validators.required, Validators.maxLength(80)]],
      privacy: [false, [Validators.required]],
      general: [false, [Validators.required]],
    });

    return form;
  }

  private initializeForm(): void {
    const userDataStore = localStorage.getItem(LocalStorage.REGISTRATION_USER_NAME);
    if (userDataStore) {
      const userData = JSON.parse(userDataStore);

      if (userData.firstName) {
        this.userNameForm.controls.firstName.setValue(userData.firstName);
      }

      if (userData.lastName) {
        this.userNameForm.controls.firstName.setValue(userData.lastName);
      }
    }
  }

  public checkDisableState(): boolean {
    return (
      this.userNameForm.invalid ||
      !this.userNameForm.controls.privacy.value ||
      !this.userNameForm.controls.general.value
    );
  }
}
