import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration-info',
  templateUrl: 'registration-info.component.html',
  styleUrls: ['registration-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationInfoComponent {
  constructor(private router: Router) {}

  public next(): void {
    this.router.navigate(['registration/registration-teaser-job']);
  }
}
