import { AuthService } from './../../../../services/auth.service';
import { switchMap, take, tap } from 'rxjs/operators';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FreelancerRegistrationParams } from 'app/interfaces/models/freelancer-registration-params';
import { FreelancerService } from 'app/services/freelancer.service';
import { StorageService } from 'app/services/storage.service';
import { MobilePhoneControlBuilder } from '../../controls/mobile-phone.control';

@Component({
  selector: 'app-registration-phone',
  templateUrl: 'registration-phone.component.html',
  styleUrls: ['registration-phone.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationPhoneComponent {
  public userPhoneForm: FormGroup;
  public isSocial: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private storageService: StorageService,
    private freelancerService: FreelancerService,
    private authService: AuthService
  ) {}

  public ngOnInit(): void {
    this.userPhoneForm = this.createForm();
  }

  private createForm(): FormGroup {
    const form = this.formBuilder.group({
      phone: ['', [Validators.required, Validators.pattern('^\\+[1-9]\\d{10,14}$')]],
      newsletter: [false],
    });

    return form;
  }

  public register(): void {
    if (this.userPhoneForm.invalid) {
      return;
    }

    const userId = this.storageService.getUserId();
    const freelancerRegParams = new FreelancerRegistrationParams(
      userId,
      this.userPhoneForm.controls.phone.value,
      this.userPhoneForm.controls.newsletter.value
    );

    this.freelancerService
      .register(freelancerRegParams)
      .pipe(
        take(1),
        switchMap(() => this.authService.login(true))
      )
      .subscribe(
        () => {
          this.router.navigate(['registration/registration-info']);
        },
        (e) => {
          this.userPhoneForm.controls.phone.setErrors({
            invalid: true,
          });
          throw e;
        }
      );
  }

  public back(): void {
    this.router.navigate(['registration/registration-email']);
  }
}
