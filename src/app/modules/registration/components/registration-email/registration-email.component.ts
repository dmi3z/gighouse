import { AuthService } from './../../../../services/auth.service';
import { take, map } from 'rxjs/operators';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { PasswordControl } from '../../controls/password.control';
import { LocalStorage } from 'app/constants/local-storage';

@Component({
  selector: 'app-registration-email',
  templateUrl: 'registration-email.component.html',
  styleUrls: ['registration-email.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationEmailComponent {
  public userEmailForm: FormGroup;
  public isSocial: boolean;

  public passwordControl = new PasswordControl();

  // public isLoggedIn$: BehaviorSubject<boolean>;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) {}

  public ngOnInit(): void {
    this.isSocial = !!this.activatedRoute.snapshot.queryParams.isSocial;

    // this.isLoggedIn$ = new BehaviorSubject<boolean>(false);

    this.userEmailForm = this.createForm();
    this.initializeForm();
  }

  public next(): void {
    if (this.userEmailForm.invalid && !this.isSocial) {
      return;
    }
    this.signUp();
  }

  public back(): void {
    this.router.navigate(['registration/registration-name']);
  }

  private signUp(): void {
    const email = this.userEmailForm.get('email').value;
    const password = this.userEmailForm.get('password').value;
    const userInfo = localStorage.getItem(LocalStorage.REGISTRATION_USER_NAME);
    const { firstName, lastName } = JSON.parse(userInfo);
    this.getAuthParams(firstName, lastName, email, password)
      .pipe(take(1))
      .subscribe(
        () => {
          this.router.navigate(['registration/registration-phone']);
        },
        (error) => {
          this.userEmailForm.controls.email.setErrors({ invalid: true });
          throw error;
        }
      );
  }

  private getAuthParams(onboardingFirstName, onboardingLastName, email, password): Observable<any> {
    const isLoggedIn = this.authService.isLoggedIn();
    if (this.isSocial) {
      return of(
        this.authService.updateUserMetadata({
          onboardingFirstName,
          onboardingLastName,
        })
      );
    }
    if (!isLoggedIn) {
      return this.authService.register(email, password, onboardingFirstName, onboardingLastName).pipe(map(() => true));
    }
    return of(false);
  }

  private createForm(): FormGroup {
    const form = this.formBuilder.group({
      email: [
        '',
        [Validators.required, Validators.pattern('^([a-zA-Z0-9_\\-\\.+]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,63})$')],
      ],
      password: this.passwordControl,
    });

    return form;
  }

  private initializeForm(): void {
    const userData = localStorage.getItem(LocalStorage.REGISTRATION_USER_NAME);
    if (userData) {
      const { email } = JSON.parse(userData);
      this.userEmailForm.controls.email.setValue(email);
      this.userEmailForm.controls.email.disable();
    }
  }
}
