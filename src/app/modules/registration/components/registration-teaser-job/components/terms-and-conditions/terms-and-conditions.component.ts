import { ModalController } from '@ionic/angular';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TERMS_AND_CONDITIONS } from './terms-and-conditions.mock';
import { PRIVACY_POLICY } from './privacy-policy.mock';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: 'terms-and-conditions.component.html',
  styleUrls: ['terms-and-conditions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TermsAndConditionsComponent {
  public buttonDisabled = true;
  public termsAndConditions = TERMS_AND_CONDITIONS;
  public privacyPolicy = PRIVACY_POLICY;

  constructor(private modalController: ModalController) {}

  public close(status: boolean): void {
    this.modalController.dismiss({ status });
  }
}
