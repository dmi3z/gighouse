export const PRIVACY_POLICY = `
<div class="field-item even"><p>Deze "Privacy Policy" regelt de verwerking van uw persoonsgegevens door de verantwoordelijke voor de verwerking: GIGHOUSE BV, met maatschappelijke zetel te Beversesteenweg 576, 8800 Roeselare, België en ingeschreven in de K.B.O. onder nummer 0825.694.088&nbsp;(hierna: "BV&nbsp;GIGHOUSE").</p>
<p>Lees deze Privacy Policy aandachtig door aangezien deze uw rechten en plichten ten aanzien van GIGHOUSE BV&nbsp;bevat.</p>
<p>Deze Privacy &amp; Cookie Policy kan regelmatig worden herwerkt of geactualiseerd en we adviseren u om deze regelmatig opnieuw te bekijken.</p>
<p>Door gebruik te maken van deze website gaat u akkoord met de verwerking van uw persoonsgegevens in overeenstemming met en zoals beschreven in deze verklaring.</p>
<h2>Artikel 1 - Algemeen</h2>
<p>1.1.&nbsp;GIGHOUSE BV&nbsp;stelt zich middels huidige Privacy Policy tevens in regel met de Europese Verordening 2016/679 van 27 april 2016 betreffende de bescherming van persoonsgegevens.</p>
<p>1.2.&nbsp;De verantwoordelijke voor de verwerking van uw persoonsgegevens is GIGHOUSE BV. Een verklaring werd afgelegd bij de Commissie voor de Bescherming van de Persoonlijke Levenssfeer wat betreft de verwerking van uw persoonsgegevens.</p>
<h2>Artikel 2 - Persoonsgegevens die u ons meedeelt</h2>
<p>2.1. tijdens een bezoek aan deze Website:</p>
<ul><li>IP-adres;</li>
<li>naam, adres, e-mailadres, telefoonnummer, expertisedomein bij registratie op onze Website;</li>
<li>via cookies: zie artikel 10;</li>
</ul><p>2.2. bij inschrijving op onze nieuwsbrief:</p>
<ul><li>e-mailadres en expertisedomein;</li>
</ul><p>2.3. als werknemer:</p>
<ul><li>naam, adres, e-mailadres, telefoonnummer;</li>
<li>foto's en video's;</li>
<li>CV, motivatiebrieven, notities gemaakt tijdens het sollicitatiegesprek;&nbsp;</li>
<li>informatie over opleiding en werkervaring;</li>
<li>evaluatieformulieren en resultaten van eventuele testen en assessments;&nbsp;</li>
<li>payrollinformatie waaronder rijksregisternummer, adres, anciënniteit, burgerlijke staat, bankrekeningnummer, gezinssamenstelling, geboortedatum;</li>
<li>loon en loonfiches;</li>
</ul><p>2.4. als freelancer:</p>
<ul><li>naam, adres, e-mailadres, telefoonnummer, expertisedomein, bedrijfsnaam, btw-nummer, bankrekeningnummer, geboortedatum;</li>
<li>foto's en video's;</li>
<li>CV, motivatiebrieven, notities gemaakt tijdens het intakegesprek;</li>
<li>Informatie bekomen bij het nagaan van de referenties; &nbsp;</li>
<li>informatie over opleiding en werkervaring, opdrachtvoorkeuren;</li>
<li>evaluatieformulieren en resultaten van eventuele testen en assessments;&nbsp;</li>
<li>timesheets en contracten</li>
</ul><p>2.5. als opdrachtgever, klant of andere zakelijke relatie:</p>
<ul><li>naam, adres, e-mailadres, telefoonnummer, bedrijfsnaam, btw-nummer, bankrekeningnummer, informatie omtrent opdrachten en projecten, contactpersoon;</li>
</ul><h2>Artikel 3 - Persoonsgegevens die wij onrechtstreeks verzamelen</h2>
<p>3.1. Publieke gegevens van de opdrachtgever, klant of andere zakelijke relatie</p>
<p>GIGHOUSE BV&nbsp;verwerkt soms publieke gegevens, bv. gegevens die onderworpen zijn aan een publicatieplicht, zoals de publicatie van uw benoeming als bestuurder van een vennootschap, of gegevens die u zelf publiek heeft gemaakt, zoals informatie op uw website, of gegevens die algemeen bekend zijn in uw streek of in de pers verschenen, of gegevens op bv. de Kruispuntbank van Ondernemingen en Graydon.</p>
<h2>Artikel 4 - Doeleinden van de verwerking</h2>
<p>4.1. Algemene doeleinden:</p>
<p>GIGHOUSE BV&nbsp;zal de vergaarde persoonsgegevens uitsluitend voor de volgende doeleinden gebruiken:</p>
<ul><li>IP-adres&nbsp;(zie artikel 2.1): voor het verzorgen en verbeteren van deze Website en het opnemen van Persoonsgegevens in anonieme statistieken, waaruit de identiteit van specifieke personen of bedrijven niet kunnen achterhaald worden, met als rechtsgrond de legitieme belangen van GIGHOUSE BV&nbsp;om haar Website en dienstverlening voortdurend te verbeteren;</li>
<li>naam, adres, e-mailadres, telefoonnummer, expertisedomein, (zie artikel 2.1) bij registratie op onze Website : om u de gevraagde informatie of diensten te kunnen verstrekken en om te voldoen aan de wettelijke voorschriften. Met als rechtsgrond de uitvoering van een overeenkomst die door u werd gevraagd, en, indien aangegeven door u, eveneens het toezenden van direct marketing, met als rechtsgrond uw expliciete, voorafgaande toestemming;</li>
<li>e-mailadres en expertisedomein (bij inschrijving op de nieuwsbrief)&nbsp;(zie artikel 2.2.): met het oog op het u toezenden van direct marketing (automatische e-mailberichten of andere elektronische berichten), nieuwsbrieven, tewerkstellingsinformatie, of informatie over geplande activiteiten (netwerkevents, zakenlunches, voordrachten) met als rechtsgrond uw expliciete, voorafgaande toestemming;</li>
<li>naam, adres, e-mailadres, telefoonnummer, expertisedomein, bedrijfsnaam, btw-nummer, bankrekeningnummer, curriculum vitae, motivatiebrieven, notities gemaakt tijdens het intakegesprek, informatie over opleiding en werkervaring, informatie bekomen bij het nagaan van referenties, opdrachtvoorkeuren, evaluatieformulieren en resultaten van eventuele testen en assessments, geboortedatum, timesheets en contracten (zie artikel 2.3 en 2.4): om u de gevraagde diensten te kunnen verstrekken en om te voldoen aan de wettelijke voorschriften met als rechtsgrond de uitvoering van een overeenkomst.</li>
<li>foto's en video's&nbsp;(zie artikel 2.3 en 2.4.): om te gebruiken voor onze interne communicatie met als rechtsgrond uw expliciete, voorafgaande toestemming;</li>
<li>naam, adres, e-mailadres, telefoonnummer, bedrijfsnaam, btw-nummer bankrekeningnummer&nbsp;(zie artikel 2.5): om u de gevraagde diensten te kunnen verstrekken en om te voldoen aan de wettelijke voorschriften met als rechtsgrond de uitvoering van een overeenkomst die door u werd gevraagd, en, indien aangegeven door u, eveneens het toezenden van direct marketing, met als rechtsgrond uw expliciete, voorafgaande toestemming.</li>
</ul><p>U bent niet verplicht om uw persoonsgegevens vrij te geven, maar begrijpt dat het verlenen van bepaalde diensten onmogelijk wordt wanneer u de verwerking weigert.</p>
<p>U verklaart dat alle informatie die u aan GIGHOUSE BV&nbsp;verstrekt hebt of zult verstrekken waar, correct en niet misleidend is. Indien deze informatie verandert, zult u GIGHOUSE BV&nbsp;daarover onmiddellijk inlichten.</p>
<p>4.2. Direct marketing:</p>
<p>De persoonsgegevens zullen eveneens worden gebruikt voor direct marketing, mits u hiervoor een bijkomende uitdrukkelijke toestemming heeft gegeven.</p>
<p>Indien u reeds bent opgenomen in onze verzendlijst voor het ontvangen van marketingmateriaal in elektronische vorm, kan GIGHOUSE BV&nbsp;uw gegevens gebruiken voor het verzenden van marketing met betrekking tot GIGHOUSE BV, haar diensten.</p>
<p>Daarnaast kan GIGHOUSE BV&nbsp;uw gegevens ook doorgeven aan haar partners, met het oog op direct marketing.</p>
<p>Zie artikel 12 met betrekking tot het intrekken van uw toestemming.</p>
<p>4.3. Doorgifte aan derden:</p>
<p>GIGHOUSE BV zal, uw persoonsgegevens nooit doorgegeven aan derden, met uitzondering van de filialen van GIGHOUSE BV&nbsp;of één van haar klanten met het oog op de tewerkstelling, technische dienstenleveranciers die technische ondersteuning verzorgen voor zover dit noodzakelijk is voor de technische dienstverlening en/of wanneer zij daartoe gehouden zijn op grond van een wettelijke bepaling of een rechterlijke uitspraak. GIGHOUSE BV&nbsp;kan ook steeds persoonsgegevens doorgeven aan haar partners die nodig zijn om extra faciliteiten en diensten aan te bieden voor de freelancer.</p>
<p>GIGHOUSE BV&nbsp;kan informatie bekendmaken aan bedrijven en personen die we gebruiken om onze bedrijfsfuncties- en diensten uit te voeren. Die functies zijn onder andere het hosten van webservers, het analyseren van gegevens, het verstrekken van juridische, boekhoudkundige en marketingdiensten.</p>
<p>GIGHOUSE BV&nbsp;zal in redelijkheid pogen u van tevoren op de hoogte te stellen van het feit dat GIGHOUSE BV&nbsp;uw gegevens aan genoemde derde bekend maakt, maar u erkent tevens dat dit niet onder alle omstandigheden technisch of commercieel haalbaar is.</p>
<p>GIGHOUSE BV&nbsp;zal uw persoonsgegevens niet verkopen, noch verhuren, verdelen of anderszins commercieel ter beschikking stellen aan derden, behalve zoals bovenstaand beschreven of tenzij met uw voorafgaande toestemming.</p>
<p>4.4. Wettelijke vereisten:</p>
<p>In zeldzame gevallen kan het voorkomen dat GIGHOUSE BV&nbsp;uw persoonsgegevens ingevolge een gerechtelijk bevel of om te voldoen aan andere dwingende wet- of regelgeving moet onthullen. GIGHOUSE BV&nbsp;zal in redelijkheid pogen u van tevoren dienaangaande te informeren, tenzij een en ander aan wettelijke beperkingen onderhevig is.</p>
<h2>Artikel 5 - Duur van de verwerking</h2>
<p>De persoonsgegevens worden door ons bewaard en verwerkt voor een periode die noodzakelijk is in functie van de doeleinden van de verwerking zoals omschreven in artikel 4 en in functie van de contractuele relatie tussen GIGHOUSE BV&nbsp;en U:</p>
<ul><li>IP-adres&nbsp;(zie artikel 2.1): voor de duur van uw bezoek aan de Website;</li>
<li>naam, adres, e-mailadres, telefoonnummer, expertisedomein (zie artikel 2.1) voor de duur van de gevraagde levering van diensten;</li>
<li>e-mailadres en expertisedomein&nbsp;(bij inschrijving op de nieuwsbrief) (zie artikel 2.2.): voor de duur van de gevraagde levering van diensten;</li>
<li>naam , adres, e-mailadres, telefoonnummer expertisedomein, bedrijfsnaam, btw-nummer, bankrekeningnummer, curriculum vitae, motivatiebrieven, notities gemaakt tijdens het intakegesprek, informatie over opleiding en werkervaring, informatie bekomen bij het nagaan van referenties, opdrachtvoorkeuren, evaluatieformulieren en resultaten van eventuele testen en assessments, geboortedatum, timesheets en contracten (zie artikel 2.3 en 2.4): voor de duur van de gevraagde levering van diensten en de tewerkstelling en overeenkomstig de toepasselijke wetgeving;</li>
<li>naam, adres, e-mailadres, telefoonnummer, bedrijfsnaam, btw-nummer, bankrekeningnummer&nbsp;(zie artikel 2.5): voor de duur van de gevraagde levering van diensten;</li>
</ul><p>Vermelde persoonsgegevens worden in elk geval bijgehouden conform de specifieke wettelijke voorschriften, alsook de verjaringstermijnen die ons verplichten uw persoonsgegevens langer bij te houden, bv. om ons te verdedigen tegen een vordering in rechte.</p>
<h2>Artikel 6 - Uw rechten</h2>
<p>6.1. Recht van toegang en inzage:</p>
<p>U heeft het recht om op elk moment gratis kennis te nemen van Uw persoonsgegevens, alsook van het gebruik dat GIGHOUSE BV&nbsp;van Uw persoonsgegevens maakt.</p>
<p>6.2. Recht van verbetering, verwijdering en beperking:</p>
<p>U bent vrij om uw persoonsgegevens al dan niet mee te delen aan GIGHOUSE BV. Daarnaast heeft u steeds het recht om ons te verzoeken uw persoonsgegevens te verbeteren, aan te vullen of te verwijderen. U erkent dat bij weigering van mededeling of verzoek tot verwijdering van persoonsgegevens, bepaalde diensten en producten niet leverbaar zijn.</p>
<p>U mag eveneens vragen om de verwerking van uw persoonsgegevens te beperken via <a href="mailto:info@gighouse.be">info@gighouse.be</a>.</p>
<p>6.3. Recht van verzet:</p>
<p>U beschikt eveneens over een recht van verzet tegen de verwerking van uw persoonsgegevens om ernstige en legitieme redenen.</p>
<p>Daarnaast heeft u steeds het recht om u te verzetten tegen het gebruik van persoonsgegevens voor doeleinden van direct marketing; in dergelijk geval hoeft u geen redenen op te geven</p>
<p>6.4. Recht van vrije gegevensoverdracht:</p>
<p>U beschikt over het recht om uw Persoonsgegevens die door ons verwerkt worden in gestructureerde, gangbare en machine leesbare vorm te verkrijgen en/of aan andere verantwoordelijken over te dragen.</p>
<p>6.5. Recht van intrekking van de toestemming:</p>
<p>Voor zover de verwerking gebaseerd is op uw voorafgaande toestemming, beschikt u over het recht om die toestemming in te trekken.</p>
<p>6.6. Uitoefening van uw rechten:</p>
<p>Zie artikel 12 met betrekking tot het intrekken van uw toestemming.</p>
<p>6.7. Automatische beslissingen en profiling:</p>
<p>De verwerking van Uw Persoonsgegevens omvat geen profiling en u zult door ons evenmin aan geautomatiseerde beslissingen worden onderworpen.</p>
<p>6.8. Recht om klacht in te dienen:</p>
<p>U beschikt over het recht om een klacht in te dienen bij de Belgische Privacycommissie: Commissie voor de Bescherming van de Persoonlijke Levenssfeer, Drukpersstraat 35, 1000 Brussel, Tel +32 (0)2 274 48 00, Fax +32 (0)2 274 48 35, e-mail: <a href="mailto:commission@privacycommission.be">commission@privacycommission.be</a>.</p>
<p>Dit laat een voorziening voor een burgerlijke rechtbank onverlet.</p>
<p>Indien u schade zou lijden als gevolg van de verwerking van uw persoonsgegevens, kunt u een vordering tot schadevergoeding instellen.</p>
<h2>Artikel 7 - Veiligheid en vertrouwelijkheid</h2>
<p>7.1.&nbsp;Wij hebben veiligheidsmaatregelen ontwikkeld die aangepast zijn op technisch en organisatorisch vlak, om de vernietiging, het verlies, de vervalsing, de wijziging, de niet-toegestane toegang of de kennisgeving per vergissing aan derden van persoonsgegevens verzameld te vermijden alsook elke andere niet toegestane verwerking van deze gegevens.</p>
<p>7.2.&nbsp;In geen geval kan GIGHOUSE BV&nbsp;dan ook aansprakelijk worden geacht voor enige directe of indirecte schade die voortvloeit uit een foutief of onrechtmatig gebruik door een derde van de persoonsgegevens. GIGHOUSE BV&nbsp;heeft ervoor gezorgd dat wanneer zij beroep doen op derden voor de verwerking van persoonsgegevens, deze derden aan dezelfde regels zullen gebonden zijn.</p>
<p>7.3.&nbsp;U dient te allen tijde de veiligheidsvoorschriften na te leven. U bent dus als enige verantwoordelijk voor het gebruik dat wordt gemaakt vanaf de Website van uw computer, IP-adres en van Uw identificatiegegevens, alsook voor de vertrouwelijkheid ervan.</p>
<h2>Artikel 8 - Toegang door derden</h2>
<p>8.1.&nbsp;Teneinde Uw persoonsgegevens te kunnen verwerken, verlenen wij toegang tot uw persoonsgegevens aan onze werknemers.</p>
<p>8.2.&nbsp;Wij garanderen een gelijkaardig niveau van bescherming door contractuele verplichtingen tegenstelbaar te maken aan deze werknemers en aangestelden, die gelijkaardig zijn aan deze Privacy Policy.</p>
<h2>Artikel 9 - Cookies</h2>
<p>9.1. Wat zijn cookies?</p>
<p>Een "cookie" is een klein bestand uitgezonden door de server van GIGHOUSE BV&nbsp;en geplaatst op de harde schijf van Uw computer. De inlichtingen opgeslagen op deze cookies kunnen enkel door ons gelezen worden en enkel gedurende de duur van het bezoek aan de Website.</p>
<p>9.2. Waarom gebruiken we cookies?</p>
<p>Onze Website gebruikt cookies en gelijkaardige technologieën om jouw gebruiksvoorkeuren te onderscheiden van die van andere gebruikers van onze Website. Dit helpt ons om u een betere gebruikerservaring te bieden wanneer u onze Website bezoekt en laat ons ook toe onze Website te optimaliseren.</p>
<p>Ten gevolge van recente wetswijzigingen worden alle Websites die gericht zijn op bepaalde delen van de Europese Unie verplicht om uw toestemming te vragen voor het gebruik of opslaan van cookies en gelijkaardige technologieën op jouw computers of mobiele apparaten. Deze cookie policy geeft u duidelijke en volledige informatie over de cookies die wij gebruiken en hun doel.</p>
<p>9.3. De Website gebruikt uitsluitend volgende cookies:</p>
<h4>Functionele cookies</h4>
<p>Authenticatiecookies: 'Remember me' Identificeren de gebruiker als hij is ingelogd</p>
<p>User input cookies: 'Lifestyle Identifier' Het onthouden van handelingen van de gebruiker op een website.</p>
<p>User interface customization-cookies: wordt gebruikt om voorkeuren van de gebruiker te onthouden.</p>
<h4>Niet-functionele cookies</h4>
<p>Trackingcookies: worden gebruikt om het surfgedrag van bezoekers vast te leggen.</p>
<p>Social plug-in trackingcookies: worden gebruikt om sociale-mediamodules aan te bieden op een website, zoals een Facebook like-knop, LinkedIn share-knop of de mogelijkheid een bericht te retweeten.</p>
<p>Voor cookies geplaatst door derde partijen (o.a. Google Analytics) verwijzen wij je graag naar de verklaringen die deze partijen op hun respectieve websites daarover geven. Let op: wij oefenen geen enkele invloed uit op de inhoud van die verklaringen, noch op de inhoud van de cookies van deze derde partijen: Google Analytics cookies</p>
<p>9.4. Uw toestemming:</p>
<p>U kunt cookies weigeren of blokkeren door de configuratieparameters van uw navigatiesysteem te wijzigen. Het uitschakelen van cookies kan betekenen dat u van bepaalde functionaliteiten van de Website geen gebruik kunt maken.</p>
<p>Mocht u verder nog vragen of opmerkingen hebben met betrekking tot de verwerking van Uw persoonsgegevens, neem gerust contact met Ons op, hetzij via <a href="mailto:info@gighouse.be">info@gighouse.be</a> of per post naar GIGHOUSE BV&nbsp;Beversesteenweg 576, 8800 Roeselare, België.</p>
<p>Meer informatie over cookies vindt u <a href="http://www.allaboutcookies.org/">hier</a>.</p>
<p>Meer informatie over online behavioural advertising en online privacy vindt u <a href="http://www.youronlinechoices.eu/">hier</a>.</p>
<h2>Artikel 10 - Toepasselijk recht en bevoegde rechtbank</h2>
<p>Deze Privacy Policy wordt beheerst, uitgelegd en uitgevoerd in overeenstemming met het Belgisch recht dat exclusief van toepassing is.</p>
<p>De rechtbanken van Kortrijk zijn exclusief bevoegd om van elk geschil kennis te nemen dat zou voortvloeien uit de interpretatie of uitvoering van deze Privacy Policy.</p>
<p>In geval van tegenstrijdigheid tussen de Engelse en Franse Privacy &amp; Cookie Policy met de Nederlandse, zal de Nederlandse versie voorrang hebben.</p>
<h2>Artikel 11 – Ons contacteren</h2>
<h4>Bedrijven/klanten</h4>
<p>Indien u als bedrijf of klant vragen heeft over hoe GIGHOUSE BV&nbsp;omgaat met privacy tijdens onze dienstverlening of samenwerking kunt u ons steeds contacteren via <a href="mailto:info@gighouse.be">info@gighouse.be</a>.</p>
<h4>Freelancers</h4>
<p>Heeft u als freelancer vragen over hoe GIGHOUSE BV&nbsp;met uw gegevens omgaat kunt u ons contacteren via&nbsp;<a href="mailto:info@gighouse.be">info@gighouse.be</a>.</p>
<p>Omdat we persoonlijke gegevens van u bijhouden of verwerken heeft u bepaalde rechten (zie Artikel 6). Indien u van deze rechten wenst gebruik te maken, gelieve ons dan te contacteren via <a href="mailto:info@gighouse.be">info@gighouse.be</a>. Om uw privacy te beschermen vragen we hierbij een kopie van uw identiteitskaart toe te voegen.</p>
<p>Wenst u geen commerciële berichten meer te ontvangen via email&nbsp;schrijf je dan hier uit. &nbsp;</p>
<p>&nbsp;</p>
</div>`;
