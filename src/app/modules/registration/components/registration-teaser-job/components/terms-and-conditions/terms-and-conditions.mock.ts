export const TERMS_AND_CONDITIONS = `<div class="blogcontent">



<h2>Algemene voorwaarden freelancer (hierna genoemd “Freelancer”)</h2><h3>Artikel 1: Voorwerp van de overeenkomst</h3>
<p>De Freelancer en GIGHOUSE sluiten hierbij een raamovereenkomst (hierna genoemd “Raamovereenkomst”) waarbij GIGHOUSE zal optreden als tussenpersoon tussen de Freelancer en een klant (hierna genoemd “de Klant”), waarbij de Freelancer bepaalde projecten (hierna genoemd “de Projecten”) zal uitvoeren voor de Klant.</p>
<p>De opdrachten (hierna genoemd “Opdrachten”) van GIGHOUSE bestaan in de matching van de Freelancer en de Klant en de administratieve afhandeling van de samenwerking, waaronder de administratie en de facturatie.</p>
<p>Per Project zal er een afzonderlijke projectovereenkomst (hierna genoemd “Projectovereenkomst”) worden opgesteld en ondertekend door beide partijen.</p>
<p>De bepalingen van onderhavige Raamovereenkomst zijn van toepassing op elke Projectovereenkomst aan deze Raamovereenkomst.</p>
<h3>Artikel 2: Uitvoering van de diensten</h3>
<p>2.1. De Freelancer zal de activiteiten in het kader van deze Raamovereenkomst op een volledig onafhankelijke en zelfstandige basis uitvoeren.</p>
<p>Voor zover als nodig benadrukken de partijen dat de vrijheid en onafhankelijkheid waarover zij jegens elkaar beschikken voor de uitvoering van onderhavige Raamovereenkomst een essentieel element uitmaakt, bij gebreke waaraan onderhavige Raamovereenkomst niet zou zijn afgesloten.</p>
<p>Freelancer zal zijn activiteit aldus naar eigen keuze en inzicht organiseren. Hij zal wel de nodige middelen, energie en tijd aan de Projecten wijden die nodig zijn voor een correcte en professionele uitvoering ervan en tevens rekening houden met de noodwendigheden van de bedrijfsvoering van de Klant.</p>
<p>Freelancer zal op geen enkel ogenblik als werknemer of agent van GIGHOUSE of de Klant kunnen worden beschouwd.</p>
<p>2.2. De Freelancer kan voor de uitvoering van de Projecten onder deze Raamovereenkomst geen beroep doen op en Projecten laten uitvoeren door door hem bepaalde en aangestelde personen, zoals werknemer(s) / aangestelden / helpers of enige andere persoon die verbonden is met de Freelancer, tenzij mits voorafgaandelijk uitdrukkelijk akkoord van GIGHOUSE.</p>
<p>2.3. De Freelancer zal erop toezien dat hij de Projecten en de gehele onderhavige Raamovereenkomst uitvoert in overeenstemming met de Belgische wetten en met de toewijding, de bekwaamheid en de know-how die bij vaklui past evenals met de loyauteit die bij de uitvoering van iedere overeenkomst verschuldigd is.</p>
<h3>Artikel 3: Duur</h3>
<p>3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Onderhavige Raamovereenkomst neemt een aanvang bij registratie door de Freelancer op de website van GIGHOUSE.</p>
<p>3.2.&nbsp;&nbsp; &nbsp;De Raamovereenkomst kan door beide partijen worden opgezegd mits inachtneming van een opzeggingstermijn gelijk aan één maand. De opzeg dient te worden betekend per aangetekend schrijven. De opzegtermijn begint te lopen de dag na het versturen van het aangetekend schrijven. De poststempel geldt als bewijs van datum van verzending.</p>
<p>Deze opzeggingstermijn wordt opgeschort tot zolang (één van) de Projectovereenkomst(en) nog niet volledig is uitgevoerd.</p>
<p>3.3. GIGHOUSE kan in afwijking van artikel 3.2 deze Raamovereenkomst éénzijdig en onmiddellijk beëindigen zonder opzegtermijn of –vergoeding, onverminderd haar recht op schadevergoeding, ingeval:</p>
<p>de Freelancer enige bepaling van deze Raamovereenkomst en/of de Projectovereenkomst schendt;</p>
<p>de Freelancer zich schuldig maakt aan een nalatigheid, ernstige fout of bedrog bij de uitvoering van de Projecten;</p>
<p>van faling, gerechtelijke reorganisatie of kennelijk onvermogen van de Freelancer .</p>
<h3>Artikel 4: Vergoeding</h3>
<p>4.1. GIGHOUSE zal aan de Freelancer een vergoeding betalen voor de uitgevoerde Projecten. Deze wordt gespecificeerd in de Projectovereenkomst bij deze Raamovereenkomst. Het betreft een vergoeding inclusief alle onkosten en exclusief BTW. De Freelancer zal derhalve geen andere vergoedingen kunnen aanrekenen.</p>
<p>4.2. Aan het eind van elke week zal de Freelancer het aantal bij de Klant gepresteerde uren/dagen in het kader van een Project schriftelijk communiceren of via het gebruikt van de GIGHOUSE mobiele applicatie (functionaliteit timesheet) aan GIGHOUSE via timesheets.</p>
<p>4.3. De verschuldigde vergoedingen zullen wekelijks door middel van self-billing door GIGHOUSE gefactureerd worden. Hij zal dit schriftelijk bevestigen aan GIGHOUSE.</p>
<p>In voorkomend geval zal elke factuur de vermelding “factuur uitgereikt door afnemer” dragen. Behoudens schriftelijke opmerkingen aangaande de uitgereikte factuur binnen de 48 uren na ontvangst door de Freelancer, wordt de factuur geacht te zijn aanvaard door de Freelancer.</p>
<p>4.4. De facturen van de Freelancer zullen door GIGHOUSE worden betaald binnen de 30 dagen na factuurdatum .</p>
<p>Ingeval de Klant in gebreke zou blijven om de facturen van GIGHOUSE met de betrekking tot de door de Freelancer uitgevoerde Projecten te betalen binnen de 120 dagen na factuurdatum, dan heeft GIGHOUSE het recht op volledige terugbetaling door de Freelancer van de facturen van laatstgenoemde met betrekking tot deze Projecten.</p>
<p>4.5. Alle onkosten die door de Freelancer gemaakt worden in de uitvoering van onderhavige Raamovereenkomst zijn begrepen in de onder artikel 4.1. vermelde vergoeding.</p>
<h3>Artikel 5: Infrastructuur – materiaal –documenten</h3>
<p>5.1. De Freelancer zal de documentatie en de meest geactualiseerde informatie ter beschikking krijgen van de Klant welke nodig is voor de uitvoering van de Projecten.</p>
<p>5.2. De Klant blijft te allen tijde eigenaar van alle materiaal, producten, documenten, gegevens of formules die zij tijdelijk ter beschikking stelt van de Freelancer voor de uitvoering van de Projecten.</p>
<h3>Artikel 6: Vertrouwelijkheid</h3>
<p>6.1. Zowel tijdens de duur van onderhavige Raamovereenkomst als nadat deze een einde genomen zal hebben om welke reden dan ook, verbindt de Freelancer zich ertoe de meest strikte discretie te bewaren ten aanzien van GIGHOUSE en elke derde met betrekking tot feiten waarvan hij kennis zou genomen hebben bij de uitvoering van de aan hem toevertrouwde Projecten.</p>
<p>6.2. De Freelancer zal erop toezien dat iedere aangestelde waarop hij beroep doet voor de uitvoering van het Project, dezelfde verplichting als beschreven in artikel&nbsp;6.1. onderschrijft.</p>
<p>6.3. Ingeval van inbreuk op artikel 6.1. en 6.2. van deze Raamvereenkomst, zal de Freelancer een forfaitaire schadevergoeding verschuldigd zijn aan GIGHOUSE van 25.000,00 EUR, onverminderd het recht van GIGHOUSE om volledige schadeloosstelling te vorderen.</p>
<h3>Artikel 7: Verbod tot afwerving van cliënteel</h3>
<p>7.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; De Freelancer verbindt er zich toe om, gedurende een termijn van 125 gepresteerde werkdagen (1 werkdag =8u) er zich van te onthouden om, zonder de voorafgaandelijke schriftelijke toestemming van GIGHOUSE, rechtstreeks of onrechtstreeks en in welke hoedanigheid ook, dezelfde of gelijkaardige opdrachten uit te voeren voor de Klant als de Projecten die het voorwerp zijn van onderhavige Raamovereenkomst.</p>
<p>7.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ingeval van inbreuk op artikel 7.1. van deze Raamovereenkomst, zal de Freelancer een schadevergoeding verschuldigd zijn aan GIGHOUSE gelijk aan het bedrag van de vergoedingen die onder deze Raamovereenkomst aan haar verschuldigd zijn, onverminderd het recht van GIGHOUSE om volledige schadeloosstelling te vorderen.</p>
<h3>Artikel 8: Intellectuele eigendom</h3>
<p>8.1. De Freelancer gaat ermee akkoord dat alle technische verbeteringen, nieuwe processen, en meer bepaald alle uitvindingen, ongeacht hun aard, waarvan de Freelancer in het kader van de uitvoering van deze Raamovereenkomst rechthebbende of mederechthebbende zou worden, onherroepelijk tot de exclusieve eigendom van de Klant zullen behoren, zonder dat hiervoor een recht op vergoeding bestaat in hoofde van de Freelancer.</p>
<p>8.2. Indien de Freelancer in het kader van de uitvoering van deze Raamvereenkomst wordt toevertrouwd met het tot stand brengen van enig auteursrechtelijk beschermd werk of computerprogramma – hierin begrepen doch niet beperkt tot tekeningen, literaire werken van alle aard, databanken – gaat de Freelancer er bij deze uitdrukkelijk mee akkoord dat alle patrimoniale rechten die aan deze werken zijn verbonden worden overgedragen aan de Klant, voor de volledige duur van deze rechten en voor de hele wereld.</p>
<p>8.3. Deze overdracht omvat, doch is niet beperkt tot, het recht om elk van deze werken op de volgende wijzen te exploiteren:</p>
<p>de grafische uitgave van het werk in om het even welke boekvorm, voor om het even welk doel, in om het even welke taal;</p>
<p>het recht om het werk te vertalen of te laten vertalen in om het even welke taal;</p>
<p>de opname van een gedeelte van het werk, ongeacht de taal van het werk, in een bloemlezing, databank, multimediawerk of anderszins, en de exploitatie van de aldus ontstane creaties;</p>
<p>de opname van het gehele werk, ongeacht de taal, in een ander werk, databank, multimediawerk of anderszins, en de exploitatie van de aldus ontstane creaties;</p>
<p>de publicatie van (een gedeelte van) het werk, ongeacht de taal, in een krant, weekblad, tijdschrift of anderszins;</p>
<p>de vastlegging van (een gedeelte van) het werk, ongeacht de taal, op geluids-en/of beelddragers (o.a. geluidscassettes, beeldcassettes, cd, cd-rom, cd-i, internet, elke andere elektronische uitgave), alsook de verveelvoudiging van (een gedeelte van) het werk, ongeacht de taal, via bovenvermelde geluids-en/of beelddragers en de exploitatie van laatstgenoemde creaties;</p>
<p>de publieke mededeling van (een gedeelte van) het werk, ongeacht de taal, via radio of televisie;</p>
<p>de bewerking van het werk, met uitsluiting van iedere audiovisuele bewerking, en de exploitatie van deze bewerking op één van de hierboven vermelde wijzen;</p>
<p>het recht om, indien het werk geëxploiteerd wordt overeenkomstig één van de hierboven genoemde wijzen en hierdoor exemplaren van dit werk tot stand komen, deze exemplaren te verhuren of uit te lenen.</p>
<p>8.4. De vergoeding voor de overdracht van patrimoniale rechten voor elk van deze exploitatiewijzen wordt gedekt door de vergoeding die is voorzien in artikel 4.1 van deze Raamovereenkomst. De Freelancer heeft geen recht op enige bijkomende vergoeding, bovenop de vergoeding voorzien in artikel 4.1.</p>
<h3>Artikel 9: Aansprakelijkheid</h3>
<p>In geval de Klant schade zou lijden als gevolg van een fout begaan door de Freelancer in het kader van de uitoefening van een Project, verbindt de Freelancer er zich toe GIGHOUSE hiervoor volledig te vrijwaren in het geval dat de Klant GIGHOUSE hiervoor zou aanspreken.</p>
<p>De Freelancer verklaart en garandeert dat hij/zij een geldige aansprakelijkheidsverzekering BA uitbating heeft afgesloten bij een erkende verzekeringsmaatschappij die zijn/haar aansprakelijkheid ten aanzien van de Klant c.q. GIGHOUSE voldoende verzekert.</p>
<p>Kopie van de hierboven vermelde polis alsook bewijs van premiebetaling zal op eerste verzoek van GIGHOUSE worden voorgelegd door de Freelancer.</p>
<h3>Artikel 10:&nbsp;Integraliteit van de overeenkomst</h3>
<p>Er wordt uitdrukkelijk overeengekomen dat onderhavige Raamovereenkomst samen met de Projectovereenkomst(en) aan deze Raamovereenkomst, de integraliteit van de overeenkomsten tussen partijen uitdrukt en alle vorige overeenkomsten tussen partijen, evenals alle schriftelijke en mondelinge voorstellen, onderhandelingen, conversaties en discussies, voorheen tussen de partijen gevoerd met betrekking tot onderhavige Raamovereenkomst, vervangt en dat, in voorkomend geval, de algemene voorwaarden van de Freelancer geen toepassing zullen vinden.</p>
<p>Enkel schriftelijk en in onderlinge overeenstemming kunnen wijzigingen aan deze Raamovereenkomst aangebracht worden.</p>
<h3>Artikel 11: Toepasselijk recht en bevoegd rechtscollege</h3>
<p>Onderhavige Raamovereenkomst is in al haar aspecten onderworpen aan Belgisch recht.</p>
<p>Ieder geschil betreffende de geldigheid, de interpretatie, de uitvoering of de beëindiging van onderhavige Raamovereenkomst behoort tot de exclusieve bevoegdheid van de rechtbanken van Kortrijk.</p>
<p>&nbsp;</p>





			</div>`;
