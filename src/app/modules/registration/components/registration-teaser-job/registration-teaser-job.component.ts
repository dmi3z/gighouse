import { TermsAndConditionsComponent } from './components/terms-and-conditions/terms-and-conditions.component';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { StorageService } from '../../../../services/storage.service';
import { FreelancerService } from '../../../../services/freelancer.service';
import { take, tap } from 'rxjs/operators';
import { LoadingRepository } from 'app/services/loading.repository';
import { BehaviorSubject, Observable, zip } from 'rxjs';
import { Expertise } from '../../../../interfaces/models/expertise';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Profile } from 'app/interfaces/models/profile';
import { MaximumTravelTime } from 'app/interfaces/models/maximum-travel-time';
import { CacheableMetadataService } from 'app/services/cacheable-metadata.service';
import { UpdateOnboardingFreelancerRequest } from 'app/interfaces/models/update-freelancer';

@Component({
  selector: 'app-teaser-job',
  templateUrl: 'registration-teaser-job.component.html',
  styleUrls: ['registration-teaser-job.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationTeaserJobComponent implements OnInit {
  public teaserJobForm: FormGroup;
  public expertises: Expertise[];
  public profilesForExpertise$: Observable<Profile[]>;
  public maximumTravelTimes: MaximumTravelTime[];
  public noProfilesForExpertiseLoaded$ = new BehaviorSubject<boolean>(true);

  constructor(
    private formBuilder: FormBuilder,
    private cacheableMetadataService: CacheableMetadataService,
    private loadingRepository: LoadingRepository,
    private freelancerService: FreelancerService,
    private storageService: StorageService,
    private router: Router,
    private modalController: ModalController
  ) {}

  public ngOnInit(): void {
    this.teaserJobForm = this.formBuilder.group({
      expertise: ['', Validators.required],
      profile: [{ value: '', disabled: true }, [Validators.required]],
      maximumTravelTime: ['', Validators.required],
      postalCode: ['', [Validators.pattern('^\\d{4}$'), Validators.required]],
      termsAndConditions: [false],
    });

    zip(this.cacheableMetadataService.getExpertises(), this.cacheableMetadataService.getMaximumTravelTimes()).subscribe(
      ([expertises, maximumTravelTimes]: [Expertise[], MaximumTravelTime[]]) => {
        this.expertises = expertises;
        this.maximumTravelTimes = maximumTravelTimes;
        this.loadingRepository.stopLoading();
      }
    );
  }

  public saveTeaserJobInput(): void {
    if (this.teaserJobForm.invalid) {
      return;
    }

    this.loadingRepository.presentLoading();
    const freelancerId = this.storageService.getFreelancerId();
    const profileId = this.teaserJobForm.controls.profile.value;
    const postalCode = this.teaserJobForm.controls.postalCode.value;
    const maximumTravelTime = this.teaserJobForm.controls.maximumTravelTime.value;
    this.freelancerService
      .getFreelancerDetails(freelancerId)
      .pipe(take(1))
      .subscribe({
        next: (freelancerDetails) => {
          const updateRequest = new UpdateOnboardingFreelancerRequest(
            freelancerDetails,
            profileId,
            postalCode,
            maximumTravelTime
          );
          return this.freelancerService.updateFreelancer(updateRequest);
        },
        complete: () => {
          this.loadingRepository.stopLoading();
          this.router.navigate(['teaser-job-overview']);
        },
      });
  }

  public async openTerms(): Promise<void> {
    const modal = await this.modalController.create({
      component: TermsAndConditionsComponent,
    });
    modal.onWillDismiss().then((termsAndConditionsStatus) => {
      if (!this.teaserJobForm.controls.termsAndConditions.value) {
        this.teaserJobForm.controls.termsAndConditions.setValue(termsAndConditionsStatus);
      }
    });
    return await modal.present();
  }

  public getProfilesForExpertise(): void {
    const expertiseValue = this.teaserJobForm.controls.expertise.value;

    this.profilesForExpertise$ = this.cacheableMetadataService.getProfilesFor(expertiseValue).pipe(
      tap(() => {
        this.teaserJobForm.controls.profile.enable();
        this.teaserJobForm.controls.profile.setValue('');
      })
    );
  }

  public checkDisableState(): boolean {
    return this.teaserJobForm.invalid || !this.teaserJobForm.controls.termsAndConditions.value;
  }
}
