import { ChatService } from './../../services/chat.service';
import { EventsService } from './../../services/events.service';
import { GighouseEvent } from './../../interfaces/models/gighouse-event';
import { Router } from '@angular/router';
import { SettingsComponent } from 'app/modules/settings/settings.component';
import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { StorageService } from 'app/services/storage.service';
import { Job } from '../../interfaces/models/job';
import { JobService } from 'app/services/job.service';
import { JobType } from 'app/interfaces/job-type';
import { ModalController } from '@ionic/angular';
import { JobDetailComponent } from './components/job-detail/job-detail.component';
import { LoadingRepository } from 'app/services/loading.repository';
import { tap, map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { AppState } from '@capacitor/core';
import { getTotalUnreadMessages } from 'app/store/chat/chat.selectors';

@Component({
  selector: 'app-assignments',
  templateUrl: 'assignments.page.html',
  styleUrls: ['assignments.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AssignmentsComponent implements OnInit {
  public possibleJobs$: Observable<Job[]>;
  readonly phoneNumber = '093317761';

  public isHaveUnreadMessages$: Observable<boolean>;

  constructor(
    private storageService: StorageService,
    private jobService: JobService,
    private modalController: ModalController,
    private router: Router,
    private loading: LoadingRepository,
    private events: EventsService,
    private store$: Store<AppState>,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ionViewDidEnter(): void {
    this.fetchPossibleJobs();
  }

  public ngOnInit(): void {
    this.events.subscribe(GighouseEvent.freelancerJobInterestUpdated, () => this.fetchPossibleJobs());

    this.isHaveUnreadMessages$ = this.store$.pipe(
      select(getTotalUnreadMessages()),
      map((res) => res > 0)
    );
  }

  public async openSettingsPage(): Promise<void> {
    const modal = await this.modalController.create({
      component: SettingsComponent,
    });
    modal.present();
  }

  public openChatList(): void {
    this.router.navigate(['tabs/chat-list']);
  }

  public async openJobDetail(jobId: string): Promise<void> {
    const jobType = JobType.possibleJob;
    const modal = await this.modalController.create({
      component: JobDetailComponent,
      componentProps: {
        jobId,
        jobType,
      },
    });

    modal.onWillDismiss().then(({ data }) => {
      if (data && data.isUpdate) {
        this.fetchPossibleJobs();
      }
    });
    modal.present();
  }

  private fetchPossibleJobs(): void {
    this.loading.presentLoading();
    const freelancerId = this.storageService.getFreelancerId();
    this.possibleJobs$ = this.jobService
      .getPossibleJobsForFreelancer(freelancerId)
      .pipe(tap(() => this.loading.stopLoading()));
    this.changeDetectorRef.markForCheck();
  }
}
