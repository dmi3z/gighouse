import { ModalController } from '@ionic/angular';
import { MessagesListComponent } from './../../../chat-list/components/message-list/messages-list.component';
import { map } from 'rxjs/operators';
import { ChatService } from './../../../../services/chat.service';
import { LoadingRepository } from './../../../../services/loading.repository';
import { StorageService } from './../../../../services/storage.service';
import { ConversationBody } from './../../../chat-list/interfaces/chat.interface';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Job } from '../../../../interfaces/models/job';

@Component({
  selector: 'app-possible-job-card',
  templateUrl: './possible-job-card.component.html',
  styleUrls: ['possible-job-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PossibleJobCardComponent implements OnInit {
  @Input()
  public job: Job;

  @Output() public openEvent = new EventEmitter<string>();

  public name: string;
  public teaserText1: string;
  public teaserText2: string;
  public teaserText3: string;
  public location: string;
  public fee: number;
  public startDate: string;
  public daysAWeek: number;

  constructor(
    private storageService: StorageService,
    private loadingRepository: LoadingRepository,
    private chatService: ChatService,
    private modalController: ModalController
  ) {}

  public ngOnInit(): void {
    this.name = this.job.getName();
    this.teaserText1 = this.job.getTeaserText1();
    this.teaserText2 = this.job.getTeaserText2();
    this.teaserText3 = this.job.getTeaserText3();
    this.location = this.job.getLocation();
    this.fee = this.job.getFee();
    this.startDate = this.job.getStartDate();
    this.daysAWeek = this.job.getDaysAWeek();
  }

  public openDescription(): void {
    this.openEvent.next(this.job.getId());
  }

  public openChat(event: Event): void {
    event.stopPropagation();

    const conversationBody: ConversationBody = {
      contactName: '',
      customerId: null,
      projectId: this.job.getId(),
    };

    const freelancerId = this.storageService.getFreelancerId();
    this.loadingRepository.presentLoading();
    this.chatService
      .getConversation(freelancerId, conversationBody)
      .pipe(map((data) => data.conversationSid))
      .subscribe(async (conversationSid) => {
        const modal = await this.modalController.create({
          component: MessagesListComponent,
          componentProps: { conversationBody, conversationSid },
        });
        await modal.present();
      });
  }
}
