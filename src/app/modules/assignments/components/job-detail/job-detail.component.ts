import { EventsService } from './../../../../services/events.service';
import { GighouseEvent } from './../../../../interfaces/models/gighouse-event';
import { LoadingRepository } from 'app/services/loading.repository';
import { Interest } from './../../../../interfaces/models/interest';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { take, tap } from 'rxjs/operators';
import { JobService } from 'app/services/job.service';
import { JobDetail } from 'app/interfaces/models/job-detail';
import { FreelancerService } from 'app/services/freelancer.service';
import { StorageService } from 'app/services/storage.service';
import { Observable } from 'rxjs';
import { JobType } from 'app/interfaces/job-type';
import { ModalController } from '@ionic/angular';
import { ToastService } from 'app/services/toast.service';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobDetailComponent implements OnInit {
  @Input() public jobId: string;
  @Input() public jobType: JobType;
  @Input() public isCanRate = true;

  public jobDetails$: Observable<JobDetail>;
  public isPossibleJob: boolean;

  constructor(
    private jobService: JobService,
    private freelancerService: FreelancerService,
    // private translationsService: TranslationsService,
    private storageService: StorageService,
    private toastService: ToastService,
    private modalController: ModalController,
    private loading: LoadingRepository,
    private events: EventsService
  ) {}

  public ngOnInit(): void {
    this.loading.presentLoading();
    this.jobDetails$ = this.jobService.getJobDetail(this.jobId).pipe(tap(() => this.loading.stopLoading()));
    this.isPossibleJob = this.getIsPossible();
  }

  public freelancerIsInterestedInJob(): void {
    this.updateFreelancerJobInterest(Interest.interested);
  }

  public freelancerIsNotInterestedInJob(): void {
    this.updateFreelancerJobInterest(Interest.not_interested);
  }

  public close(isUpdate: boolean): void {
    this.modalController.dismiss({ isUpdate });
  }

  private getIsPossible(): boolean {
    return this.jobType === JobType.possibleJob;
  }

  private updateFreelancerJobInterest(interest: Interest): void {
    const freelancerId = this.storageService.getFreelancerId();
    this.freelancerService
      .updateFreelancerJobInterest(freelancerId, this.jobId, interest)
      .pipe(
        take(1),
        tap(() => {
          this.close(true);
          this.events.publish(GighouseEvent.freelancerJobInterestUpdated);
          this.toastService.presentSuccessToast('job-detail.update.interest.success.message');
        })
      )
      .subscribe();
  }
}
