import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AssignmentsComponent } from './assignments.page';

import { AssignmentsRoutingModule } from './assignments-routing.module';
import { PossibleJobCardComponent } from './components/possible-job-card/possible-job-card.component';
import { PipesModule } from 'app/pipes/pipes.module';
import { JobDetailComponent } from './components/job-detail/job-detail.component';
import { SettingsModule } from '../settings/settings.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    AssignmentsRoutingModule,
    TranslateModule,
    PipesModule,
    SettingsModule,
  ],
  declarations: [
    AssignmentsComponent,
    PossibleJobCardComponent,
    JobDetailComponent,
  ],
})
export class AssignmentsModule {}
