export interface StakeholdersItem {
  accountName: string;
  projectId: string;
  projectName: string;
  contactId: string;
  contactName: string;
  freelancerId: string;
}
