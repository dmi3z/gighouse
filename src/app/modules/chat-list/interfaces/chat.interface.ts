export interface ChatItem {
  accountName: string;
  projectId: string;
  projectName: string;
}

export interface UserIdentifier {
  identity: string;
  token: string;
}

export interface ConversationBody {
  customerId: string;
  projectId: string;
  contactName: string;
}

export interface ConversationSid {
  conversationSid: string;
}
