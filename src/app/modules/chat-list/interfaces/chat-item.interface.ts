export interface ChatItem {
  accountName: string;
  projectId: string;
  projectName: string;
}
