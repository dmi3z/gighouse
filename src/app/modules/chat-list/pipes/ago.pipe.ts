import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'appAgoPipe',
})
export class AgoPipe implements PipeTransform {
  transform(value: Date): string {
    return moment(value).fromNow();
  }
}
