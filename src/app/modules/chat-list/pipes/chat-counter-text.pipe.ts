import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'chatCounterPipe',
})
export class ChatCounterPipe implements PipeTransform {
  transform(numberOfChats: number): string {
    return numberOfChats > 1 ? numberOfChats + ' chats' : numberOfChats + ' chat';
  }
}
