import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pruningNamePipe',
})
export class PruningNamePipe implements PipeTransform {
  transform(value: string): string {
    return value.split(' ').reduce((prunedString, currWord) => prunedString.concat(currWord[0]), '');
  }
}
