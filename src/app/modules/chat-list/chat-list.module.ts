import { PruningNamePipe } from './pipes/pruning-name.pipe';
import { ChatCounterPipe } from './pipes/chat-counter-text.pipe';
import { MessagesListComponent } from './components/message-list/messages-list.component';
import { ChatItemComponent } from './components/chat-item/chat-item.component';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { ChatListComponent } from './chat-list.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ChatProjectsComponent } from './components/chat-projects/chat-projects.component';
import { ChatParticipantsComponent } from './components/chat-participants/chat-participants.component';
import { StakeholderItemComponent } from './components/stakeholder-item/stakeholder-item.component';
import { MessageItemComponent } from './components/message-item/message-item.component';
import { AgoPipe } from './pipes/ago.pipe';

const routes: Routes = [
  {
    path: '',
    component: ChatListComponent,
  },
];

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule, FormsModule, RouterModule.forChild(routes)],
  declarations: [
    ChatListComponent,
    ChatItemComponent,
    MessagesListComponent,
    ChatProjectsComponent,
    ChatParticipantsComponent,
    StakeholderItemComponent,
    MessageItemComponent,
    AgoPipe,
    ChatCounterPipe,
    PruningNamePipe,
  ],
})
export class ChatListModule {}
