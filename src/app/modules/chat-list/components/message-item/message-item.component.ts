import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Message } from '@twilio/conversations/lib/message';

@Component({
  selector: 'app-message-item',
  templateUrl: 'message-item.component.html',
  styleUrls: ['message-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageItemComponent implements OnInit {
  @Input() public item: Message;
  @Input() freelancerId: string;

  public isMyself: boolean;
  constructor() {}

  ngOnInit() {
    this.isMyself = this.item.author === this.freelancerId;
  }
}
