import { ChatService } from './../../../../services/chat.service';
import { LoadingRepository } from './../../../../services/loading.repository';
import { switchMap, take, tap, map } from 'rxjs/operators';
import { from, fromEvent } from 'rxjs';
import { IonContent, ModalController } from '@ionic/angular';
import {
  Component,
  Input,
  OnInit,
  ViewChild,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { ConversationBody } from '../../interfaces/chat.interface';
import { Conversation } from '@twilio/conversations/lib/conversation';
import { Message } from '@twilio/conversations/lib/message';
import { StorageService } from 'app/services/storage.service';
import { Store } from '@ngrx/store';
import { AppState } from '@capacitor/core';
import { Paginator } from '@twilio/conversations/lib/interfaces/paginator';

@Component({
  selector: 'app-messages-list',
  templateUrl: 'messages-list.component.html',
  styleUrls: ['messages-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessagesListComponent implements OnInit, OnDestroy {
  @Input()
  public conversationBody: ConversationBody;
  @Input()
  public customerName: string;
  @Input() public conversationSid: string;
  @ViewChild(IonContent, { read: IonContent }) scrollContainer: IonContent;
  public message: string;
  public messages: Message[] = [];
  public freelancerId: string;
  private conversation: Conversation;
  private paginator: Paginator<Message>;

  constructor(
    private modalController: ModalController,
    private storageService: StorageService,
    private loadingService: LoadingRepository,
    private chatService: ChatService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.freelancerId = this.storageService.getFreelancerId();
    from(this.chatService.twilioClient.getConversationBySid(this.conversationSid))
      .pipe(
        take(1),
        switchMap((conversation) => {
          this.conversation = conversation;
          this.addNewMessageSubscription(conversation);
          return conversation.getMessages();
        })
      )
      .subscribe((paginator) => {
        this.paginator = paginator;
        this.messages = paginator.items;
        this.scrollToBottom();
        this.changeDetectorRef.markForCheck();
        this.loadingService.stopLoading();
      });
  }

  public onScroll(event): void {
    if (event.detail.scrollTop === 0 && this.paginator.hasPrevPage) {
      this.paginator.prevPage().then((res) => this.messages.unshift(...res.items));
    }
  }

  public close(): void {
    this.modalController.dismiss();
  }

  public loadData(event): void {
    if (this.paginator.hasPrevPage) {
      this.paginator.prevPage().then((res) => {
        this.paginator = res;
        this.messages.unshift(...res.items);
        event.target.complete();
        this.changeDetectorRef.markForCheck();
      });
    } else {
      event.target.disabled = true;
    }
  }

  public sendMessage(): void {
    if (this.message.trim().length > 0) {
      this.conversation.sendMessage(this.message);
      this.message = '';
    }
  }

  private addNewMessageSubscription(conversation: Conversation): void {
    fromEvent(conversation, 'messageAdded')
      .pipe(map((result: any) => result.state as Message))
      .subscribe((message) => {
        this.messages = [...this.messages, message];
        this.changeDetectorRef.markForCheck();
        this.scrollToBottom();
      });
  }

  private scrollToBottom(): void {
    setTimeout(() => {
      const scrollContainer = this.scrollContainer;
      scrollContainer.scrollToBottom();
    }, 0);
  }

  public ngOnDestroy() {
    this.conversation.removeAllListeners();
  }
}
