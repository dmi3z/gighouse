import { ChatItem } from './../../interfaces/chat-item.interface';
import { LoadingRepository } from 'app/services/loading.repository';
import { MessagesListComponent } from './../message-list/messages-list.component';
import { ModalController, Platform } from '@ionic/angular';
import { ChatService } from '../../../../services/chat.service';

import { ChangeDetectionStrategy, Component, Input, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { StakeholdersItem } from '../../interfaces/stakeholders-item.interface';
import { Observable, Subscription } from 'rxjs';
import { StorageService } from 'app/services/storage.service';
import { tap } from 'rxjs/operators';
import { BackButtonPriority } from 'app/interfaces/models/back-button-priority';

@Component({
  selector: 'app-chat-participants',
  templateUrl: 'chat-participants.component.html',
  styleUrls: ['chat-participants.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatParticipantsComponent implements OnInit, OnDestroy {
  public stakeholdersItems$: Observable<StakeholdersItem[]>;
  private freelancerId: string;
  private childLoadingCounter: number;
  private subs: Subscription = new Subscription();

  @Input()
  public project: ChatItem;

  constructor(
    private storageService: StorageService,
    private chatService: ChatService,
    private modalController: ModalController,
    private loadingRepository: LoadingRepository,
    private changeDetectorRef: ChangeDetectorRef,
    private platform: Platform
  ) {}

  public ngOnInit(): void {
    this.initializeBackButtonHandler();
    this.loadingRepository.presentLoading();
    this.freelancerId = this.storageService.getFreelancerId();
    this.stakeholdersItems$ = this.chatService.getStakeholdersList(this.freelancerId, this.project.projectId).pipe(
      tap(() => {
        this.childLoadingCounter = 0;
      })
    );
  }

  public checkLoadingStatus(statusLoading: number): void {
    this.childLoadingCounter += statusLoading;
    if (!this.childLoadingCounter) {
      this.loadingRepository.stopLoading();
    }
  }

  private initializeBackButtonHandler(): void {
    const subscription = this.platform.backButton.subscribeWithPriority(BackButtonPriority.medium, () => {
      this.loadingRepository.stopLoading();
      this.close();
    });
    this.subs.add(subscription);
  }

  public close(): void {
    this.modalController.dismiss();
  }

  public async showChatModal({ conversationBody, conversationSid, customerName }): Promise<void> {
    this.loadingRepository.presentLoading();

    const modal = await this.modalController.create({
      component: MessagesListComponent,
      componentProps: { conversationBody, conversationSid, customerName },
    });
    modal.onDidDismiss().then(() => {
      this.changeDetectorRef.markForCheck();
      this.loadingRepository.presentLoading();
      this.stakeholdersItems$ = this.chatService.getStakeholdersList(this.freelancerId, this.project.projectId).pipe(
        tap(() => {
          this.childLoadingCounter = 0;
        })
      );
    });
    await modal.present();
  }

  public ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
