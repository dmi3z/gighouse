import { combineAll, map, filter, concatAll, tap, mergeMap } from 'rxjs/operators';
import { Observable, combineLatest, forkJoin, pipe } from 'rxjs';
import {
  Component,
  Input,
  EventEmitter,
  Output,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
} from '@angular/core';
import { ChatItem } from '../../interfaces/chat-item.interface';
import { select, Store } from '@ngrx/store';
import { AppState } from '@capacitor/core';
import { getConversationsByProjectId } from 'app/store/chat/chat.selectors';

@Component({
  selector: 'app-chat-item',
  templateUrl: 'chat-item.component.html',
  styleUrls: ['chat-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatItemComponent implements OnInit {
  @Input()
  public item: ChatItem;
  @Output()
  public itemClick = new EventEmitter<ChatItem>();

  public unreadCount$: Observable<number>;
  public isLoading;
  public chatsCounter = 0;
  constructor(private store$: Store<AppState>, private changeDetectorRef: ChangeDetectorRef) {}

  public ngOnInit(): void {
    this.isLoading = true;
    this.unreadCount$ = this.store$.pipe(select(getConversationsByProjectId(this.item.projectId))).pipe(
      filter((conversations) => !!conversations),
      tap((items) => {
        this.chatsCounter = items.length;
        this.changeDetectorRef.detectChanges();
      }),
      mergeMap((conversationData) => forkJoin(conversationData.map((item) => item.unreadCount))),
      map((items) => items.reduce((acc, count) => (acc += count))),
      pipe(
        tap(() => {
          this.isLoading = false;
        })
      )
    );
  }

  public openParticipants(): void {
    this.itemClick.next(this.item);
  }
}
