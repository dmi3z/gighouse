import { LoadingRepository } from './../../../../services/loading.repository';
import { ChatParticipantsComponent } from './../chat-participants/chat-participants.component';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { ChatItem } from './../../interfaces/chat-item.interface';
import { tap, map } from 'rxjs/operators';
import { ChatService } from '../../../../services/chat.service';
import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { StorageService } from 'app/services/storage.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AppState } from '@capacitor/core';

@Component({
  selector: 'app-chat-projects',
  templateUrl: 'chat-projects.component.html',
  styleUrls: ['chat-projects.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatProjectsComponent implements OnInit {
  public counter = 0;
  public chatItems$: Observable<ChatItem[]>;

  constructor(
    private loadingRepository: LoadingRepository,
    private modalController: ModalController,
    private storageService: StorageService,
    private chatService: ChatService,
    private router: Router,
    private loading: LoadingRepository,
    private store$: Store<AppState>,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.loading.presentLoading();

    const freelancerId = this.storageService.getFreelancerId();
    this.chatItems$ = this.chatService.getChatList(freelancerId).pipe(
      tap((result) => {
        if (result) {
          this.counter = result.length;
          this.changeDetectorRef.markForCheck();
          this.loading.stopLoading();
        }
      }),
      map((items) => items.map((item) => ({ ...item, unreadCount: 0 })))
    );
  }

  public async goToStakeholdersList(project: ChatItem): Promise<void> {
    const modal = await this.modalController.create({
      component: ChatParticipantsComponent,
      componentProps: { project },
    });
    modal.present();
  }
}
