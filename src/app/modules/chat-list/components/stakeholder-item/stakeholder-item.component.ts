import { ChatService } from './../../../../services/chat.service';
import { ConversationBody } from './../../interfaces/chat.interface';
import { map, switchMap } from 'rxjs/operators';
import { StakeholdersItem } from './../../interfaces/stakeholders-item.interface';
import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@capacitor/core';
import { Observable, from } from 'rxjs';
import { Message } from '@twilio/conversations/lib/message';

@Component({
  selector: 'app-stakeholder-item',
  templateUrl: 'stakeholder-item.component.html',
  styleUrls: ['stakeholder-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StakeholderItemComponent implements OnInit {
  @Input()
  public item: StakeholdersItem;
  @Output()
  public itemClick = new EventEmitter<{ conversationBody; conversationSid; customerName }>();
  @Output()
  public messageLoaded = new EventEmitter<number>();

  public unreadCount$: Observable<number>;
  public lastMessage$: Observable<Message>;

  private conversationSid: string;

  constructor(private chatService: ChatService, private changeDetectorRef: ChangeDetectorRef) {}

  public ngOnInit(): void {
    this.messageLoaded.next(1);
    const conversationBody = this.getConversationBody();
    this.chatService
      .getConversation(this.item.freelancerId, conversationBody)
      .pipe(
        switchMap(({ conversationSid }) => {
          this.conversationSid = conversationSid;
          return this.chatService.twilioClient.getConversationBySid(conversationSid);
        })
      )
      .subscribe((result) => {
        this.unreadCount$ = from(result.getUnreadMessagesCount());
        this.changeDetectorRef.markForCheck();
        this.lastMessage$ = from(result.getMessages()).pipe(
          map((messages) => {
            this.messageLoaded.next(-1);
            return messages.items[messages.items.length - 1];
          })
        );
      });
  }

  public clickItemHandler() {
    const conversationBody = this.getConversationBody();
    this.itemClick.next({
      conversationBody,
      conversationSid: this.conversationSid,
      customerName: this.item.accountName,
    });
  }

  private getConversationBody(): ConversationBody {
    const { contactId, projectId, contactName } = this.item;

    const conversationBody: ConversationBody = {
      customerId: contactId,
      projectId,
      contactName,
    };

    return conversationBody;
  }
}
