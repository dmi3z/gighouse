import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ChatItem } from './interfaces/chat-item.interface';
import { Location } from '@angular/common';

@Component({
  selector: 'app-chat-list',
  templateUrl: 'chat-list.component.html',
  styleUrls: ['chat-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatListComponent {
  public chatItems$: Observable<ChatItem[]>;
  public counter = 0;

  constructor(private location: Location) {}

  public back(): void {
    this.location.back();
  }
}
