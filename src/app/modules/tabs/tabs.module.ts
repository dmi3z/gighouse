import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { JobService } from '../../services/job.service';
import { FreelancerRepository } from 'app/services/freelancer.repository';

@NgModule({
  imports: [IonicModule, CommonModule, FormsModule, TranslateModule, TabsPageRoutingModule],
  declarations: [TabsPage],
  providers: [JobService, FreelancerRepository],
})
export class TabsPageModule {}
