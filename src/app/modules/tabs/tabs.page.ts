import { StorageService } from './../../services/storage.service';
import { ChatService } from '../../services/chat.service';
import { TimesheetIncompleteParams } from './../timesheets/models/timesheet-incomplete-params';
import { GighouseEvent } from './../../interfaces/models/gighouse-event';
import { EventsService } from './../../services/events.service';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { take, map, mergeMap, concatAll, switchMap, tap } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { AppState } from '@capacitor/core';
import { loadConversationsAction, updateUnreadMessagesCountAction } from 'app/store/chat/chat.actions';
import { forkJoin, from } from 'rxjs';
import { ConversationData } from 'app/interfaces/conversation-data.interface';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TabsPage implements OnInit {
  public tabs = [
    {
      title: 'navigation-bar.tab.jobs',
      icon: 'list',
      path: 'jobs',
      badge: null,
    },
    {
      title: 'navigation-bar.tab.interested-jobs',
      icon: 'heart',
      path: 'interested-jobs',
      badge: null,
    },
    {
      title: 'navigation-bar.tab.profile',
      icon: 'person',
      path: 'profile',
      badge: null,
    },
    {
      title: 'navigation-bar.tab.timesheets',
      icon: 'calendar',
      path: 'timesheets',
      badge: null,
    },
  ];

  constructor(
    private events: EventsService,
    private chatService: ChatService,
    private storageService: StorageService,
    private store$: Store<AppState>
  ) {}

  public ngOnInit(): void {
    const freelancerId = this.storageService.getFreelancerId();
    this.initTwilioClient(freelancerId);
    this.addSubscriptions();
  }

  private addSubscriptions(): void {
    this.events.subscribe(
      GighouseEvent.timesheetIncompleteNotificationReceived,
      (timesheetIncompleteParams: TimesheetIncompleteParams) => {
        this.handleIncompleteTimesheetNotification(timesheetIncompleteParams);
      }
    );

    this.events.subscribe(GighouseEvent.newJobNotificationReceived, () => (this.tabs[0].badge = '!'));
    this.events.subscribe(GighouseEvent.mutualInterestNotificationReceived, () => (this.tabs[1].badge = '!'));
  }

  private handleIncompleteTimesheetNotification(timesheetIncompleteParams: TimesheetIncompleteParams): void {
    if (timesheetIncompleteParams.isOnForeground()) {
      this.tabs[3].badge = '!';
    }
  }

  public ngOnDestroy(): void {
    this.events.destroy(GighouseEvent.timesheetIncompleteNotificationReceived);
  }

  private initTwilioClient(freelancerId: string): void {
    this.chatService
      .initClient(freelancerId)
      .pipe(
        map((conversation) =>
          conversation.items.map(
            (item): ConversationData => {
              return {
                conversation: item,
                messages: from(item.getMessages()),
                unreadCount: from(item.getUnreadMessagesCount()),
              };
            }
          )
        )
      )
      .subscribe((conversations) => {
        this.chatService.twilioClient.on('messageAdded', () => console.log('New message was come'));
        this.store$.dispatch(loadConversationsAction({ conversations }));
        this.chatService
          .getUnreadMessagesCount()
          .pipe(
            tap((counter) => this.store$.dispatch(updateUnreadMessagesCountAction({ counter: counter }))),
            take(1)
          )
          .subscribe();
      });
  }
}
