import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'jobs',
        loadChildren: () => import('../assignments/assignments.module').then((m) => m.AssignmentsModule),
      },
      {
        path: 'interested-jobs',
        loadChildren: () => import('../interests/interests.module').then((m) => m.InterestsPageModule),
      },
      {
        path: 'chat-list',
        loadChildren: () => import('../chat-list/chat-list.module').then((m) => m.ChatListModule),
      },
      {
        path: 'profile',
        loadChildren: () => import('../profile/profile.module').then((m) => m.ProfileModule),
      },
      {
        path: 'timesheets',
        loadChildren: () => import('../timesheets/timesheets.module').then((m) => m.TimesheetsModule),
      },
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
