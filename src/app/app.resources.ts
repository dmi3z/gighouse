export const resources = {
  appVersionAllowedResource: '/api/public/v1/versions/app-allowed',
  resetPasswordResource: '/api/public/v1/freelancers/reset-password',

  freelancerResourceBaseUrl: '/api/private/v1/freelancers',
  notificationResourceBaseUrl: '/api/private/v1/freelancers/notifications',

  jobResourceBaseUrl: '/api/private/v1/freelancers/jobs',

  nicheMetadataResource: '/api/public/v1/metadata/niches',
  profileMetadataResource: '/api/public/v1/metadata/profiles',
  languageSkillLevelMetadataResource: '/api/public/v1/metadata/language-skill-levels',
  expertiseMetadataResource: '/api/public/v1/metadata/expertises',
  maximumTravelTimesMetadataResource: '/api/public/v1/metadata/maximum-travel-times',
  companyFormsMetadataResource: '/api/public/v1/metadata/company-forms',

  jobResource: '/api/private/v1/jobs',
  chatResourceBaseUrl: '/api/private/v1/freelancers/chat/',
};
