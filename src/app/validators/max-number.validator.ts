import { Directive } from "@angular/core";
import { AbstractControl, NG_VALIDATORS } from "@angular/forms";
import { NumberUtils } from "app/pipes/number-utils";

export function validateMaxNumber(maxNumber: number): any {
  return (control: AbstractControl): any => {
    if (!control) {
      return {'max': {'valid': false}};
    }
    if (!control.value || !control.value) {
      return null;
    }

    const number: number = NumberUtils.parseToNumber(control.value.toString());

    if (number > maxNumber) {
      return {max: {max: maxNumber, actual: number}};
    }
    return null;
  };
}

@Directive({
  selector: '[appMaxNumberValidator]',
  providers: [
    {provide: NG_VALIDATORS, multi: true, useValue: validateMaxNumber}
  ]
})
export class MaxNumberValidatorDirective {
}
