import { Observable } from 'rxjs';
import { Conversation } from '@twilio/conversations/lib/conversation';
import { Paginator } from '@twilio/conversations/lib/interfaces/paginator';
import { Message } from '@twilio/conversations/lib/message';

export interface ConversationData {
  conversation: Conversation;
  messages: Observable<Paginator<Message>>;
  unreadCount: Observable<number>;
}
