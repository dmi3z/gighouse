import { JobSkillRequirement } from './job-skill-requirement';

export class JobSkill {
  constructor(
    private readonly description: string,
    private readonly requirement: JobSkillRequirement,
    private readonly requirementLabel: string
  ) {}

  static from(jobSkillParams: JobSkillParams): JobSkill {
    return new JobSkill(
      jobSkillParams.description,
      jobSkillParams.requirement.value,
      jobSkillParams.requirement.label
    );
  }

  getDescription(): string {
    return this.description;
  }

  getRequirement(): JobSkillRequirement {
    return this.requirement;
  }

  getFormattedRequirementLabel(): string {
    if (Object.is(this.requirement, JobSkillRequirement.OPTIONAL)) {
      // !!!! isEqual
      return '(' + this.requirementLabel + ')';
    }
    return '';
  }
}

export interface JobSkillParams {
  description: string;
  requirement: JobSkillRequirementParams;
}

export interface JobSkillRequirementParams {
  value: JobSkillRequirement;
  label: string;
}
