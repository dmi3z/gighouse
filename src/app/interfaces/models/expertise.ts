export class Expertise {
  constructor(private readonly value: string, private readonly label: string) {
    this.value = value;
    this.label = label;
  }

  static from(params: ExpertiseParams): Expertise {
    return new Expertise(params.value, params.label);
  }

  public getValue(): string {
    return this.value;
  }

  public getLabel(): string {
    return this.label;
  }
}

export interface ExpertiseParams {
  value: string;
  label: string;
}
