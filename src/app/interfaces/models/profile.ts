export class Profile {
  constructor(
    private readonly id: string,
    private readonly name: string,
    private readonly expertiseValue: string
  ) {
    this.id = id;
    this.name = name;
    this.expertiseValue = expertiseValue;
  }

  static from(params: ProfileParams): Profile {
    return new Profile(params.id, params.name, params.expertiseValue);
  }

  public getId(): string {
    return this.id;
  }

  public getName(): string {
    return this.name;
  }

  public getExpertiseValue(): string {
    return this.expertiseValue;
  }
}

export interface ProfileParams {
  id: string;
  name: string;
  expertiseValue: string;
}
