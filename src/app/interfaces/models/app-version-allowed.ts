export class AppVersionAllowed {
  private readonly allowed: boolean;

  constructor(allowed: boolean) {
    this.allowed = allowed;
  }

  isAllowed(): boolean {
    return this.allowed;
  }
}

export interface AppVersionAllowedParams {
  allowed: boolean;
}
