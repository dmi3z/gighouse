export class Job {
  constructor(
    private readonly _id: string,
    private readonly _name: string,
    private readonly _location: string,
    private readonly _fee: number,
    private readonly _daysAWeek: number,
    private readonly _startDate: string,
    private readonly _teaserText1: string,
    private readonly _teaserText2: string,
    private readonly _teaserText3: string
  ) {
    this._id = _id;
    this._name = _name;
    this._location = _location;
    this._fee = _fee;
    this._daysAWeek = _daysAWeek;
    this._startDate = _startDate;
    this._teaserText1 = _teaserText1;
    this._teaserText2 = _teaserText2;
    this._teaserText3 = _teaserText3;
  }

  getId(): string {
    return this._id;
  }

  getName(): string {
    return this._name;
  }

  getLocation(): string {
    return this._location;
  }

  getFee(): number {
    return this._fee;
  }

  getDaysAWeek(): number {
    return this._daysAWeek;
  }

  getStartDate(): string {
    return this._startDate;
  }

  getTeaserText1(): string {
    return this._teaserText1;
  }

  getTeaserText2(): string {
    return this._teaserText2;
  }

  getTeaserText3(): string {
    return this._teaserText3;
  }
}

export interface TeaserJobParams {
  name: string;
  location: string;
  fee: number;
  daysAWeek: number;
  startDate: string;
  teaserText1: string;
  teaserText2: string;
  teaserText3: string;
}

export interface JobParams {
  id: string;
  name: string;
  location: string;
  fee: number;
  daysAWeek: number;
  startDate: string;
  teaserText1: string;
  teaserText2: string;
  teaserText3: string;
}
