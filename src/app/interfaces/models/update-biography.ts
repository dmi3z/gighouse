export class UpdateBiographyRequest {
  biography: string;

  constructor(biography: string) {
    this.biography = biography;
  }
}
