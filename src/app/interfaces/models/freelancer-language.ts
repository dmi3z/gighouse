export class FreelancerLanguage {
  id: string;
  languageValue: string;
  languageLabel: string;
  languageSkillLevelValue: string;

  constructor(id: string, languageValue: string, languageLabel: string, languageSkillLevelValue: string) {
    this.id = id;
    this.languageValue = languageValue;
    this.languageLabel = languageLabel;
    this.languageSkillLevelValue = languageSkillLevelValue;
  }
}

export interface FreelancerLanguageParams {
  id: string;
  languageValue: string;
  languageLabel: string;
  languageSkillLevelValue: string;
}
