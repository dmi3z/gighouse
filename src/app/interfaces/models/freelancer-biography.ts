export class FreelancerBiography {
  biography: string;

  constructor(biography: string) {
    this.biography = biography;
  }

  static from(params: FreelancerBiographyParams): FreelancerBiography {
    return new FreelancerBiography(params.biography);
  }
}

export interface FreelancerBiographyParams {
  biography: string;
}
