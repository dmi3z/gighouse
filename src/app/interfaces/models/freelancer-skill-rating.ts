export class FreelancerSkillRating {
  skillId: string;
  rating: number;

  constructor(skillId: string, rating: number) {
    this.skillId = skillId;
    this.rating = rating;
  }
}
