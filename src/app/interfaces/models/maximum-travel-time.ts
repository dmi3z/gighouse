export class MaximumTravelTime {
  constructor(
    private readonly key: string,
    private readonly translation: string
  ) {}

  public getKey(): string {
    return this.key;
  }

  public getTranslation(): string {
    return this.translation;
  }
}
