export class TranslatableError implements Error {
  key: string;
  originalError: any;
  message: string;
  name: string;
  stack: string;

  constructor(key: string, error: Error) {
    this.key = key;
    this.originalError = error;
    this.message = error.message;
    this.name = error.name;
    this.stack = error.stack;
  }
}
