import { Expertise, ExpertiseParams } from './expertise';

export class FreelancerProfile {
  constructor(private readonly id: string, private readonly name: string, private readonly expertise: Expertise) {
    this.id = id;
    this.name = name;
    this.expertise = expertise;
  }

  static from(params: FreelancerProfileParams): FreelancerProfile {
    return new FreelancerProfile(params.id, params.name, Expertise.from(params.expertise));
  }

  public getId(): string {
    return this.id;
  }

  public getName(): string {
    return this.name;
  }

  public getExpertise(): Expertise {
    return this.expertise;
  }
}

export interface FreelancerProfileParams {
  id: string;
  name: string;
  expertise: ExpertiseParams;
}
