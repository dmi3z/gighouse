export class JobPreferences {
  available: boolean;
  availabilityDate: Date;
  daysAWeek: number;
  willingToTravelAbroad: boolean;
  maximumTravelTime: string;
  maximumTravelDistance: number;
  teleworking: boolean;
  hasOwnCar: boolean;
  dayFee: number;
  dayFeeNegotiable: boolean;
  hourlyFee: number;
  feeComment: string;

  constructor(available: boolean,
              availabilityDate: Date,
              daysAWeek: number,
              willingToTravelAbroad: boolean,
              maximumTravelTime: string,
              maximumTravelDistance: number,
              teleworking: boolean,
              hasOwnCar: boolean,
              dayFee: number,
              dayFeeNegotiable: boolean,
              hourlyFee: number,
              feeComment: string) {
    this.available = available;
    this.availabilityDate = availabilityDate;
    this.daysAWeek = daysAWeek;
    this.willingToTravelAbroad = willingToTravelAbroad;
    this.maximumTravelTime = maximumTravelTime;
    this.maximumTravelDistance = maximumTravelDistance;
    this.teleworking = teleworking;
    this.hasOwnCar = hasOwnCar;
    this.dayFee = dayFee;
    this.dayFeeNegotiable = dayFeeNegotiable;
    this.hourlyFee = hourlyFee;
    this.feeComment = feeComment;
  }

  static from(params: JobPreferencesParams): JobPreferences {
    return new JobPreferences(
      params.available,
      params.availabilityDate,
      params.daysAWeek,
      params.willingToTravelAbroad,
      params.maximumTravelTime,
      params.maximumTravelDistance,
      params.teleworking,
      params.hasOwnCar,
      params.dayFee,
      params.dayFeeNegotiable,
      params.hourlyFee,
      params.feeComment);
  }
}

export interface JobPreferencesParams {
  available: boolean;
  availabilityDate: Date;
  daysAWeek: number;
  willingToTravelAbroad: boolean;
  maximumTravelTime: string;
  maximumTravelDistance: number;
  teleworking: boolean;
  hasOwnCar: boolean;
  dayFee: number;
  dayFeeNegotiable: boolean;
  hourlyFee: number;
  feeComment: string;
}


