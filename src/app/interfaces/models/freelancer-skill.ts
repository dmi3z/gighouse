import { FreelancerSkillRating } from './freelancer-skill-rating';
export class FreelancerSkill implements FreelancerSkillParams {
  id: string;
  description: string;
  skills: FreelancerSkill[];
  tools: FreelancerSkill[];
  rating: number;

  constructor(
    id: string,
    description: string,
    skills: FreelancerSkillParams[],
    tools: FreelancerSkillParams[],
    rating: number
  ) {
    this.id = id;
    this.description = description;
    this.skills = skills.map(
      (skill) =>
        new FreelancerSkill(
          skill.id,
          skill.description,
          skill.skills,
          skill.tools,
          skill.rating
        )
    );
    this.tools = tools.map(
      (skill) =>
        new FreelancerSkill(
          skill.id,
          skill.description,
          skill.skills,
          skill.tools,
          skill.rating
        )
    );
    this.rating = rating;
  }

  getAmountOfSkills(): number {
    return this.getActiveFreelancerSkills().length;
  }

  extractActiveFreelancerSkillIds(skillRatings: FreelancerSkillRating[]) {
    this.getActiveFreelancerSkills().forEach((child: FreelancerSkill) => {
      skillRatings.push(new FreelancerSkillRating(child.id, child.rating));
    });
  }

  getActiveFreelancerSkills(): FreelancerSkill[] {
    let activeFreelancerSkills: FreelancerSkill[] = [];
    this.skills.forEach(function (child: FreelancerSkill) {
      if (child.rating > 0) {
        activeFreelancerSkills.push(child);
      }
      activeFreelancerSkills = [
        ...activeFreelancerSkills,
        ...child.getActiveFreelancerSkills(),
      ];
    });
    this.tools.forEach((child: FreelancerSkill) => {
      if (child.rating > 0) {
        activeFreelancerSkills.push(child);
      }
      activeFreelancerSkills = [
        ...activeFreelancerSkills,
        ...child.getActiveFreelancerSkills(),
      ];
    });
    return activeFreelancerSkills;
  }
}

export interface FreelancerSkillParams {
  id: string;
  description: string;
  skills: FreelancerSkillParams[];
  tools: FreelancerSkillParams[];
  rating: number;
}
