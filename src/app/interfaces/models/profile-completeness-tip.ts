export enum ProfileCompletenessTip {
  addLanguage = 'ADD_LANGUAGE',
  addAspiredProfile = 'ADD_ASPIRED_PROFILE',
  addSkill = 'ADD_SKILL',
  addGeneralInformation = 'ADD_GENERAL_INFORMATION'
}
