export enum GighouseEvent {
  profileCompletenessChanged = 'profile-completeness:changed',
  timesheetEntryComponentAdded = 'timesheet-entry:component-added',
  timesheetIncompleteNotificationReceived = 'timesheet-incomplete-notification:received',
  newJobNotificationReceived = 'new-job-notification:received',
  mutualInterestNotificationReceived = 'mutual-interest-notification:received',
  freelancerJobInterestUpdated = 'freelancer-job-interest:updated',
  timesheetUpdated = 'timesheet:updated',
  timesheetsUpdated = 'timesheets:updated',
  freelancerLogout = 'freelancer:logout',
  otherAspiredProfileRefreshed = 'otherAspiredProfile:refreshed',
}
