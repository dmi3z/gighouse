export class FreelancerRegistrationParams {
  private readonly userId: string;
  private readonly mobilePhone: string;
  private readonly newsletterAllowed: boolean;

  constructor(userId: string, mobilePhone: string, newsletterAllowed: boolean) {
    this.userId = userId;
    this.mobilePhone = mobilePhone;
    this.newsletterAllowed = newsletterAllowed;
  }
}
