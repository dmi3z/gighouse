import {
  AspiredFreelancerProfile,
  AspiredFreelancerProfileParams,
} from './aspired-freelancer-profile';

export class AspiredFreelancerProfilesByExpertise {
  expertiseLabel: string;
  profiles: AspiredFreelancerProfile[];

  constructor(expertiseLabel: string, profiles: AspiredFreelancerProfile[]) {
    this.expertiseLabel = expertiseLabel;
    this.profiles = profiles;
  }

  static from(
    aspiredFreelancerProfilesByExpertiseParams: AspiredFreelancerProfilesByExpertiseParams
  ): AspiredFreelancerProfilesByExpertise {
    return new AspiredFreelancerProfilesByExpertise(
      aspiredFreelancerProfilesByExpertiseParams.expertiseLabel,
      aspiredFreelancerProfilesByExpertiseParams.profiles.map(
        (profile: AspiredFreelancerProfileParams) =>
          AspiredFreelancerProfile.from(profile)
      )
    );
  }

  countSelectedProfiles(): number {
    return this.profiles.filter((profile) => profile.active).length;
  }
}

export interface AspiredFreelancerProfilesByExpertiseParams {
  expertiseLabel: string;
  profiles: AspiredFreelancerProfileParams[];
}
