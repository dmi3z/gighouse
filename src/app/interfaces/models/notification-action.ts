export enum NotificationAction {
  timesheetIncomplete = 'TIMESHEET_INCOMPLETE',
  newJob = 'NEW_JOB',
  mutualInterest = 'MUTUAL_INTEREST'
}
