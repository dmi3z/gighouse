export enum BackButtonPriority {
  low = 100,
  medium = 200,
  high = 300
}
