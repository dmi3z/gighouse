export class RegisterDeviceParams {

  private readonly freelancerId: string;
  private readonly deviceToken: string;
  private readonly platform: string;

  constructor(freelancerId: string, deviceToken: string, platform: string) {
    this.freelancerId = freelancerId;
    this.deviceToken = deviceToken;
    this.platform = platform;
  }

  getFreelancerId(): string {
    return this.freelancerId;
  }

  getDeviceToken(): string {
    return this.deviceToken;
  }

  getPlatform(): string {
    return this.platform;
  }
}
