import { CompanyForm, CompanyFormParams } from './company-form';

export class FreelancerCompany {
  name: string;
  companyForm: CompanyForm;
  vatNumber: string;
  street: string;
  postalCode: string;
  city: string;
  bankAccount: string;

  constructor(
    name: string,
    companyForm: CompanyForm,
    vatNumber: string,
    street: string,
    postalCode: string,
    city: string,
    bankAccount: string
  ) {
    this.name = name;
    this.companyForm = companyForm;
    this.vatNumber = vatNumber;
    this.street = street;
    this.postalCode = postalCode;
    this.city = city;
    this.bankAccount = bankAccount;
  }

  static from(params: FreelancerCompanyParams): FreelancerCompany {
    return new FreelancerCompany(
      params.name,
      CompanyForm.from(params.companyForm),
      params.vatNumber,
      params.street,
      params.postalCode,
      params.city,
      params.bankAccount
    );
  }
}

export interface FreelancerCompanyParams {
  name: string;
  companyForm: CompanyFormParams;
  vatNumber: string;
  street: string;
  postalCode: string;
  city: string;
  bankAccount: string;
}
