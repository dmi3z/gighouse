import { FreelancerCompany } from './freelancer-company';
import { FreelancerDetails } from './freelancer-details';
import { JobPreferences } from './job-preferences';

export interface UpdateFreelancerRequest {
  freelancerId: string;
  firstName: string;
  lastName: string;
  mobilePhone: string;
  email: string;
  profileId: string;
  company: UpdateFreelancerCompanyRequest;
  jobPreferences: JobPreferences;
}

export class UpdateOnboardingFreelancerRequest
  implements UpdateFreelancerRequest {
  freelancerId: string;
  firstName: string;
  lastName: string;
  mobilePhone: string;
  email: string;
  profileId: string;
  company: UpdateFreelancerCompanyRequest;
  jobPreferences: JobPreferences;

  constructor(
    freelancerDetails: FreelancerDetails,
    profileId: string,
    postalCode: string,
    maximumTravelTime: string
  ) {
    this.freelancerId = freelancerDetails.freelancerId;
    this.firstName = freelancerDetails.firstName;
    this.lastName = freelancerDetails.lastName;
    this.mobilePhone = freelancerDetails.mobilePhone;
    this.email = freelancerDetails.email;
    this.profileId = profileId;
    this.company = new UpdateFreelancerCompanyRequest(
      freelancerDetails.company.name,
      !!freelancerDetails.company.companyForm
        ? freelancerDetails.company.companyForm.getValue()
        : null,
      freelancerDetails.company.vatNumber,
      freelancerDetails.company.street,
      postalCode,
      freelancerDetails.company.city,
      freelancerDetails.company.bankAccount
    );
    this.jobPreferences = new JobPreferences(
      freelancerDetails.jobPreferences.available,
      freelancerDetails.jobPreferences.availabilityDate,
      freelancerDetails.jobPreferences.daysAWeek,
      freelancerDetails.jobPreferences.willingToTravelAbroad,
      maximumTravelTime,
      freelancerDetails.jobPreferences.maximumTravelDistance,
      freelancerDetails.jobPreferences.teleworking,
      freelancerDetails.jobPreferences.hasOwnCar,
      freelancerDetails.jobPreferences.dayFee,
      freelancerDetails.jobPreferences.dayFeeNegotiable,
      freelancerDetails.jobPreferences.hourlyFee,
      freelancerDetails.jobPreferences.feeComment
    );
  }
}

export class UpdateFreelancerProfileDetailsRequest
  implements UpdateFreelancerRequest {
  freelancerId: string;
  firstName: string;
  lastName: string;
  mobilePhone: string;
  email: string;
  profileId: string;
  company: UpdateFreelancerCompanyRequest;
  jobPreferences: JobPreferences;

  constructor(
    freelancerDetails: FreelancerDetails,
    firstName: string,
    lastName: string,
    mobilePhone: string,
    email: string,
    profileId: string,
    company: UpdateFreelancerCompanyRequest
  ) {
    this.freelancerId = freelancerDetails.freelancerId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.mobilePhone = mobilePhone;
    this.email = email;
    this.profileId = profileId;
    this.company = company;
    this.jobPreferences = freelancerDetails.jobPreferences;
  }
}

export class UpdateFreelancerCompanyRequest {
  name: string;
  companyFormValue: string;
  vatNumber: string;
  street: string;
  postalCode: string;
  city: string;
  bankAccount: string;

  constructor(
    name: string,
    companyFormValue: string,
    vatNumber: string,
    street: string,
    postalCode: string,
    city: string,
    bankAccount: string
  ) {
    this.name = name;
    this.companyFormValue = companyFormValue;
    this.vatNumber = vatNumber;
    this.street = street;
    this.postalCode = postalCode;
    this.city = city;
    this.bankAccount = bankAccount;
  }
}

export class UpdateFreelancerJobPreferencesRequest
  implements UpdateFreelancerRequest {
  freelancerId: string;
  firstName: string;
  lastName: string;
  mobilePhone: string;
  email: string;
  profileId: string;
  company: UpdateFreelancerCompanyRequest;
  jobPreferences: JobPreferences;

  constructor(
    freelancerDetails: FreelancerDetails,
    jobPreferences: JobPreferences
  ) {
    this.freelancerId = freelancerDetails.freelancerId;
    this.firstName = freelancerDetails.firstName;
    this.lastName = freelancerDetails.lastName;
    this.mobilePhone = freelancerDetails.mobilePhone;
    this.email = freelancerDetails.email;
    this.profileId = freelancerDetails.profile.getId();
    this.company = this.mapFreelancerCompanyToUpdateRequest(
      freelancerDetails.company
    );
    this.jobPreferences = jobPreferences;
  }

  private mapFreelancerCompanyToUpdateRequest(
    freelancerCompany: FreelancerCompany
  ): UpdateFreelancerCompanyRequest {
    return new UpdateFreelancerCompanyRequest(
      freelancerCompany.name,
      !!freelancerCompany.companyForm
        ? freelancerCompany.companyForm.getValue()
        : null, // !!!! isNil
      freelancerCompany.vatNumber,
      freelancerCompany.street,
      freelancerCompany.postalCode,
      freelancerCompany.city,
      freelancerCompany.bankAccount
    );
  }
}
