export class OtherAspiredProfile {
  constructor(private readonly _other: string) {
  }

  get other(): string {
    return this._other;
  }
}

export interface OtherAspiredProfileParams {
  other: string;
}
