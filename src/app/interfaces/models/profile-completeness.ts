import { ProfileCompletenessTip } from './profile-completeness-tip';

export class ProfileCompleteness {
  private readonly percentageComplete: number;
  private readonly tips: ProfileCompletenessTip[];

  constructor(percentageComplete: number, tips: ProfileCompletenessTip[]) {
    this.percentageComplete = percentageComplete;
    this.tips = tips;
  }

  static from(params: ProfileCompletenessParams): ProfileCompleteness {
    return new ProfileCompleteness(params.percentageComplete, params.tips);
  }

  public getPercentageComplete(): number {
    return this.percentageComplete;
  }

  public isPercentage100Percent(): boolean {
    return this.percentageComplete === 100;
  }

  public getTips(): ProfileCompletenessTip[] {
    return this.tips;
  }
}

export interface ProfileCompletenessParams {
  percentageComplete: number;
  tips: ProfileCompletenessTip[];
}
