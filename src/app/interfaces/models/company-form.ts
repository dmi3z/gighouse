export class CompanyForm {
  constructor(private readonly value: string, private readonly label: string) {
  }

  static from(companyFormParams: CompanyFormParams): CompanyForm {
    return companyFormParams != null ? new CompanyForm(companyFormParams.value, companyFormParams.label) : null;
  }

  getValue(): string {
    return this.value;
  }

  getLabel(): string {
    return this.label;
  }
}

export interface CompanyFormParams {
  value: string;
  label: string;
}
