export class UnregisterDeviceParams {

  private readonly freelancerId: string;
  private readonly deviceToken: string;

  constructor(freelancerId: string, deviceToken: string) {
    this.freelancerId = freelancerId;
    this.deviceToken = deviceToken;
  }

  getFreelancerId(): string {
    return this.freelancerId;
  }

  getDeviceToken(): string {
    return this.deviceToken;
  }
}
