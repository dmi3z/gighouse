export class AspiredFreelancerProfile {
  id: string;
  description: string;
  active: boolean;

  constructor(id: string, description: string, active: boolean) {
    this.id = id;
    this.description = description;
    this.active = active;
  }

  static from(aspiredFreelancerProfileParams: AspiredFreelancerProfileParams): AspiredFreelancerProfile {
    return new AspiredFreelancerProfile(
      aspiredFreelancerProfileParams.id,
      aspiredFreelancerProfileParams.description,
      aspiredFreelancerProfileParams.active
    );
  }
}

export interface AspiredFreelancerProfileParams {
  id: string;
  description: string;
  active: boolean;
}
