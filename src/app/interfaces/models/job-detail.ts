import { JobSkill, JobSkillParams } from './job-skill';

export class JobDetail {
  constructor(
    private readonly _jobId: string,
    private readonly _profileLabel: string,
    private readonly _description: string,
    private readonly _skills: JobSkill[],
    private readonly _region: string,
    private readonly _feeAmount: number,
    private readonly _commissionPercentage: number,
    private readonly _startDate: string,
    private readonly _projectDurationInWeeks: number,
    private readonly _daysAWeek: number,
    private readonly _descriptionAsRichText: string
  ) {
    this._jobId = _jobId;
    this._profileLabel = _profileLabel;
    this._description = _description;
    this._skills = _skills;
    this._region = _region;
    this._feeAmount = _feeAmount;
    this._commissionPercentage = _commissionPercentage;
    this._startDate = _startDate;
    this._projectDurationInWeeks = _projectDurationInWeeks;
    this._daysAWeek = _daysAWeek;
    this._descriptionAsRichText = _descriptionAsRichText;
  }

  static from(jobDetailParams: JobDetailParams): JobDetail {
    return new JobDetail(
      jobDetailParams.jobId,
      jobDetailParams.profileLabel,
      jobDetailParams.description,
      jobDetailParams.skills.map((skill) => JobSkill.from(skill)),
      jobDetailParams.region,
      jobDetailParams.feeAmount,
      jobDetailParams.commissionPercentage,
      jobDetailParams.startDate,
      jobDetailParams.projectDurationInWeeks,
      jobDetailParams.daysAWeek,
      jobDetailParams.descriptionAsRichText
    );
  }

  getJobId(): string {
    return this._jobId;
  }

  getProfileLabel(): string {
    return this._profileLabel;
  }

  getDescription(): string {
    return this._description;
  }

  getSkills(): JobSkill[] {
    return this._skills;
  }

  getRegion(): string {
    return this._region;
  }

  getFeeAmount(): number {
    return this._feeAmount;
  }

  getCommissionPercentage(): number {
    return this._commissionPercentage;
  }

  getStartDate(): string {
    return this._startDate;
  }

  getProjectDurationInWeeks(): number {
    return this._projectDurationInWeeks;
  }

  getDaysAWeek(): number {
    return this._daysAWeek;
  }

  getDescriptionAsRichText(): string {
    return this._descriptionAsRichText;
  }
}

export interface JobDetailParams {
  jobId: string;
  profileLabel: string;
  description: string;
  skills: JobSkillParams[];
  region: string;
  feeAmount: number;
  commissionPercentage: number;
  startDate: string;
  projectDurationInWeeks: number;
  daysAWeek: number;
  descriptionAsRichText: string;
}
