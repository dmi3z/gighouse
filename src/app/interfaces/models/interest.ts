export enum Interest {
  interested = 'INTERESTED',
  not_interested = 'NOT_INTERESTED'
}
