export class LanguageSkillLevel {
  value: string;
  label: string;

  constructor(value: string, label: string) {
    this.value = value;
    this.label = label;
  }
}

export interface LanguageSkillLevelParams {
  value: string;
  label: string;
}
