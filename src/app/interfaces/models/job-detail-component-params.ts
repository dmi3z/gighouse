import { JobType } from "../job-type";

export interface JobDetailComponentParams {
  jobId: string;
  jobType: JobType;
}
