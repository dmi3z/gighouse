import {DatePipe} from '@angular/common';

export class DateUtils {
  static formatDate(date: Date): string {
    const pipe = new DatePipe('en-US');
    return pipe.transform(date, 'yyyy-MM-dd');
  }
}
