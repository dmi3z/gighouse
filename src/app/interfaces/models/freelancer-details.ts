import { FreelancerCompany, FreelancerCompanyParams } from './freelancer-company';
import { FreelancerProfile, FreelancerProfileParams } from './freelancer-profile';
import { JobPreferences, JobPreferencesParams } from './job-preferences';

export class FreelancerDetails {
  freelancerId: string;
  firstName: string;
  lastName: string;
  mobilePhone: string;
  email: string;
  profile: FreelancerProfile;
  company: FreelancerCompany;
  jobPreferences: JobPreferences;

  constructor(
    freelancerId: string,
    firstName: string,
    lastName: string,
    mobilePhone: string,
    email: string,
    profile: FreelancerProfile,
    company: FreelancerCompany,
    jobPreferences: JobPreferences
  ) {
    this.freelancerId = freelancerId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.mobilePhone = mobilePhone;
    this.email = email;
    this.profile = profile;
    this.company = company;
    this.jobPreferences = jobPreferences;
  }

  static from(params: FreelancerDetailsParams): FreelancerDetails {
    return new FreelancerDetails(
      params.freelancerId,
      params.firstName,
      params.lastName,
      params.mobilePhone,
      params.email,
      FreelancerProfile.from(params.profile),
      FreelancerCompany.from(params.company),
      JobPreferences.from(params.jobPreferences)
    );
  }
}

export interface FreelancerDetailsParams {
  freelancerId: string;
  firstName: string;
  lastName: string;
  mobilePhone: string;
  email: string;
  profile: FreelancerProfileParams;
  company: FreelancerCompanyParams;
  jobPreferences: JobPreferencesParams;
}
