export interface UnifiedBackendApiResponse<T> {
  data: T;
}
