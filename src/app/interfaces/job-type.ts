export enum JobType {
  interestedJob = 'INTERESTED_JOB',
  possibleJob = 'POSSIBLE_JOB'
}
