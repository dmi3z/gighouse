export interface LocalTranslations {
  data: {
    [lang: string]: {
      [key: string]: string;
    };
  };
  version: string;
}
