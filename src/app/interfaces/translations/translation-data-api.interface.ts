import { TranslationApi } from './translation-api.interface';

export interface TranslationDataApi {
  translations: TranslationApi[];
  version: string;
}
