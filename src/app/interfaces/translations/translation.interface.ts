export interface Translation {
  version: string;
  id: number;
  key: string;
  language: {
    [language: string]: string;
  };
}
