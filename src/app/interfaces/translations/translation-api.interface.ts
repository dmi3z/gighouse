export interface TranslationApi {
  version: string;
  id: number;
  key: string;
  language: {
    [language: string]: string;
  };
}
