export interface Translations {
  data: {
    [language: string]: {
      [key: string]: string;
    };
  };
  version: string;
}
