import { Translation } from './translation.interface';

export interface TranslationData {
  translations: Translation[];
  version: string;
}
