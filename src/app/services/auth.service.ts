import { EventsService } from './events.service';
import { GighouseEvent } from './../interfaces/models/gighouse-event';
import { LocalStorage } from '../constants/local-storage';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { combineLatest, Observable, Subscriber, Subject } from 'rxjs';
import { map, flatMap, take, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { AuthResult } from '../interfaces/models/auth-result';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from './storage.service';
import { SignupResult } from '../interfaces/models/signup-result';
import { resources } from '../app.resources';
import { WebViewService } from './webview.service';
import { WebAuth } from 'auth0-js';

const FREELANCER_ID_PROPERTY = 'http://gighouse.be/freelancerId';
const USER_METADATA_PROPERTY = 'http://gighouse.be/user_metadata';
const USER_ID_PROPERTY = 'sub';
const USER_NAME = 'name';
const AUTH0_PROVIDER_PREFIX = 'auth0|';
const RESET_PASSWORD_RESOURCE_URL = resources.resetPasswordResource;

const auth0Config = {
  clientID: environment.authConfig.clientID,
  clientId: environment.authConfig.clientId,
  domain: environment.authConfig.domain,
  audience: environment.authConfig.audience,
  redirectUri: environment.authConfig.redirectUri,
  returnTo: environment.authConfig.returnTo,
  scope: environment.authConfig.scope,
  responseType: environment.authConfig.responseType,
  callbackURL: environment.authConfig.redirectUri,
  packageIdentifier: environment.authConfig.packageIdentifier,
  nonce: '123456',
};

interface CodeResponse {
  code: string;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  private static redirectSubject: Subject<AuthResult> = new Subject<AuthResult>();
  public loggedIn: boolean;
  public Auth0: WebAuth;
  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService,
    private storageService: StorageService,
    private webViewService: WebViewService,
    private events: EventsService
  ) {
    this.Auth0 = new WebAuth(environment.authConfig);
  }

  public loginWithLinkedIn() {
    // return this.linkedinService
    //   .loginWithLinkedin()
    //   .pipe(flatMap((loginLinkedInResult: CodeResponse) => this.convertLinkedinCodeIntoIdToken(loginLinkedInResult)));
  }

  public logout(): Observable<void> {

    return new Observable<void>((subscriber: Subscriber<void>) => {
      const url = this.Auth0.client.buildLogoutUrl({
        clientID: auth0Config.clientID,
        returnTo: auth0Config.returnTo,
      });
      this.webViewService.openLink(url, true).subscribe((isOpen: boolean) => {
        if (isOpen) {
          AuthService.redirectSubject.pipe(take(1)).subscribe(() => {
            this.events.publish(GighouseEvent.freelancerLogout);
            this.webViewService.closeWebView();
            subscriber.next();
            subscriber.complete();
          });
        } else {
          subscriber.error();
        }
      });
      localStorage.clear();
    });
  }

  public login(isSilent: boolean = false, options: any = {}): Observable<boolean> {
    return new Observable((subscriber: Subscriber<boolean>) => {
      const url = this.Auth0.client.buildAuthorizeUrl({
        ...auth0Config,
        ...(isSilent ? { prompt: 'none' } : { prompt: 'login' }),
        ...options,
      } as any);

      this.Auth0.checkSession({}, (err, auth) => {});
      this.webViewService.openLink(url, isSilent).subscribe(
        (isOpen: boolean) => {
          if (isOpen) {
            AuthService.redirectSubject.pipe(take(1)).subscribe((result: AuthResult) => {
              console.log('Result');

              if (result.error && result.error === 'login_required') {
                this.unsetSession();
                subscriber.next(false);
                subscriber.complete();
              } else if (result) {
                this.setSession(result);
                subscriber.next(true);
                this.webViewService.closeWebView();
                subscriber.complete();
              } else {
                subscriber.next(false);
                subscriber.complete();
              }
            });
          } else {
            subscriber.error();
          }
        },
        (error) => {
          subscriber.error(error);
        }
      );
    });
  }

  public isLoggedIn(): boolean {
    const accessTokenPresent = this.storageService.getAccessToken();
    const freelancerIdPresent = this.storageService.getFreelancerId();

    return !!accessTokenPresent && !!freelancerIdPresent;
  }

  public async updateUserMetadata(data: any = {}) {
    const userId = this.storageService.getUserId();

    return this.http
      .post(`${environment.auth0ManagementConfig.baseUrl}/oauth/token`, {
        client_id: environment.auth0ManagementConfig.client_id,
        client_secret: environment.auth0ManagementConfig.client_secret,
        grant_type: environment.auth0ManagementConfig.grant_type,
        audience: environment.auth0ManagementConfig.audience,
      })
      .toPromise()
      .then((response: { access_token: string }) =>
        this.http
          .patch(
            `${environment.authConfig.baseUrl}/api/v2/users/${userId}`,
            {
              user_metadata: data,
            },
            {
              headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${response.access_token}`,
              },
            }
          )
          .toPromise()
      );
  }

  public register(email: string, password: string, firstName: string, lastName: string): Observable<boolean> {
    const registerParams = {
      client_id: environment.authConfig.clientId,
      email: email,
      password: password,
      connection: environment.authConfig.connection,
      user_metadata: {
        onboardingFirstName: firstName,
        onboardingLastName: lastName,
      },
    };

    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http
      .post(environment.authConfig.baseUrl + '/dbconnections/signup', registerParams, { headers: headers })
      .pipe(
        flatMap((signupResult: SignupResult) => {
          console.log('SignUpresults: ', AUTH0_PROVIDER_PREFIX, signupResult);

          this.storageService.setUserId(`${AUTH0_PROVIDER_PREFIX}${signupResult._id}`);
          return this.login();
        })
      );
  }

  public resetPassword(email: string): Observable<void> {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<void>(
      `${environment.serverUrl}${RESET_PASSWORD_RESOURCE_URL}`,
      {
        email: email,
      },
      { headers: headers }
    );
  }

  private unsetSession(): void {
    this.storageService.removeExpiresAt();
    this.storageService.removeAccessToken();
    this.storageService.removeIdToken();
    this.storageService.removeFreelancerId();
    this.storageService.removeUserId();
    this.storageService.removeRefreshToken();
  }

  private setSession(authResult: AuthResult): void {
    const decodedToken = this.jwtHelper.decodeToken(authResult.id_token);
    const expiresAt = authResult.expires_in * 1000 + new Date().getTime();

    this.storageService.setExpiresAt(expiresAt);
    this.storageService.setAccessToken(authResult.access_token);
    this.storageService.setIdToken(authResult.id_token);
    this.storageService.setFreelancerId(decodedToken[FREELANCER_ID_PROPERTY]);
    this.storageService.setUserId(decodedToken[USER_ID_PROPERTY]);

    if (!!authResult.refresh_token) {
      // !!!! - isNil
      this.storageService.setRefreshToken(authResult.refresh_token);
    }
  }

  public getUserMetadata(): any {
    const idToken = this.storageService.getIdToken();
    const decodedToken = this.jwtHelper.decodeToken(idToken);
    const localStorageInfo = JSON.parse(localStorage.getItem(LocalStorage.REGISTRATION_USER_NAME));
    if (decodedToken[USER_METADATA_PROPERTY].onboardingLastName) {
      return decodedToken[USER_METADATA_PROPERTY];
    } else {
      if (localStorageInfo && decodedToken.email === localStorageInfo.email) {
        localStorage.removeItem(LocalStorage.REGISTRATION_USER_NAME);
        return {
          onboardingLastName: localStorageInfo.lastname,
          onboardingFirstName: localStorageInfo.firstname,
        };
      } else {
        return decodedToken[USER_ID_PROPERTY].includes('apple')
          ? this.parseNameAppleIDUser(decodedToken[USER_NAME])
          : {
              onboardingLastName: decodedToken.family_name,
              onboardingFirstName: decodedToken.given_name,
            };
      }
    }
  }

  private parseNameAppleIDUser(name: string): any {
    if (!name || name.trim() === '') {
      return {
        onboardingFirstName: undefined,
        onboardingLastName: undefined,
      };
    }
    const username: string[] = name.trim().replace(/[ ]+/g, ' ').split(' ');
    switch (username.length) {
      case 0:
        return {
          onboardingFirstName: undefined,
          onboardingLastName: undefined,
        };
      case 1:
        return {
          onboardingFirstName: username[0],
          onboardingLastName: undefined,
        };
      case 2:
        return {
          onboardingFirstName: username[0],
          onboardingLastName: username[1],
        };
      default:
        return {
          onboardingFirstName: username[0],
          onboardingLastName: username.slice(1).join(' '),
        };
    }
  }

  public getUserEmail(): any {
    const idToken = this.storageService.getIdToken();
    const decodedToken = this.jwtHelper.decodeToken(idToken);
    return decodedToken.email;
  }

  public async checkSession(): Promise<boolean> {
    const exp = await this.storageService.getExpiresAt();
    this.loggedIn = Date.now() < +exp;
    if (!this.loggedIn) {
      const token = await this.storageService.getAccessToken();
      if (token) {
        return await this.login(true).toPromise();
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  public onRedirect(url: string): void {
    AuthService.redirectSubject.next(this.parseAuthHash(url));
  }

  private parseAuthHash(responseUrl: string): any {
    const result = {};
    const [url, data] = responseUrl.split('#');
    if (url.match(environment.authConfig.domain)) {
      const params = new URLSearchParams(data);
      Array.from(params['entries']()).forEach((entry) => {
        result[entry[0]] = entry[1];
      });
      return {
        ...result,
        accessToken: result['access_token'],
        expiresIn: result['expires_in'],
      };
    }
    return null;
  }
}
