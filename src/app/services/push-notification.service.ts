import { JobDetailComponent } from './../modules/assignments/components/job-detail/job-detail.component';
import { TimesheetIncompleteParams } from './../modules/timesheets/models/timesheet-incomplete-params';
import { Injectable } from '@angular/core';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
  Capacitor,
} from '@capacitor/core';
import { environment } from 'environments/environment';
import { RegisterDeviceService } from './register-device.service';
import { ModalController, Platform } from '@ionic/angular';
import { EventsService } from './events.service';
import { ToastService } from './toast.service';
import { StorageService } from './storage.service';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { JobType } from 'app/interfaces/job-type';
import { JobDetailComponentParams } from 'app/interfaces/models/job-detail-component-params';
import { NotificationAction } from 'app/interfaces/models/notification-action';

const { PushNotifications } = Plugins;

@Injectable({ providedIn: 'root' })
export class PushNotificationService {
  private options = {
    android: {
      senderID: environment.googleConsoleAppId,
    },
    ios: {
      sound: true,
      alert: true,
    },
  };

  constructor(
    private modalController: ModalController,
    private toastService: ToastService,
    private events: EventsService,
    private storageService: StorageService
  ) {}

  public initialisePush(): void {
    if (Capacitor.platform !== 'web') {
      this.registerPush();
    }
  }

  private registerPush() {
    PushNotifications.requestPermission().then((permission) => {
      if (permission.granted) {
        console.log('Have permissions for pushes');
        PushNotifications.register();
      } else {
        console.log('No permissions for pushes!!!');
      }
    });

    PushNotifications.addListener('registration', (token: PushNotificationToken) => {
      console.log('My token: ' + JSON.stringify(token));
    });

    PushNotifications.addListener('registrationError', (error: any) => {
      console.log('Error: ' + JSON.stringify(error));
    });

    PushNotifications.addListener('pushNotificationReceived', async (notification: PushNotification) => {
      console.log('Push received: ' + JSON.stringify(notification));
    });

    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      async (notification: PushNotificationActionPerformed) => {
        const data = notification.notification.data;
        console.log('Action performed: ' + JSON.stringify(notification.notification));
        if (data.detailsId) {
          // this.router.navigateByUrl(`/home/${data.detailsId}`);
        }
      }
    );
  }

  //   public registerDevice(): void {
  //     PushNotifications.addListener('registration',
  //       (registrationEvent) => {

  // const deviceToken = this.storageService.getDeviceToken();
  // const registrationId = registrationEvent.value.
  //           if (!deviceToken) {
  //             return zip(this.registerDeviceWithNotificationService(registrationId), of(registrationId));
  //           } else {
  //             return zip(of(false), of(null));
  //           }

  //         mergeMap(([registrationSucceeded, registrationId]: [boolean, string]) => {
  //           if (registrationSucceeded) {
  //             return of(this.storageService.setDeviceToken(registrationId));
  //           } else {
  //             return of(false);
  //           }
  //         })
  //       )
  //       .subscribe();
  //   }

  // private registerDeviceWithNotificationService(deviceToken: string): Observable<boolean> {
  //   return this.registerDeviceService.register(
  //     new RegisterDeviceParams(this.storageService.getFreelancerId(), deviceToken, this.getCurrentPlatform())
  //   );
  // }

  // private getCurrentPlatform(): string {
  //   if (this.platform.is('android') || this.platform.is('ios')) {
  //     return this.platform.is('android') ? 'ANDROID' : 'IOS';
  //   }
  // }

  // public unregister(): Observable<string> {
  //   return zip(this.storageService.getFreelancerId(), this.storageService.getDeviceToken()).pipe(
  //     mergeMap(([freelancerId, registrationId]: [string, string]) => {
  //       this.registerDeviceService.unregister(new UnregisterDeviceParams(freelancerId, registrationId)).subscribe();
  //       this.storageService.removeDeviceToken();

  //       return of('success');
  //     })
  //   );
  // }

  // public addNotificationListener(): void {
  //   this.pushObject.on('notification').subscribe((notification) => {
  //     this.push.hasPermission().then((permission: { isEnabled: boolean }) => {
  //       if (permission.isEnabled) {
  //         this.handleNotification(notification);
  //       }
  //     });
  //   });
  // }

  public unregister(): void {
    PushNotifications.removeAllListeners();
  }

  private handleNotification(notification): void {
    const appIsOnForeground = notification.additionalData['foreground'];

    switch (notification.additionalData['gighouse_action']) {
      case NotificationAction.newJob:
        if (appIsOnForeground) {
          this.toastService.presentInformationToast('toast.new.job');
          this.events.publish(GighouseEvent.newJobNotificationReceived);
        } else {
          this.navigateToJobDetail(notification.additionalData['gighouse_job_id'], JobType.possibleJob);
        }
        break;

      case NotificationAction.mutualInterest:
        if (appIsOnForeground) {
          this.toastService.presentInformationToast('toast.mutual.interest');
          this.events.publish(GighouseEvent.mutualInterestNotificationReceived);
        } else {
          this.navigateToJobDetail(notification.additionalData['gighouse_job_id'], JobType.interestedJob);
        }
        break;

      case NotificationAction.timesheetIncomplete:
        if (appIsOnForeground) {
          this.toastService.presentInformationToast('toast.timesheet.incomplete');
        }
        const timesheetIncompleteParams = new TimesheetIncompleteParams(notification.additionalData['foreground']);
        this.events.publish(GighouseEvent.timesheetIncompleteNotificationReceived, timesheetIncompleteParams);
        break;
    }
  }

  async navigateToJobDetail(jobId: string, jobType: JobType) {
    if (jobId) {
      const jobDetailParams: JobDetailComponentParams = {
        jobId,
        jobType,
      };
      const modal = await this.modalController.create({
        component: JobDetailComponent,
        componentProps: {
          jobDetailParams,
        },
      });
      return await modal.present();
    }
  }
}
