import { ExpertiseParams } from './../interfaces/models/expertise';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { resources } from 'app/app.resources';
import { TranslatableError } from 'app/interfaces/models/translatable-error';
import { catchError } from 'rxjs/operators';
import { ProfileParams } from 'app/interfaces/models/profile';
import { LanguageSkillLevelParams } from 'app/interfaces/models/language-skill-level';
import { CompanyFormParams } from 'app/interfaces/models/company-form';

const PROFILE_METADATA_RESOURCE_URL = resources.profileMetadataResource;
const LANGUAGES_SKILL_LEVEL_RESOURCE_URL = resources.languageSkillLevelMetadataResource;
const EXPERTISE_METADATA_RESOURCE_URL = resources.expertiseMetadataResource;
const MAXIMUM_TRAVEL_TIMES_METADATA_RESOURCE_URL = resources.maximumTravelTimesMetadataResource;
const COMPANY_FORMS_METADATA_RESOURCE_URL = resources.companyFormsMetadataResource;

@Injectable({ providedIn: 'root' })
export class MetadataService {
  constructor(private http: HttpClient) {}

  public getExpertises(): Observable<ExpertiseParams[]> {
    return this.http
      .get<ExpertiseParams[]>(EXPERTISE_METADATA_RESOURCE_URL)
      .pipe(catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))));
  }

  public getProfiles(): Observable<ProfileParams[]> {
    return this.http
      .get<ProfileParams[]>(PROFILE_METADATA_RESOURCE_URL)
      .pipe(catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))));
  }

  public getLanguageSkillLevels(): Observable<LanguageSkillLevelParams[]> {
    return this.http
      .get<LanguageSkillLevelParams[]>(LANGUAGES_SKILL_LEVEL_RESOURCE_URL)
      .pipe(catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))));
  }

  public getMaximumTravelTimes(): Observable<string[]> {
    return this.http
      .get<string[]>(MAXIMUM_TRAVEL_TIMES_METADATA_RESOURCE_URL)
      .pipe(catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))));
  }

  getCompanyForms(): Observable<CompanyFormParams[]> {
    return this.http
      .get<CompanyFormParams[]>(COMPANY_FORMS_METADATA_RESOURCE_URL)
      .pipe(catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))));
  }
}
