import { GighouseEvent } from './../interfaces/models/gighouse-event';
import { EventsService } from './events.service';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CompanyForm, CompanyFormParams } from 'app/interfaces/models/company-form';
import { Expertise } from 'app/interfaces/models/expertise';
import { LanguageSkillLevel, LanguageSkillLevelParams } from 'app/interfaces/models/language-skill-level';
import { MaximumTravelTime } from 'app/interfaces/models/maximum-travel-time';
import { Profile } from 'app/interfaces/models/profile';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MetadataService } from './metadata.service';

const ONE_HOUR = 3600;
const ONE_DAY = 24 * ONE_HOUR;

@Injectable({ providedIn: 'root' })
export class CacheableMetadataService {
  constructor(private metadataService: MetadataService, private translateService: TranslateService) {}

  getExpertises(): Observable<Expertise[]> {
    const expertiseRequest = this.metadataService.getExpertises();
    return expertiseRequest.pipe(map((data) => data.map(({ label, value }) => new Expertise(value, label))));
  }

  private getProfiles(): Observable<Profile[]> {
    const profilesRequest = this.metadataService.getProfiles();
    return profilesRequest.pipe(
      map((data) => data.map(({ id, name, expertiseValue }) => new Profile(id, name, expertiseValue)))
    );
  }

  public getProfilesFor(expertiseValue: string): Observable<Profile[]> {
    console.log('VALUE: ', expertiseValue);

    return this.getProfiles().pipe(
      map((profiles: Profile[]) =>
        profiles.filter((profile: Profile) => profile.getExpertiseValue() === expertiseValue)
      )
    );
  }

  public getLanguageSkillLevels(): Observable<LanguageSkillLevel[]> {
    const languageSkillLevelsRequest$ = this.metadataService.getLanguageSkillLevels();
    return languageSkillLevelsRequest$.pipe(
      map((languageSkillLevels: LanguageSkillLevelParams[]) =>
        languageSkillLevels.map(
          (languageSkillLevel: LanguageSkillLevelParams) =>
            new LanguageSkillLevel(languageSkillLevel.value, languageSkillLevel.label)
        )
      )
    );
  }

  public getMaximumTravelTimes(): Observable<MaximumTravelTime[]> {
    const maximumTravelTimesRequest$ = this.metadataService.getMaximumTravelTimes();
    return maximumTravelTimesRequest$.pipe(
      map((maximumTravelTimeKeys: string[]) => {
        const maximumTravelTimes: MaximumTravelTime[] = [];
        maximumTravelTimeKeys.forEach((maximumTravelTimeKey: string) => {
          maximumTravelTimes.push(
            new MaximumTravelTime(maximumTravelTimeKey, this.translateService.instant(maximumTravelTimeKey))
          );
        });
        return maximumTravelTimes;
      })
    );
  }

  public getCompanyForms(): Observable<CompanyForm[]> {
    const companyFormsRequest$ = this.metadataService.getCompanyForms();
    return companyFormsRequest$.pipe(
      map((companyFormsParams: CompanyFormParams[]) =>
        companyFormsParams.map(
          (companyFormsParam: CompanyFormParams) => new CompanyForm(companyFormsParam.value, companyFormsParam.label)
        )
      )
    );
  }
}
