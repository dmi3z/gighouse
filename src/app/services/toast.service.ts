import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

import { TranslationsService } from './translations.service';

@Injectable()
export class ToastService {

  constructor(private toastController: ToastController,
    private translationsService: TranslationsService) {
  }

  async presentSuccessToast(message: string = 'toast.save.successful'): Promise<void> {
    const successToast = await this.toastController.create({
      message: this.translationsService._(message),
      duration: 3000,
      position: 'top',
      cssClass: 'success',
      buttons: [
        {
          side: 'start',
          icon: 'alert-circle',
          handler: () => {},
        }
      ]
    });
    successToast.present();
  }

  async presentInformationToast(message: string): Promise<void> {
    const informationToast = await this.toastController.create({
      message: this.translationsService._(message),
      duration: 3000,
      position: 'top',
      cssClass: 'information',
      buttons: [
        {
          side: 'start',
          icon: 'alert-circle',
          handler: () => {},
        }
      ]
    });
    informationToast.present();
  }

  async presentErrorToast(message: string): Promise<void> {
    const errorToast = await this.toastController.create({
      message: this.translationsService._(message),
      duration: 3000,
      position: 'top',
      cssClass: 'error',
      buttons: [
        {
          side: 'start',
          icon: 'alert-circle',
          handler: () => {},
        }
      ]
    });
    errorToast.present();
  }
}
