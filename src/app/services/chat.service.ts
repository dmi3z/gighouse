import { ConversationBody } from './../modules/chat-list/interfaces/chat.interface';
import { UserIdentifier } from '../modules/chat-list/interfaces/chat.interface';
import { from, Observable, forkJoin } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ChatItem } from '../modules/chat-list/interfaces/chat-item.interface';
import { resources } from 'app/app.resources';
import { StakeholdersItem } from '../modules/chat-list/interfaces/stakeholders-item.interface';
import Client from '@twilio/conversations';
import * as Twilio from '@twilio/conversations/dist/@twilio/conversations.min.js';
import { switchMap, filter, take, map, mergeMap } from 'rxjs/operators';
import { Paginator } from '@twilio/conversations/lib/interfaces/paginator';
import { Conversation } from '@twilio/conversations/lib/conversation';

const FREELANCER_RESOURCE_URL = resources.chatResourceBaseUrl;

@Injectable({ providedIn: 'root' })
export class ChatService {
  public twilioClient: Client;

  constructor(private http: HttpClient) {}

  public getChatList(freelancerId: string): Observable<ChatItem[]> {
    return this.http.get<ChatItem[]>(FREELANCER_RESOURCE_URL.concat(freelancerId, '/projects'));
  }

  public getStakeholdersList(freelancerId: string, projectId: string): Observable<StakeholdersItem[]> {
    return this.http
      .get<StakeholdersItem[]>(FREELANCER_RESOURCE_URL.concat(freelancerId, '/stakeholders/', projectId))
      .pipe(map((res) => res.map((item) => ({ ...item, freelancerId }))));
  }

  public getConversation(
    freelancerId: string,
    conversationBody: ConversationBody
  ): Observable<{ conversationSid: string }> {
    return this.http.post<{ conversationSid: string }>(
      FREELANCER_RESOURCE_URL.concat(freelancerId, '/conversation'),
      conversationBody
    );
  }

  public initClient(freelancerId: string): Observable<Paginator<Conversation>> {
    return this.getToken(freelancerId).pipe(
      take(1),
      switchMap((authData) => from(Twilio.Client.create(authData.token) as Promise<Client>)),
      switchMap((client) => {
        this.twilioClient = client;
        return from(client.getSubscribedConversations());
      }),
      filter((data) => data.items && data.items.length > 0)
    );
  }

  public getUnreadMessagesCount(): Observable<number> {
    return from(this.twilioClient.getSubscribedConversations()).pipe(
      take(1),
      mergeMap((conversations) =>
        forkJoin(conversations.items.map((conversation) => conversation.getUnreadMessagesCount()))
      ),
      map((unreadMessageCounters) => unreadMessageCounters.map((item) => (item ? item : 0))),
      map((result) => result.reduce((acc, item) => (acc += item)))
    );
  }

  private getToken(freelancerId: string): Observable<UserIdentifier> {
    return this.http.get<UserIdentifier>(FREELANCER_RESOURCE_URL.concat(freelancerId, '/access-token'));
  }
}
