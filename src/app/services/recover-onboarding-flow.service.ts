import { AccountState } from './../interfaces/models/account-state';
import { Injectable } from '@angular/core';

import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class RecoverOnboardingFlowService {
  constructor(private router: Router) {}

  public navigateToCorrectScreenInRegistrationFlow(state: AccountState): void {
    if (state === AccountState.apply) {
      this.router.navigate(['registration/registration-name', { isSocial: true }]);
    } else if (state === AccountState.complete) {
      this.router.navigate(['tabs']);
    } else if (state === AccountState.incomplete) {
      this.router.navigate(['tabs']);
    } else if (state === AccountState.known) {
      this.router.navigate(['tabs']);
    }
  }
}
