import { FreelancerBiography } from './../interfaces/models/freelancer-biography';
import { FreelancerDetails } from './../interfaces/models/freelancer-details';
import {Injectable} from '@angular/core';
import {Observable, of, ReplaySubject} from 'rxjs';
import { mergeMap, take} from 'rxjs/operators';
import { FreelancerService } from './freelancer.service';
import { StorageService } from './storage.service';
import { EventsService } from './events.service';
import { GighouseEvent } from 'app/interfaces/models/gighouse-event';
import { UpdateFreelancerRequest } from 'app/interfaces/models/update-freelancer';
import { UpdateBiographyRequest } from 'app/interfaces/models/update-biography';


@Injectable()
export class FreelancerRepository {
  protected _freelancerDetailsInitialized: boolean;
  protected _freelancerBiographyInitialized: boolean;
  protected _freelancerDetails$: ReplaySubject<FreelancerDetails> = new ReplaySubject(1);
  protected _freelancerBiography$: ReplaySubject<FreelancerBiography> = new ReplaySubject(1);

  constructor(private freelancerService: FreelancerService,
              private storageService: StorageService,
              private events: EventsService)
  {
    this.events.subscribe(GighouseEvent.freelancerLogout, () => this.clearData());
  }

  public saveFreelancerDetails(updateFreelancerRequest: UpdateFreelancerRequest): Observable<boolean> {
    return this.freelancerService.updateFreelancer(updateFreelancerRequest).pipe(
      mergeMap(() => {
        this.refreshFreelancerDetails();
        return of(true);
      })
    );
  }

  public saveBiography(freelancerId: string, biography: UpdateBiographyRequest): Observable<boolean> {
    return this.freelancerService.updateBiography(freelancerId, biography).pipe(
      mergeMap(() => {
        this.refreshFreelancerBiography();
        return of(true);
      })
    );
  }

  private refreshFreelancerDetails(): void {
    this.freelancerService.getFreelancerDetails(this.storageService.getFreelancerId())
      .pipe(take(1))
      .subscribe((freelancerDetails: FreelancerDetails) => {
        this._freelancerDetails$.next(freelancerDetails);
      });
  }

  public getFreelancerDetails(refresh?: boolean): Observable<FreelancerDetails> {
    if (refresh || !this._freelancerDetailsInitialized) {
      this._freelancerDetailsInitialized = true;
      this.refreshFreelancerDetails();
    }
    return this._freelancerDetails$.asObservable();
  }

  private refreshFreelancerBiography(): void {
    this.freelancerService.getFreelancerBiography(this.storageService.getFreelancerId())
      .pipe(take(1))
      .subscribe((freelancerBiography: FreelancerBiography) => {
        this._freelancerBiography$.next(freelancerBiography);
      });
  }

  public getBiography(refresh?: boolean): Observable<FreelancerBiography> {
    if (refresh || !this._freelancerBiographyInitialized) {
      this._freelancerBiographyInitialized = true;
      this.refreshFreelancerBiography();
    }
    return this._freelancerBiography$.asObservable();
  }

  private clearData(): void {
    this._freelancerDetailsInitialized = false;
    this._freelancerDetails$.next(null);
    this._freelancerBiographyInitialized = false;
    this._freelancerBiography$.next(null);
  }
}
