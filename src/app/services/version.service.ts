import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { TranslatableError } from '../interfaces/models/translatable-error';
import { resources } from '../app.resources';
import {
  AppVersionAllowed,
  AppVersionAllowedParams,
} from '../interfaces/models/app-version-allowed';

const APP_VERSION_ALLOWED_RESOURCE_URL = resources.appVersionAllowedResource;

@Injectable({ providedIn: 'root' })
export class VersionService {
  constructor(private http: HttpClient) {}

  checkIfAppIsAllowed(version: string): Observable<AppVersionAllowed | Error> {
    return this.http
      .get<AppVersionAllowedParams>(
        APP_VERSION_ALLOWED_RESOURCE_URL + '/' + version
      )
      .pipe(
        map(
          (appVersionAllowed) =>
            new AppVersionAllowed(appVersionAllowed.allowed)
        ),
        catchError((error) =>
          throwError(
            new TranslatableError('error.could.not.contact.server', error)
          )
        )
      );
  }
}
