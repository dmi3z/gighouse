import { Subject, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class LoadingRepository {
  private isLoading: boolean;

  constructor(private loadingController: LoadingController) {}

  public async presentLoading(): Promise<void> {
    this.isLoading = true;
    return await this.loadingController
      .create({
        message: 'Please wait...',
      })
      .then((ionLoading) => {
        ionLoading.present().then(() => {
          if (!this.isLoading) {
            ionLoading.dismiss();
          }
        });
      });
  }

  public async stopLoading(): Promise<void> {
    if (this.isLoading) {
      this.isLoading = false;
      await this.loadingController.dismiss();
    }
  }
}
