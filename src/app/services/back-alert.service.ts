import { AlertController, ModalController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { TranslationsService } from './translations.service';

@Injectable({ providedIn: 'root' })
export class BackAlertService {
  private alertIsOpen = false;

  constructor(
    private alertController: AlertController,
    private translationsService: TranslationsService,
    private modalController: ModalController
  ) {}

  public async showAlert(message?: string): Promise<boolean> {
    if (this.alertIsOpen) {
      return;
    }
    this.alertIsOpen = true;
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        header: this.translationsService._(message ? message : 'alert-service.confirm.cancel.title'),
        buttons: [
          {
            text: this.translationsService._('alert-service.confirm.yes'),
            handler: () => {
              this.alertIsOpen = false;
              this.modalController.dismiss();
              resolve(true);
            },
          },
          {
            text: this.translationsService._('alert-service.confirm.no'),
            handler: () => {
              this.alertIsOpen = false;
            },
          },
        ],
        cssClass: 'custom-alert',
      });
      alert.present();
    });
  }
}
