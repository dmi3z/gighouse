import { AccountStateService } from './account-state.service';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { RecoverOnboardingFlowService } from './recover-onboarding-flow.service';
import { Injectable } from '@angular/core';

@Injectable()
export class RedirectWelcomeGuard implements CanActivate {
  constructor(
    private storageService: StorageService,
    private authService: AuthService,
    private accountStateService: AccountStateService,
    private onboardFlow: RecoverOnboardingFlowService
  ) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    const token = this.storageService.getAccessToken();
    if (token) {
      this.authService.checkSession().then((succeeded) => {
        if (succeeded) {
          this.accountStateService.getState().subscribe((state) => {
            this.onboardFlow.navigateToCorrectScreenInRegistrationFlow(state);
            return false;
          });
        } else {
          return true;
        }
      });
    } else {
      return true;
    }
  }
}
