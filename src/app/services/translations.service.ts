import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { take } from 'rxjs/operators';
import * as moment from 'moment';
import { LocalStorage } from '../constants/local-storage';
import { TranslationsDataService } from './translations-data.service';
import { TranslationData } from '../interfaces/translations/translation-data.interface';
import { LocalTranslations } from '../interfaces/translations/local-translations.interface';

@Injectable({
  providedIn: 'root',
})
export class TranslationsService {
  public language: string;
  public localization: any = environment.localization;

  constructor(private translate: TranslateService, private translationsDataService: TranslationsDataService) {}

  /**
   * Sets up default language for the application. Uses browser default language.
   */
  public async setupLanguage(): Promise<void> {
    const languages: Array<string> = this.localization.languages.map((lang) => lang.code);
    const currentLanguage = localStorage.getItem(LocalStorage.Language);
    const translations = JSON.parse(localStorage.getItem(LocalStorage.TranslationsData));
    const version = localStorage.getItem(LocalStorage.TranslationsVersion);

    this.translate.addLangs(languages);

    if (currentLanguage) {
      await this.setLanguage(currentLanguage);
    } else {
      const browserLang: string = this.translate.getBrowserLang();
      const selectedLang = browserLang.match(/en|nl|fr/) ? browserLang : this.localization.defaultLanguage;
      await this.setLanguage(selectedLang);
    }
    if (!version || version === 'undefined') {
      localStorage.setItem(LocalStorage.TranslationsVersion, '1.0');
    }
    if (translations && translations.data) {
      this.setTranslations(translations);
    }
  }

  public async checkTranslationUpdate(): Promise<any> {
    return new Promise((resolve, reject) => {
      const currentVersion = localStorage.getItem(LocalStorage.TranslationsVersion);
      this.translationsDataService.getLastVersion().subscribe((latestVersion) => {
        if (this.compareVersions(latestVersion, currentVersion) > 0) {
          this.updateTranslations().then(() => {
            resolve(null);
          }, reject);
        } else {
          resolve(null);
        }
      }, reject);
    });
  }

  private compareVersions(firstValue: string, secondValue: string): number {
    if (firstValue === secondValue) {
      return 0;
    }
    const firstVersion = firstValue.split('.').map((value) => +value);
    const secondVersion = secondValue.split('.').map((value) => +value);
    if (firstVersion[0] === secondVersion[0]) {
      return firstVersion[1] > secondVersion[1] ? 1 : -1;
    } else {
      return firstVersion[0] > secondVersion[0] ? 1 : -1;
    }
  }

  public async updateTranslations(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.translationsDataService.updateTranslations().subscribe((translationData: TranslationData) => {
        const newData = { data: {}, version: '1.0' };
        translationData.translations.forEach((translation) => {
          Object.keys(translation.language).forEach((lang) => {
            newData.data[lang] = {
              ...newData.data[lang],
              [translation.key]: translation.language[lang],
            };
          });
        });
        if (translationData.version) {
          localStorage.setItem(LocalStorage.TranslationsVersion, translationData.version);
        }
        this.setTranslations(newData).then(() => {
          resolve(null);
        });
      }, reject);
    });
  }

  private async setTranslations(translations: LocalTranslations): Promise<void> {
    const { data } = translations;
    const oldLang = this.language;
    await Promise.all(
      Object.keys(data).map(async (lang: string) => {
        await this.setLanguage(lang);
        this.translate.setTranslation(lang, data[lang], true);
      })
    );
    this.setLanguage(oldLang);
    localStorage.setItem(LocalStorage.TranslationsData, JSON.stringify({ data }));
  }

  /**
   * Sets the language with the give language code
   * @param selectedLang string
   */
  public async setLanguage(selectedLang: string): Promise<any> {
    return new Promise((resolve, reject) => {
      localStorage.setItem(LocalStorage.Language, selectedLang);
      this.translate
        .use(selectedLang)
        .pipe(take(1))
        .subscribe(() => {
          this.language = selectedLang;
          moment.updateLocale(this.language, {
            calendar: {
              lastDay: `[${this.translate.instant('shared.yesterday')}]`,
              sameDay: 'HH:mm',
              nextDay: '[Tomorrow]',
              lastWeek: 'ddd',
              nextWeek: 'ddd',
              sameElse: 'DD MMM',
            },
          });
          moment.locale(this.language);
          resolve(null);
        });
    });
  }

  public getIso3Language(): string {
    const currentLanguage = this.language
      ? this.localization.languages.find((lang) => lang.code === this.language)
      : { iso3: 'NLD' };
    return currentLanguage.iso3;
  }

  public getIso3TestLanguage(): string {
    const language = this.language === 'en' ? 'nl' : this.language;
    const currentLanguage = this.language
      ? this.localization.languages.find((lang) => lang.code === language)
      : { iso3: 'NLD' };
    return currentLanguage.iso3;
  }

  public _(key: string): string {
    return this.translate.instant(key);
  }
}
