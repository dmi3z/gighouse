import { Injectable } from '@angular/core';
import { TranslatableError } from '../interfaces/models/translatable-error';
import { Job, JobParams, TeaserJobParams } from '../interfaces/models/job';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { resources } from '../app.resources';
import { JobDetail, JobDetailParams } from '../interfaces/models/job-detail';

const FREELANCER_RESOURCE_URL = resources.freelancerResourceBaseUrl;
const TEASER_JOBS_RESOURCE_POST_FIX = '/teaser-jobs';
const POSSIBLE_JOBS_RESOURCE_POST_FIX = '/possible-jobs';
const INTERESTED_JOBS_RESOURCE_POST_FIX = '/interested-jobs';
const JOBS_RESOURCE_URL = resources.jobResourceBaseUrl;

@Injectable()
export class JobService {
  constructor(private http: HttpClient) {}

  getTeaserJobsForFreelancer(freelancerId: string): Observable<Job[]> {
    return this.http
      .get<TeaserJobParams[]>(
        FREELANCER_RESOURCE_URL +
          '/' +
          freelancerId +
          TEASER_JOBS_RESOURCE_POST_FIX
      )
      .pipe(
        map((jobs) =>
          jobs.map(
            (job) =>
              new Job(
                null,
                job.name,
                job.location,
                job.fee,
                job.daysAWeek,
                job.startDate,
                job.teaserText1,
                job.teaserText2,
                job.teaserText3
              )
          )
        ),
        catchError((error) =>
          throwError(
            new TranslatableError('error.could.not.contact.server', error)
          )
        )
      );
  }

  getPossibleJobsForFreelancer(freelancerId: string): Observable<Job[]> {
    return this.http
      .get<JobParams[]>(
        FREELANCER_RESOURCE_URL +
          '/' +
          freelancerId +
          POSSIBLE_JOBS_RESOURCE_POST_FIX
      )
      .pipe(
        map((jobs) =>
          jobs.map(
            (job) =>
              new Job(
                job.id,
                job.name,
                job.location,
                job.fee,
                job.daysAWeek,
                job.startDate,
                job.teaserText1,
                job.teaserText2,
                job.teaserText3
              )
          )
        ),
        catchError((error) =>
          throwError(
            new TranslatableError('error.could.not.contact.server', error)
          )
        )
      );
  }

  getJobDetail(jobId: string): Observable<JobDetail> {
    return this.http.get<JobDetailParams>(JOBS_RESOURCE_URL + '/' + jobId).pipe(
      map((jobDetailParams) => JobDetail.from(jobDetailParams)),
      catchError((error) =>
        throwError(
          new TranslatableError('error.could.not.contact.server', error)
        )
      )
    );
  }

  getInterestedJobsForFreelancer(freelancerId: string): Observable<Job[]> {
    return this.http
      .get<JobParams[]>(
        FREELANCER_RESOURCE_URL +
          '/' +
          freelancerId +
          INTERESTED_JOBS_RESOURCE_POST_FIX
      )
      .pipe(
        map((jobs) =>
          jobs.map(
            (job) =>
              new Job(
                job.id,
                job.name,
                job.location,
                job.fee,
                job.daysAWeek,
                job.startDate,
                job.teaserText1,
                job.teaserText2,
                job.teaserText3
              )
          )
        ),
        catchError((error) =>
          throwError(
            new TranslatableError('error.could.not.contact.server', error)
          )
        )
      );
  }
}
