import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, share, tap } from 'rxjs/operators';
import { FreelancerDetails, FreelancerDetailsParams } from '../interfaces/models/freelancer-details';
import { TranslatableError } from '../interfaces/models/translatable-error';
import { resources } from '../app.resources';
import { FreelancerRegistrationParams } from '../interfaces/models/freelancer-registration-params';
import { FreelancerSkill, FreelancerSkillParams } from '../interfaces/models/freelancer-skill';
import { FreelancerLanguage, FreelancerLanguageParams } from '../interfaces/models/freelancer-language';
import { UpdateFreelancerRequest } from '../interfaces/models/update-freelancer';
import {
  AspiredFreelancerProfilesByExpertise,
  AspiredFreelancerProfilesByExpertiseParams,
} from '../interfaces/models/aspired-freelancer-profiles-by-expertise';
import { FreelancerSkillRating } from '../interfaces/models/freelancer-skill-rating';
import { Interest } from '../interfaces/models/interest';
import { ProfileCompleteness, ProfileCompletenessParams } from '../interfaces/models/profile-completeness';
import { OtherAspiredProfile, OtherAspiredProfileParams } from '../interfaces/models/other-aspired-profile';
// import {text} from '@angular/core/src/render3/instructions';
import { FreelancerBiography, FreelancerBiographyParams } from '../interfaces/models/freelancer-biography';
import { UpdateBiographyRequest } from '../interfaces/models/update-biography';

const FREELANCER_BASE_URL = resources.freelancerResourceBaseUrl;
const FREELANCER_SKILLS_RESOURCE_POST_FIX = '/skills';
const FREELANCER_LANGUAGES_RESOURCE_POST_FIX = '/languages';
const ASPIRED_FREELANCER_PROFILE_RESOURCE_POST_FIX = '/aspired-profiles';
const BIOGRAPHY_RESOURCE_POST_FIX = '/biography';

@Injectable({ providedIn: 'root' })
export class FreelancerService {
  constructor(private http: HttpClient) {}

  public getFreelancerDetails(freelancerId: string): Observable<FreelancerDetails> {
    console.log(freelancerId);

    return this.http.get<FreelancerDetailsParams>(FREELANCER_BASE_URL.concat('/', freelancerId)).pipe(
      map((freelancerDetailsParams) => {
        return FreelancerDetails.from(freelancerDetailsParams);
      }),
      catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error)))
    );
  }

  updateFreelancerDetails(freelancerDetails: FreelancerDetails): Observable<void> {
    return this.http
      .put<void>(FREELANCER_BASE_URL + '/' + freelancerDetails.freelancerId, freelancerDetails, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
      .pipe(
        share(),
        catchError((error: HttpErrorResponse) => throwError(this.checkIfErrorHasCustomTranslation(error)))
      );
  }

  updateFreelancer(updateFreelancerRequest: UpdateFreelancerRequest): Observable<void> {
    return this.http
      .put<void>(FREELANCER_BASE_URL + '/' + updateFreelancerRequest.freelancerId, updateFreelancerRequest, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
      .pipe(
        share(),
        catchError((error: HttpErrorResponse) => throwError(this.checkIfErrorHasCustomTranslation(error)))
      );
  }

  getFreelancerBiography(freelancerId: string): Observable<FreelancerBiography> {
    return this.http
      .get<FreelancerBiographyParams>(FREELANCER_BASE_URL + '/' + freelancerId + BIOGRAPHY_RESOURCE_POST_FIX)
      .pipe(
        map((freelancerBiographyParams) => FreelancerBiography.from(freelancerBiographyParams)),
        catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error)))
      );
  }

  updateBiography(freelancerId: string, biography: UpdateBiographyRequest): Observable<void> {
    return this.http
      .put<void>(FREELANCER_BASE_URL + '/' + freelancerId + BIOGRAPHY_RESOURCE_POST_FIX, biography, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
      .pipe(
        share(),
        catchError((error: HttpErrorResponse) => throwError(this.checkIfErrorHasCustomTranslation(error)))
      );
  }

  checkIfErrorHasCustomTranslation(error: HttpErrorResponse): TranslatableError {
    if (error && error.error && error.error.translationKey) {
      return new TranslatableError(error.error.translationKey, error);
    }
    return new TranslatableError('error.profile.could.not.be.updated', error);
  }

  register(freelancerRegistrationParams: FreelancerRegistrationParams): Observable<string> {
    return this.http
      .post<string>(FREELANCER_BASE_URL, freelancerRegistrationParams, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
      .pipe(
        catchError((error) => {
          return throwError(new TranslatableError('error.profile.could.not.be.updated', error));
        })
      );
  }

  getFreelancerSkills(freelancerId: string): Observable<FreelancerSkill[]> {
    return this.http
      .get<FreelancerSkillParams[]>(FREELANCER_BASE_URL + '/' + freelancerId + FREELANCER_SKILLS_RESOURCE_POST_FIX)
      .pipe(
        map((skills) =>
          skills.map(
            (skill) => new FreelancerSkill(skill.id, skill.description, skill.skills, skill.tools, skill.rating)
          )
        ),
        catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error)))
      );
  }

  updateFreelancerSkills(freelancerId: string, skillRatings: FreelancerSkillRating[]): Observable<void> {
    return this.http
      .put<void>(FREELANCER_BASE_URL + '/' + freelancerId + FREELANCER_SKILLS_RESOURCE_POST_FIX, skillRatings, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
      .pipe(catchError((error) => throwError(new TranslatableError('error.profile.could.not.be.updated', error))));
  }

  getFreelancerLanguages(freelancerId: string): Observable<FreelancerLanguage[]> {
    return this.http
      .get<FreelancerLanguageParams[]>(
        FREELANCER_BASE_URL + '/' + freelancerId + FREELANCER_LANGUAGES_RESOURCE_POST_FIX
      )
      .pipe(
        map((languages) =>
          languages.map(
            (language) =>
              new FreelancerLanguage(
                language.id,
                language.languageValue,
                language.languageLabel,
                language.languageSkillLevelValue
              )
          )
        ),
        catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error)))
      );
  }

  updateFreelancerLanguages(freelancerId: string, freelancerLanguages: FreelancerLanguage[]): Observable<void> {
    return this.http
      .put<void>(
        FREELANCER_BASE_URL + '/' + freelancerId + FREELANCER_LANGUAGES_RESOURCE_POST_FIX,
        freelancerLanguages,
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        }
      )
      .pipe(catchError((error) => throwError(new TranslatableError('error.profile.could.not.be.updated', error))));
  }

  getAspiredFreelancerProfiles(freelancerId: string): Observable<AspiredFreelancerProfilesByExpertise[]> {
    return this.http
      .get<AspiredFreelancerProfilesByExpertiseParams[]>(
        FREELANCER_BASE_URL + '/' + freelancerId + ASPIRED_FREELANCER_PROFILE_RESOURCE_POST_FIX
      )
      .pipe(
        map((aspiredFreelancerProfilesByExpertiseParams) =>
          aspiredFreelancerProfilesByExpertiseParams.map((aspiredFreelancerProfileTreeParam) =>
            AspiredFreelancerProfilesByExpertise.from(aspiredFreelancerProfileTreeParam)
          )
        ),
        catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error)))
      );
  }

  updateAspiredFreelancerProfiles(freelancerId: string, profileIds: string[]): Observable<void> {
    return this.http
      .put<void>(FREELANCER_BASE_URL + '/' + freelancerId + ASPIRED_FREELANCER_PROFILE_RESOURCE_POST_FIX, profileIds, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
      .pipe(catchError((error) => throwError(new TranslatableError('error.profile.could.not.be.updated', error))));
  }

  updateFreelancerJobInterest(freelancerId: string, jobId: string, interest: Interest): Observable<void> {
    return this.http
      .put<void>(FREELANCER_BASE_URL + '/' + freelancerId + '/jobs/' + jobId + '/interest', { interest: interest })
      .pipe(catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))));
  }

  getProfileCompleteness(freelancerId: string): Observable<ProfileCompleteness> {
    return this.http
      .get<ProfileCompletenessParams>(FREELANCER_BASE_URL + '/' + freelancerId + '/profile-complete')
      .pipe(
        map((profileCompletenessParams: ProfileCompletenessParams) =>
          ProfileCompleteness.from(profileCompletenessParams)
        ),
        catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error)))
      );
  }

  getOtherAspiredProfile(freelancerId: string): Observable<OtherAspiredProfile> {
    return this.http
      .get<OtherAspiredProfile>(
        FREELANCER_BASE_URL + '/' + freelancerId + ASPIRED_FREELANCER_PROFILE_RESOURCE_POST_FIX + '/other'
      )
      .pipe(
        map(
          (profileCompletenessParams: OtherAspiredProfileParams) =>
            new OtherAspiredProfile(profileCompletenessParams.other)
        ),
        catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error)))
      );
  }

  updateOtherAspiredProfile(freelancerId: string, other: string): Observable<void> {
    return this.http
      .put<void>(FREELANCER_BASE_URL + '/' + freelancerId + ASPIRED_FREELANCER_PROFILE_RESOURCE_POST_FIX + '/other', {
        other: other,
      })
      .pipe(catchError((error) => throwError(new TranslatableError('error.could.not.contact.server', error))));
  }
}
