import { HttpClient, HttpBackend } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslationData } from '../interfaces/translations/translation-data.interface';
import { map } from 'rxjs/operators';
import { UnifiedBackendApiResponse } from '../interfaces/unified-backend-api-response.interface';
import { TranslationApi } from '../interfaces/translations/translation-api.interface';
import { API_UNIFIED_BASE_URL } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TranslationsDataService {
  private http: HttpClient;
  constructor(handler: HttpBackend) {
    this.http = new HttpClient(handler);
  }

  public getLastVersion(): Observable<string> {
    return this.http
      .get<UnifiedBackendApiResponse<string>>(
        `${API_UNIFIED_BASE_URL}/translations/version`,
        {
          headers: {
            'X-Application': 'gighouse',
          },
        }
      )
      .pipe(map((response) => response.data));
  }

  public updateTranslations(): Observable<TranslationData> {
    return this.http
      .get<UnifiedBackendApiResponse<TranslationApi>>(
        `${API_UNIFIED_BASE_URL}/translations`,
        {
          headers: {
            'X-Application': 'gighouse',
          },
        }
      )
      .pipe(map((response) => response.data as any));
  }
}
