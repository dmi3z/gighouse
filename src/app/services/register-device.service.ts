import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { resources } from 'app/app.resources';
import { UnregisterDeviceParams } from 'app/interfaces/models/unregister-device-params';
import { RegisterDeviceParams } from 'app/interfaces/models/register-device-params';

const NOTIFICATION_BASE_URL = resources.notificationResourceBaseUrl;
const NOTIFICATION_RESOURCE_REGISTER_URL = NOTIFICATION_BASE_URL + '/register';
const NOTIFICATION_RESOURCE_UNREGISTER_URL = NOTIFICATION_BASE_URL + '/unregister';

@Injectable({ providedIn: 'root' })
export class RegisterDeviceService {
  constructor(private http: HttpClient) {}

  register(registerDeviceParams: RegisterDeviceParams): Observable<boolean> {
    return this.http
      .post<string>(NOTIFICATION_RESOURCE_REGISTER_URL, registerDeviceParams, {
        headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
        observe: 'response',
      })
      .pipe(
        flatMap((httpResponse: HttpResponse<string>) => {
          if (httpResponse.status === 200) {
            return of(true);
          } else {
            return of(false);
          }
        })
      );
  }

  unregister(unregisterDeviceParams: UnregisterDeviceParams): Observable<string> {
    return this.http.post<string>(NOTIFICATION_RESOURCE_UNREGISTER_URL, unregisterDeviceParams, {
      headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
    });
  }
}
