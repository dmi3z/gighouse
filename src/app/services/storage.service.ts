import { GighouseEvent } from './../interfaces/models/gighouse-event';
import { EventsService } from './events.service';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { Plugins } from '@capacitor/core';
// const { Storage } = Plugins;

const FREELANCER_ID = 'freelancer_id';
const ACCESS_TOKEN = 'access_token';
const ID_TOKEN = 'id_token';
const USER_ID = 'user_id';
const REFRESH_TOKEN = 'refresh_token';
const DEVICE_TOKEN = 'device_token';
const EXPIRES_AT = 'expires_at';

@Injectable({ providedIn: 'root' })
export class StorageService {
  constructor(private events: EventsService) {
    // events.subscribe(GighouseEvent.freelancerLogout, () => this.clearData()); !!!!
  }

  public getFreelancerId(): string {
    return localStorage.getItem(FREELANCER_ID);
  }

  public getAccessToken(): string {
    return localStorage.getItem(ACCESS_TOKEN);
  }

  public getIdToken(): string {
    return localStorage.getItem(ID_TOKEN);
  }

  public getUserId(): string {
    return localStorage.getItem(USER_ID);
  }

  public getExpiresAt(): number {
    return Number(localStorage.getItem(EXPIRES_AT));
  }

  public getRefreshToken(): string {
    return localStorage.getItem(REFRESH_TOKEN);
  }

  public getDeviceToken(): string {
    return localStorage.getItem(DEVICE_TOKEN);
  }

  public setRefreshToken(refreshToken: string): void {
    localStorage.setItem(REFRESH_TOKEN, refreshToken);
  }

  public setDeviceToken(deviceToken: string): void {
    localStorage.setItem(DEVICE_TOKEN, deviceToken);
  }

  public setUserId(userId: string): void {
    localStorage.setItem(USER_ID, userId);
  }

  public setIdToken(token: string): void {
    localStorage.setItem(ID_TOKEN, token);
  }

  public setExpiresAt(session: number): void {
    localStorage.setItem(EXPIRES_AT, session.toString());
  }

  public setFreelancerId(freelancerId: string): void {
    localStorage.setItem(FREELANCER_ID, freelancerId);
  }

  public setAccessToken(token: string): void {
    localStorage.setItem(ACCESS_TOKEN, token);
  }

  public removeFreelancerId(): void {
    localStorage.removeItem(FREELANCER_ID);
  }

  public removeRefreshToken() {
    localStorage.removeItem(REFRESH_TOKEN);
  }

  public removeExpiresAt() {
    localStorage.removeItem(EXPIRES_AT);
  }

  public removeDeviceToken() {
    localStorage.removeItem(DEVICE_TOKEN);
  }

  public removeUserId(): void {
    localStorage.removeItem(USER_ID);
  }

  public removeIdToken(): void {
    localStorage.removeItem(ID_TOKEN);
  }

  public removeAccessToken(): void {
    localStorage.removeItem(ACCESS_TOKEN);
  }
}
