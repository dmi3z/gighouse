import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { FreelancerDetails } from '../interfaces/models/freelancer-details';
import { map, take } from 'rxjs/operators';
import { FreelancerService } from './freelancer.service';
import { AccountState } from '../interfaces/models/account-state';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AccountStateService {
  constructor(
    private storageService: StorageService,
    private freelancerService: FreelancerService,
    private authService: AuthService
  ) {}

  public getState(): Observable<AccountState> {
    const freelancerId = this.storageService.getFreelancerId();
    const accessToken = this.storageService.getAccessToken();
    const userMetadata = this.authService.getUserMetadata();

    let freelancerDetails$ = of(null);
    const isHaveFreelancerId =
      freelancerId && freelancerId.length > 0 && freelancerId !== 'undefined';

    if (isHaveFreelancerId) {
      freelancerDetails$ = this.freelancerService.getFreelancerDetails(
        freelancerId
      );
    }

    return freelancerDetails$.pipe(
      take(1),
      map((freelancerDetails) => {
        if (!userMetadata.onboardingFirstName) {
          return AccountState.apply;
        } else if (this.userHasNoSalesForceAccount(accessToken, freelancerId)) {
          return AccountState.known;
        } else if (
          this.userHasNoTeaserJobCriteria(
            accessToken,
            freelancerId,
            freelancerDetails
          )
        ) {
          return AccountState.incomplete;
        } else {
          return AccountState.complete;
        }
      })
    );
  }

  private userHasNoSalesForceAccount(
    accessToken: string,
    freelancerId: string
  ): boolean {
    return !!accessToken && !freelancerId;
  }

  private userHasNoTeaserJobCriteria(
    accessToken: string,
    freelancerId: string,
    freelancerDetails: FreelancerDetails
  ): boolean {
    return (
      !!accessToken &&
      !!freelancerId &&
      this.teaserJobCriteriaAreNotPresent(freelancerDetails)
    );
  }

  private teaserJobCriteriaAreNotPresent(
    freelancerDetails: FreelancerDetails
  ): boolean {
    return (
      !freelancerDetails ||
      !freelancerDetails.jobPreferences ||
      !freelancerDetails.jobPreferences.maximumTravelTime ||
      !freelancerDetails.company ||
      !freelancerDetails.company.postalCode ||
      !freelancerDetails.profile ||
      !freelancerDetails.profile.getExpertise().getValue()
    );
  }
}
