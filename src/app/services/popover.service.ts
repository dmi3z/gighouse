import { Injectable } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { SelectorPopoverComponent } from 'app/modules/settings/selector-popover/selector-popover.component';

@Injectable({
  providedIn: 'root',
})
export class PopoverService {
  public popover: HTMLIonPopoverElement = null;

  constructor(private popoverController: PopoverController) {}

  async showSelectorPopover(data: any): Promise<any> {
    // const popover = await this.popoverController.create({
    //   component: SelectorPopoverComponent,
    //   cssClass: 'tests-popover',
    //   showBackdrop: true,
    //   // enableBackdropDismiss: options.backdropDismiss === undefined ? true : options.backdropDismiss,
    //   options,
    // });
    // this.popover = popover;
    // popover.present();
    // const result = new Promise((resolve) => {
    //   popover.onDidDismiss(
    //     //   (data) => {
    //     //   this.popover = null;
    //     //   resolve(data);
    //     // }
    //   );
    // });
    // return result;
    const popover = await this.popoverController.create({
      component: SelectorPopoverComponent,
      componentProps: {
        data,
      },
    });
    popover.present();

    // const result = new Promise((resolve) => {
    //     popover.onDidDismiss(
    //         (data) => {
    //         this.popover = null;
    //         resolve(data);
    //       }
    //     );
    //   });
    //   return result;

    return popover.onDidDismiss();

    // const { role } = await popover.onDidDismiss();
  }

  public hidePopover(): void {
    if (this.popover) {
      this.popover.dismiss();
      this.popover = null;
    }
  }
}
