import { Injectable } from '@angular/core';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class WebViewService {
  constructor(private safariViewController: SafariViewController) {}

  public closeWebView(): void {
    this.safariViewController.hide();
  }

  public openLink(
    url: string,
    hidden: boolean = false,
    color: string = '#f53d3d'
  ): Observable<boolean> {
    return new Observable<boolean>((observer: Observer<boolean>) => {
      this.safariViewController.isAvailable().then((available: boolean) => {
        if (available) {
          this.safariViewController
            .show({
              url,
              hidden,
              animated: false,
              transition: 'curl',
              enterReaderModeIfAvailable: true,
              tintColor: '#ffffff',
              barColor: color,
              controlTintColor: '#ffffff',
              toolbarColor: color,
            })
            .subscribe(
              (data) => {
                if (data.event === 'opened' || data.event === 'loaded') {
                  observer.next(true);
                  observer.complete();
                }
              },
              () => {
                observer.error(false);
              }
            );
        } else {
          console.log('You are on web');
        }
      });
    });
  }
}
