import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class HelperService {
  public objectIsEqual<T>(item_1: T, item_2: T): boolean {
    return Object.keys(item_1).some((key) => {
      return Object.getOwnPropertyNames(item_2).includes(key) ? item_1[key] != item_2[key] : false;
    });
  }

  public isEqual<T>(item_1: T, item_2: T): boolean {
    return JSON.stringify(item_1) === JSON.stringify(item_2);
  }

  // public areObjectsEqual<T>(OBJ_1: Array<T>, OBJ_2: Array<T>): boolean {
  //   return Object.entries(OBJ_1).
  // }

  public cloneDeepArray<T>(ARRAY_TO_COPY: Array<T>): Array<T> {
    return ARRAY_TO_COPY.map((item) => this.cloneDeepObject(item));
  }

  public cloneDeepObject<T>(OBJECT_TO_COPY: T): T {
    if (OBJECT_TO_COPY && typeof OBJECT_TO_COPY === 'object') {
      return Object.entries(OBJECT_TO_COPY).reduce((acc, [key, value]) => {
        acc[key] = Array.isArray(value)
          ? this.cloneDeepArray(value)
          : typeof value === 'object'
          ? this.cloneDeepObject(value)
          : value;
        return acc;
      }, {}) as T;
    }
    return OBJECT_TO_COPY;
  }
}
