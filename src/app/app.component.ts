import { LoadingRepository } from './services/loading.repository';
import { AuthService } from './services/auth.service';
import { TranslationsService } from './services/translations.service';
import { environment } from './../environments/environment';
import { ChangeDetectionStrategy, Component, NgZone } from '@angular/core';
import { Location, registerLocaleData } from '@angular/common';
import localeNlBE from '@angular/common/locales/nl-BE';
import { AppVersionAllowed } from './interfaces/models/app-version-allowed';
import { Platform, ModalController } from '@ionic/angular';
import { VersionService } from './services/version.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

import { Plugins, Capacitor } from '@capacitor/core';
import { Router } from '@angular/router';
const { SplashScreen } = Plugins;
const { App } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  private readonly disabledBackRoutes = ['registration-teaser-job', 'registration-phone', 'registration-info'];

  constructor(
    private platform: Platform,
    private authService: AuthService,
    private versionService: VersionService,
    private translationsService: TranslationsService,
    private ngZone: NgZone,
    private screenOrientation: ScreenOrientation,
    private router: Router,
    private location: Location,
    private loadingRepository: LoadingRepository,
    private modalController: ModalController
  ) {}

  public ngOnInit(): void {
    // SplashScreen.show();
    // if (environment.production) {
    //   Logger.enableProductionMode();
    // }
    // log.debug('init');

    // Setup locale
    SplashScreen.show();
    registerLocaleData(localeNlBE);

    this.versionService
      .checkIfAppIsAllowed(environment.appVersion)
      .subscribe((appVersionAllowed: AppVersionAllowed) => {
        if (appVersionAllowed.isAllowed()) {
          this.platform.ready().then(() => {
            this.lockScreenOrientation();
            this.translationsService.setupLanguage().then(() => {
              this.translationsService.checkTranslationUpdate();
            });
            App.addListener('appUrlOpen', ({ url }) => {
              this.ngZone.run(() => {
                if (url.includes('#') || url.includes('logout')) {
                  this.authService.onRedirect(url);
                } else {
                  console.log('2 way');
                  // Auth0Cordova.onRedirectUri(url);
                }
              });
            });
            this.disableBackButtonWhereNeeded();
          });
        } else {
          console.log('ELSE WAY APP');

          // this.rootPage = OutdatedAppPageComponent;
          // if (window['cordova']) {
          //   this.splashScreen.hide();
          // }
        }
      });
  }

  private disableBackButtonWhereNeeded(): void {
    this.platform.backButton.subscribeWithPriority(10, () => {
      const currentRoute = this.router.url;
      const isGoBackDisabled = this.disabledBackRoutes.some((item) => currentRoute.includes(item));
      if (isGoBackDisabled) {
        console.log('Can not go back here');
        return;
      } else {
        this.loadingRepository.stopLoading();
        this.location.back();
      }
    });
  }

  private lockScreenOrientation(): void {
    if (Capacitor.platform !== 'web') {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
  }
}
