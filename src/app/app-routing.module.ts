import { RedirectWelcomeGuard } from './services/redirect-from-welcome.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/welcome/welcome.module').then((m) => m.WelcomeModule),
    canActivate: [RedirectWelcomeGuard],
  },
  {
    path: 'tabs',
    loadChildren: () => import('./modules/tabs/tabs.module').then((m) => m.TabsPageModule),
  },
  {
    path: 'registration',
    loadChildren: () => import('./modules/registration/registration.module').then((m) => m.RegistrationModule),
  },
  {
    path: 'teaser-job-overview',
    loadChildren: () =>
      import('./modules/teaser-job-overview/teaser-job-overview.module').then((m) => m.TeaserJobOverviewModule),
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: [RedirectWelcomeGuard],
})
export class AppRoutingModule {}
