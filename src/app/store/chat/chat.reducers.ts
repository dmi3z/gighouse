import { createReducer, on, Action } from '@ngrx/store';
import { Conversation } from '@twilio/conversations/lib/conversation';
import { ConversationData } from 'app/interfaces/conversation-data.interface';
import { loadConversationsAction, updateUnreadMessagesCountAction } from './chat.actions';

export interface State {
  conversations: ConversationData[];
  totalUnreadCount: number;
}

const initialState: State = {
  conversations: [],
  totalUnreadCount: 0,
};

const chatReducer = createReducer(
  initialState,
  // on(addConversationAction, (state, action) => ({
  //   ...state,
  //   conversations: [...state.conversations, action.conversation],
  // })),
  on(loadConversationsAction, (state, action) => {
    return {
      ...state,
      conversations: action.conversations,
    };
  }),
  on(updateUnreadMessagesCountAction, (state, action) => {
    return {
      ...state,
      totalUnreadCount: action.counter,
    };
  })
);

export function reducer(state: State = initialState, action: Action): State {
  return chatReducer(state, action);
}
