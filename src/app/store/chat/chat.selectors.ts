import { ConversationData } from './../../interfaces/conversation-data.interface';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { ChatActions } from './chat.actions';
import * as fromChat from './chat.reducers';

export interface AppState {
  conversations: fromChat.State;
}

export const getChatState = createFeatureSelector<fromChat.State>('conversations');
// export const getConversations = () => createSelector(getChatState, (res): Conversation[] => res.conversations);
// export const getConversations = () => createSelector(getChatState, (res): ConversationData[] => res.conversations);

export const getConversationsByProjectId = (projectId: string) =>
  createSelector(getChatState, (res): ConversationData[] =>
    res.conversations.filter((item) => (item.conversation.attributes as any).projectId === projectId)
  );

export const getTotalUnreadMessages = () => createSelector(getChatState, (res): number => res.totalUnreadCount);

export const appReducers: ActionReducerMap<AppState, ChatActions> = {
  conversations: fromChat.reducer,
};
