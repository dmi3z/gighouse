import { createAction, props, union } from '@ngrx/store';
import { Conversation } from '@twilio/conversations/lib/conversation';
import { ConversationData } from 'app/interfaces/conversation-data.interface';

export enum ChatActionsTypes {
  AddConversation = '[Chat] AddConversation',
  LoadConversations = '[Chat] LoadConversations',
  UpdateUnreadMessagesCount = '[Chat] UpdateUnreadMessagesCount',
}

export const addConversationAction = createAction(
  ChatActionsTypes.AddConversation,
  props<{ conversation: Conversation }>()
);
export const loadConversationsAction = createAction(
  ChatActionsTypes.LoadConversations,
  props<{ conversations: ConversationData[] }>()
);

export const updateUnreadMessagesCountAction = createAction(
  ChatActionsTypes.UpdateUnreadMessagesCount,
  props<{ counter: number }>()
);

const allActions = union({
  addConversationAction,
  loadConversationsAction,
  updateUnreadMessagesCountAction,
});

export type ChatActions = typeof allActions;
