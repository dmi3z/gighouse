import { ApiInterceptor } from './services/api.interceptor';
import { ToastService } from './services/toast.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { environment } from './../environments/environment';
import { JwtModule } from '@auth0/angular-jwt';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { appReducers } from './store/chat/chat.selectors';

export function tokenGetter() {
  const idToken = localStorage.getItem('id_token');
  return idToken;
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    IonicModule.forRoot(),
    AppRoutingModule,
    StoreModule.forRoot(appReducers, {
      runtimeChecks: {
        strictActionImmutability: false,
        strictStateImmutability: false,
      },
    }),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: [environment.serverDomain],
        disallowedRoutes: [new RegExp(environment.serverDomain + '/api/public.*')],
      },
    }),
  ],
  providers: [
    SafariViewController,
    ScreenOrientation,
    ToastService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
