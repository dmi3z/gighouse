import {DecimalPipe} from '@angular/common';

export class NumberUtils {
  static formatNumber(value: number, maxDecimals: number): string {
    const pipe = new DecimalPipe('nl-BE');
    return pipe.transform(value, '1.0' + '-' + maxDecimals).replace('.', '');
  }

  static parseToNumber(value: string): number {
    if (value && value.toString().length > 0) {
      const stringValue = value.toString();
      return parseFloat(stringValue.replace(',', '.'));
    }
    return null;
  }
}
