import { Pipe, PipeTransform } from '@angular/core';
import { NumberUtils } from './number-utils';

@Pipe({
  name: 'formatNumber'
})
export class FormatNumberPipe implements PipeTransform {

  transform(value: any, maxDecimals: number = 2): any {
    if (value && typeof value === 'number') {
      return NumberUtils.formatNumber(value, maxDecimals);
    }
    return value;
  }
}
