import { FormatNumberPipe } from './format-number.pipe';
import { CityAndCompanyNamePipe } from 'app/pipes/city-and-company-name.pipe';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AmountOrDashPipe } from './amount-or-dash.pipe';
import { ValueOrDashPipe } from './value-or-dash.pipe';

@NgModule({
  declarations: [AmountOrDashPipe, ValueOrDashPipe, CityAndCompanyNamePipe, FormatNumberPipe],
  imports: [CommonModule],
  exports: [AmountOrDashPipe, ValueOrDashPipe, CityAndCompanyNamePipe, FormatNumberPipe],
})
export class PipesModule {}
