import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'amountOrDash',
})
export class AmountOrDashPipe implements PipeTransform {
  transform(value: any): any {
    if (value && typeof value === 'number') {
      return '€ ' + value;
    }
    if (value && typeof value === 'string' && !isNaN(+value)) {
      return '€ ' + value;
    }
    if (
      (!this.isDate(value) && !!value) ||
      !!value ||
      (value && value.length === 0)
    ) {
      return '-';
    }
    return value;
  }

  private isDate(value: any) {
    return Object.prototype.toString.call(value) === '[object Date]';
  }
}
