import { Pipe, PipeTransform } from '@angular/core';
import { FreelancerCompany } from 'app/interfaces/models/freelancer-company';

const separator = ' ';

@Pipe({
  name: 'cityAndCompanyName'
})
export class CityAndCompanyNamePipe implements PipeTransform {

  transform(value: FreelancerCompany, args?: any): any {
    return this.cityAndCompanyAggregated(value);
  }

  private cityAndCompanyAggregated(company: FreelancerCompany): string {
    const noCompanyLabel = '';
    const companyLabel = this.createCompanyLabel(company);

    const aggregatedString = [company.city, '-', companyLabel];
    if (this.showOnlyCity(company, companyLabel)) {
      aggregatedString.pop();
      aggregatedString.pop();
    } else if (this.showOnlyCompanyLabel(company, companyLabel)) {
      aggregatedString.reverse();
      aggregatedString.pop();
      aggregatedString.pop();
    } else if (this.showNothing(company, companyLabel)) {
      return noCompanyLabel;
    }

    return aggregatedString.join(separator);
  }

  private createCompanyLabel(company: FreelancerCompany): string {
    const noCompanyFormLabel = '';
    const companyFormLabel = company && company.companyForm ? company.companyForm.getLabel() : noCompanyFormLabel;
    const combinedCompanyNameAndForm = [company.name, companyFormLabel];

    if (!company.name) { // isEmpty
      combinedCompanyNameAndForm.pop();
      combinedCompanyNameAndForm.pop();
    } else if (!companyFormLabel) {
      combinedCompanyNameAndForm.pop();
    }

    return combinedCompanyNameAndForm.join(separator);
  }

  private showOnlyCity(company: FreelancerCompany, companyLabel: string): boolean {
    return company.city && !companyLabel;
  }

  private showOnlyCompanyLabel(company: FreelancerCompany, companyLabel: string): boolean {
    return companyLabel && !company.city;
  }

  private showNothing(company: FreelancerCompany, companyLabel: string) {
    return !company.city && !companyLabel;
  }
}
