import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'valueOrDash',
})
export class ValueOrDashPipe implements PipeTransform {
  transform(value: any): any {
    if (value && value.length > 0) {
      return value;
    }
    return '-';
  }
}
