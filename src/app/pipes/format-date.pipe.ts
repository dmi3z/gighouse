import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'formatDate',
})
export class FormatDatePipe implements PipeTransform {
  public transform(value: number[]): string {
    if (value) {
      return moment(`${value[2]}/${value[1]}/${value[0]}`, 'DD/MM/YYYY').format('DD/MM/YY');
    }
    return '';
  }
}
