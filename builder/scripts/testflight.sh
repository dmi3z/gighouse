#!/bin/bash

echo "█ TestFlight release has been triggered..."

echo "█ Checking environment..."

ENV="prod"
NG_ENV="production"
if [[ $* == *--prod* ]]
then
    ENV="prod"
    NG_ENV="production"
elif [[ $* == *--dev* ]]
then
    ENV="dev"
    NG_ENV="develop"
elif [[ $* == *--test* ]]
then
    ENV="test"
    NG_ENV="test"
fi
echo "  $ENV environment found..."

echo "█ Reading config..."

BUILD_CONFIG=`cat builder/config.public.json`

if [[ $* == *--dev* ]]
then
    BUILD_CONFIG=`echo "$BUILD_CONFIG" | jq '.development'`
elif [[ $* == *--prod* ]]
then
    BUILD_CONFIG=`echo "$BUILD_CONFIG" | jq '.production'` 
elif [[ $* == *--test* ]]
then
    BUILD_CONFIG=`echo "$BUILD_CONFIG" | jq '.test'`
else
    echo "█ ERROR: No environment flag found. Add --dev, --test or --prod."
    exit 1
fi

BUILD_CONFIG_TESTFLIGHT=`echo "$BUILD_CONFIG" | jq '.ios["testflight"]'`
PROVISIONING_PROFILE=`echo "$BUILD_CONFIG_TESTFLIGHT" | jq '.["provisioning-profile"]' | tr -d '"'`
TEAM_ID=`echo "$BUILD_CONFIG_TESTFLIGHT" | jq '.["team-id"]' | tr -d '"'`

echo "█ Building and signing iOS App Store build..."

npm run build:$ENV

npx cordova build ios --release --device --prod --configuration="$NG_ENV" --no-interactive --packageType="app-store" --developmentTeam="$TEAM_ID" --codeSignIdentity="iPhone Distribution" --provisioningProfile="$PROVISIONING_PROFILE" --buildFlag="-UseModernBuildSystem=0" --buildFlag="CODE_SIGN_IDENTITY=iPhone Distribution" --buildFlag="DEVELOPMENT_TEAM=$TEAM_ID" --buildFlag="PROVISIONING_PROFILE_SPECIFIER=$PROVISIONING_PROFILE" --buildFlag="-sdk iphoneos"

IPA_PATH=$(find . -name '*.ipa')
echo "IPA_PATH: $IPA_PATH"

echo "█ Uploading the build to Apple..."

xcrun altool --upload-app -f "$IPA_PATH" -u mobile.support@houseofhr.com -p mmud-gtne-ncwn-wvll
