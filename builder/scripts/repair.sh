echo "█ Repair start"

NODE_MODULES=./node_modules
if [ -d "$NODE_MODULES" ]; then
    echo "█ Removing node_nodules folder ($NODE_MODULES)"
    rm -rf "$NODE_MODULES"
fi

echo "█ Removing package-lock.json ($NODE_MODULES)"
[ ! -e file ] || rm ./package-lock.json

PLATFORMS=./platforms
if [ -d "$PLATFORMS" ]; then
    echo "█ Removing platforms folder ($PLATFORMS)"
    rm -rf "$PLATFORMS"
fi

PLUGINS=./plugins
if [ -d "$PLUGINS" ]; then
    echo "█ Removing plugins folder ($PLUGINS)"
    rm -rf "$PLUGINS"
fi

echo "█ Installing packages"
npm i

echo "█ Addins platforms"
npx cordova platform add android@8
npx cordova platform add ios@5

echo "█ Cordova prepare"
npx cordova prepare
echo "█ Repair done"
