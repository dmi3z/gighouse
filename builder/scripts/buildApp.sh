#!/bin/bash

echo "█ Checking environment..."

ENV="prod"
NG_ENV="prod"
if [[ $* == *--prod* ]]; then
  ENV="prod"
  NG_ENV="prod"
elif [[ $* == *--dev* ]]; then
  ENV="dev"
  NG_ENV="dev"
elif [[ $* == *--test* ]]; then
  ENV="test"
  NG_ENV="test"
fi
echo "  $ENV environment found..."

echo "█ Reading config..."

TIMESTAMP=$(date "+%Y.%m.%d-%H.%M.%S")
BUILD_CONFIG=$(cat builder/config.public.json)
APP_NAME=$(echo "$BUILD_CONFIG" | jq '.["app-name"]' | tr -d '"')

if [[ $* == *--dev* ]]; then
  BUILD_CONFIG=$(echo "$BUILD_CONFIG" | jq '.development')
elif [[ $* == *--prod* ]]; then
  BUILD_CONFIG=$(echo "$BUILD_CONFIG" | jq '.production')
elif [[ $* == *--test* ]]; then
  BUILD_CONFIG=$(echo "$BUILD_CONFIG" | jq '.test')
else
  echo "█ ERROR: No environment flag found. Add --dev, --test or --prod."
  exit 1
fi

echo "█ Plugin voodoo..."

APP_ID=$(echo "$BUILD_CONFIG" | jq '.id' | tr -d '"')
AUTH0_HOST=$(echo "$BUILD_CONFIG" | jq '.["auth0-host"]' | tr -d '"')

echo "█ Changing config.xml file..."

# setting app ID

xmlstarlet edit --inplace -O -N x=http://www.w3.org/ns/widgets --update "x:widget/@id" --value "$APP_ID" config.xml

# setting build number

IOS_BUILD_NR=$(date "+%Y%m%d.%H%M")
ANDROID_BUILD_NR=$(date "+%Y%m%d")

# xmlstarlet edit --inplace -O -N x=http://www.w3.org/ns/widgets --update "x:widget/@ios-CFBundleVersion" --value "$IOS_BUILD_NR" config.xml
# xmlstarlet edit --inplace -O -N x=http://www.w3.org/ns/widgets --update "x:widget/@android-versionCode" --value "$ANDROID_BUILD_NR" config.xml

# setting status bar color

STATUSBAR_COLOR=$(echo "$BUILD_CONFIG" | jq '.["statusbar-color"]' | tr -d '"')
STATUSBAR_COLOR_LINE=$(cat config.xml | grep "StatusBarBackgroundColor")
STATUSBAR_COLOR_NEW="<preference name=\"StatusBarBackgroundColor\" value=\"$STATUSBAR_COLOR\"/>"
sed -i "s@$STATUSBAR_COLOR_LINE@$STATUSBAR_COLOR_NEW@g" config.xml

# echo "█ Picking correct Google Services config..."

# find ./platforms/android/ -type f -name 'project.properties' -exec sed -i '' s/play-services-analytics:11.0.1/play-services-analytics:10.+/ {} +
# find ./platforms/android/ -type f -name 'project.properties' -exec sed -i '' s/play-services-gcm:11.0.1/play-services-gcm:10.+/ {} +

echo "█ Picking correct environment icons..."

if [ -d "resources/env-icons/$ENV" ]; then
  cp -rf "resources/env-icons/$ENV/android/." "resources/android/"
  cp -rf "resources/env-icons/$ENV/ios/." "resources/ios/"
else
  echo "  No environment icons found, using cordova resource generator."
fi

echo "█ Removing old builds..."

rm -rf android
rm -rf ios
rm -rf www

npm run build:$ENV
npx cap sync

if [[ $* != *--ios* ]]; then
  npx cap add android

  ionic capacitor build android --configuration="$NG_ENV" --no-open --prod
  ionic capacitor copy android --configuration="$NG_ENV" --prod

  APP_VERSION_CODE_LINE=$(cat android/app/build.gradle | grep "versionCode")
  APP_VERSION_CODE_NEW="versionCode $ANDROID_BUILD_NR"
  sed -i "s@$APP_VERSION_CODE_LINE@$APP_VERSION_CODE_NEW@g" android/app/build.gradle

  APP_VERSION_LINE=$(cat android/app/build.gradle | grep "versionName")
  APP_VERSION_NEW="versionName \"1.0.16\""
  sed -i "s@$APP_VERSION_LINE@$APP_VERSION_NEW@g" android/app/build.gradle

  if [ -d "config/google-services" ]; then
    cp -f "config/google-services/ios/GoogleService-Info-$ENV.plist" "ios/app/" GoogleService-Info.plist
    cp -f "config/google-services/android/google-services-$ENV.json" "android/app/google-services.json"
  else
    echo "  No Google Services config files found. If you use them, they should be in config/google-services/[platform]/"
  fi

  cordova-res android --copy

  cd android
  ./gradlew assembleRelease

  cd ..

  cp -f builder/release-signing.properties android/app/build/outputs/apk/release/release-signing.properties
  cp -f builder/release-signing.keystore android/app/build/outputs/apk/release/release-signing.keystore

  cd android/app/build/outputs/apk/release &&
    jarsigner -keystore release-signing.keystore -storepass '0y0EK5g2YiieF9&LkPI9D@MEpI3drp' app-release-unsigned.apk gighouse-app &&
    zipalign 4 app-release-unsigned.apk app-release.apk

  if [[ $* == *--no-upload* ]]; then
    echo "█ Found --no-upload flag, skipping Android build upload."
  else
    echo "█ Calling release script for Android build..."
    sh ./builder/scripts/releaseAppNew.sh "$@" --android
  fi
fi

if [[ $* != *--android* ]]; then
  BUILD_CONFIG_IOS=$(echo "$BUILD_CONFIG" | jq '.ios["app-center"]')
  PROVISIONING_PROFILE=$(echo "$BUILD_CONFIG_IOS" | jq '.["provisioning-profile"]' | tr -d '"')
  TEAM_ID=$(echo "$BUILD_CONFIG_IOS" | jq '.["team-id"]' | tr -d '"')

  npx cap add ios

  PLIST_URL_NAME_LINE=$(cat ios/App/App/Info.plist | grep "com.getcapacitor.capacitor")
  PLIST_URL_NAME_NEW="<string>$APP_ID</string>"
  sed -i '' "s@$PLIST_URL_NAME_LINE@$PLIST_URL_NAME_NEW@g" ios/App/App/Info.plist

  PLIST_URL_SCHEME_LINE=$(cat ios/App/App/Info.plist | grep "<string>capacitor</string>")
  PLIST_URL_SCHEME_NEW="<string>$APP_ID</string>"
  sed -i '' "s@$PLIST_URL_SCHEME_LINE@$PLIST_URL_SCHEME_NEW@g" ios/App/App/Info.plist

  cordova-res ios --copy
  ionic capacitor build ios --configuration="$NG_ENV" --no-open --prod
  ionic capacitor copy ios --configuration="$NG_ENV" --prod

  # BUILD_CONFIG_IOS=$(echo "$BUILD_CONFIG" | jq '.ios["app-center"]')
  # PROVISIONING_PROFILE=$(echo "$BUILD_CONFIG_IOS" | jq '.["provisioning-profile"]' | tr -d '"')
  # TEAM_ID=$(echo "$BUILD_CONFIG_IOS" | jq '.["team-id"]' | tr -d '"')

  if [[ $* == *--no-upload* ]]; then
    echo "█ Found --no-upload flag, skipping iOS build upload."
  else
    echo "█ Calling release script for iOS build..."
    sh ./builder/scripts/releaseAppNew.sh "$@" --ios
  fi

  if [[ $* == *--testflight* ]]; then
    echo "█ Found --testflight flag, starting TestFlight build."
    sh ./builder/scripts/testflight.sh "$@"
  else
    echo "█ Skipping TestFlight release..."
  fi
fi
