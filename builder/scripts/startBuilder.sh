#!/bin/bash

# Usage: startBuilder.sh --[ios|android] --[prod|test|dev]
# Options: --no-code-checkout --no-commit-check --no-upload --no-release --testflight

echo "█ Checking environment..."

ENV="prod"
if [[ $* == *--prod* ]]
then
    ENV="prod"
elif [[ $* == *--test* ]]
then
    ENV="test"
elif [[ $* == *--dev* ]]
then
    ENV="dev"
fi

echo "  $ENV environment found..."

echo "█ Checking out 'develop' branch..."

if [[ $* == *--no-code-checkout* ]]
then
    echo "█ --no-code-checkout flag found, skipping git checkout and pull..."
else
    git reset --hard
    git clean -f
    git checkout develop
    git pull
fi

echo "█ Checking last commit..."

if [[ $* == *--no-commit-check* ]]
then
    echo "█ --no-commit-check flag found, skipping check..."
else
    LAST_BUILD_COMMIT=$(<./builder/logs/lastBuild-$ENV.log)
    CURRENT_COMMIT=`git log --format="%H" -n 1`

    echo "  Last build commit: $LAST_BUILD_COMMIT"
    echo "  Current commit: $CURRENT_COMMIT"

    if [ "$LAST_BUILD_COMMIT" == "$CURRENT_COMMIT" ]
    then
        echo "█ No new commits, skipping build"
        exit 0
    else
        echo "█ Found new commits, starting build..."
        echo "$CURRENT_COMMIT" > "./builder/logs/lastBuild-$ENV.log"
    fi
fi

echo "█ Installing packages..."

rm -rf node_modules/ > /dev/null 2>&1
npm i

echo "█ Preparing build script caller..."

if [[ $* == *--async-build* ]]
then
    echo "█ Creating temporary build folder..."

    BUILD_CONFIG=`cat builder/config.public.json`
    APP_NAME=`echo "$BUILD_CONFIG" | jq '.["app-name"]' | tr -d '"'`
    TIMESTAMP=`date "+%Y.%m.%d-%H.%M.%S"`
    BUILD_DIR="$APP_NAME-$TIMESTAMP"

    cp -rf "." "../_temp/$BUILD_DIR"
    echo "  Temp folder: $BUILD_DIR"

    echo "█ Calling build script..."

    cd "../_temp/$BUILD_DIR"
    bash ./builder/scripts/buildApp.sh "$@"

    echo "█ Cleaning up temporary build folder..."

    cd ..
    rm -rf "$BUILD_DIR"

else
    echo "█ Calling build script..."
    bash ./builder/scripts/buildApp.sh "$@"
fi

echo "█ All done."
