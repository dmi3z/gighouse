#!/bin/bash

echo "█ Checking environment..."

ENV="prod"
if [[ $* == *--prod* ]]
then
    ENV="prod"
elif [[ $* == *--dev* ]]
then
    ENV="dev"
elif [[ $* == *--test* ]]
then
    ENV="test"
fi
echo "  $ENV environment found..."

echo "█ Reading config..."

TIMESTAMP=`date "+%Y.%m.%d-%H.%M.%S"`
BUILD_CONFIG=`cat builder/config.public.json`
APP_NAME=`echo "$BUILD_CONFIG" | jq '.["app-name"]' | tr -d '"'`

if [[ $* == *--dev* ]]
then
    BUILD_CONFIG=`echo "$BUILD_CONFIG" | jq '.development'`
elif [[ $* == *--test* ]]
then
    BUILD_CONFIG=`echo "$BUILD_CONFIG" | jq '.test'`
elif [[ $* == *--prod* ]]
then
    BUILD_CONFIG=`echo "$BUILD_CONFIG" | jq '.production'`
else
    echo "█ ERROR: No environment flag found. Add --dev, --test or --prod."
    exit 1
fi

BUILD_CONFIG_PRIVATE=`cat builder/config.private.json`
AC_API_TOKEN=`echo "$BUILD_CONFIG_PRIVATE" | jq '.["app-center"]["api-token"]' | tr -d '"'`

echo "█ Starting app uploads..."

if [[ $* != *--ios* ]]
then
    echo "█ Uploading Android app to AppCenter..."

    BUILD_CONFIG_ANDROID=`echo "$BUILD_CONFIG" | jq '.android["app-center"]'`
    AC_CORP=`echo "$BUILD_CONFIG_ANDROID" | jq '.user' | tr -d '"'`
    AC_APP=`echo "$BUILD_CONFIG_ANDROID" | jq '.app' | tr -d '"'`
    AC_RELEASE_GROUP=`echo "$BUILD_CONFIG_ANDROID" | jq '.["release-group"]' | tr -d '"'`
    AC_RELEASE_NOTES=`echo "$BUILD_CONFIG_ANDROID" | jq '.["release-notes"]' | tr -d '"'`", Env: $ENV, Timestamp: $TIMESTAMP"

    UPLOAD_DATA=`curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'X-API-Token: '$AC_API_TOKEN 'https://api.appcenter.ms/v0.1/apps/'$AC_CORP'/'$AC_APP'/release_uploads'`

    UPLOAD_URL=`echo $UPLOAD_DATA |  jq .upload_url | tr -d '"'`
    UPLOAD_ID=`echo $UPLOAD_DATA |  jq .upload_id | tr -d '"'`

    echo "Upload URL: $UPLOAD_URL"
    echo "Upload ID: $UPLOAD_ID"

    curl -F "ipa=@android/app/build/outputs/apk/release/app-release.apk" --header 'Content-Type: multipart/form-data' $UPLOAD_URL

    echo "Finished upload app package call"

    RELEASE_DATA=`curl -X PATCH --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'X-API-Token: '$AC_API_TOKEN -d '{ "status": "committed"  }' 'https://api.appcenter.ms/v0.1/apps/'$AC_CORP'/'$AC_APP'/release_uploads/'$UPLOAD_ID`

    echo "Release response: $RELEASE_DATA"

    RELEASE_ID=`echo $RELEASE_DATA | jq .release_id | tr -d '"'`

    echo "Release ID: $RELEASE_ID"

    if [[ $* == *--no-release* ]]
    then
        echo "█ Found --no-release flag, skipping AppCenter release..."
    else
        curl -X PATCH --header 'Content-Type: application/json' --header 'Accept: application/json' --header "X-API-Token: $AC_API_TOKEN" -d "{ \"destination_name\": \"$AC_RELEASE_GROUP\", \"release_notes\": \"$AC_RELEASE_NOTES\" }" 'https://api.appcenter.ms/v0.1/apps/'$AC_CORP'/'$AC_APP'/releases/'$RELEASE_ID
        echo "\n"
    fi
fi

if [[ $* != *--android* ]]
then
    echo "█ Uploading iOS app to AppCenter..."

    BUILD_CONFIG_IOS=`echo "$BUILD_CONFIG" | jq '.ios["app-center"]'`
    AC_CORP=`echo "$BUILD_CONFIG_IOS" | jq '.user' | tr -d '"'`
    AC_APP=`echo "$BUILD_CONFIG_IOS" | jq '.app' | tr -d '"'`
    AC_RELEASE_GROUP=`echo "$BUILD_CONFIG_IOS" | jq '.["release-group"]' | tr -d '"'`
    AC_RELEASE_NOTES=`echo "$BUILD_CONFIG_IOS" | jq '.["release-notes"]' | tr -d '"'`", Env: $ENV, Timestamp: $TIMESTAMP"

    echo "------------------"
    echo $AC_APP
    echo $BUILD_CONFIG_IOS
    echo $AC_APP
    echo $AC_RELEASE_GROUP
    echo $AC_RELEASE_NOTES
    echo "------------------"

    cd ios/App/App

    UPLOAD_DATA=`curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'X-API-Token: '$AC_API_TOKEN 'https://api.appcenter.ms/v0.1/apps/'$AC_CORP'/'$AC_APP'/release_uploads'`

    UPLOAD_URL=`echo $UPLOAD_DATA |  jq .upload_url | tr -d '"'`
    UPLOAD_ID=`echo $UPLOAD_DATA |  jq .upload_id | tr -d '"'`

    echo "Upload URL: $UPLOAD_URL"
    echo "Upload ID: $UPLOAD_ID"

    IPA_PATH=$(find . -name '*.ipa')

    echo "IPA_PATH: $IPA_PATH"

    curl -F "ipa=@$IPA_PATH" --header 'Content-Type: multipart/form-data' $UPLOAD_URL

    echo "Finished upload app package call"

    RELEASE_DATA=`curl -X PATCH --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'X-API-Token: '$AC_API_TOKEN -d '{ "status": "committed"  }' 'https://api.appcenter.ms/v0.1/apps/'$AC_CORP'/'$AC_APP'/release_uploads/'$UPLOAD_ID`

    echo "Release response: $RELEASE_DATA"

    RELEASE_ID=`echo $RELEASE_DATA | jq .release_id | tr -d '"'`

    echo "Release ID: $RELEASE_ID"

    if [[ $* == *--no-release* ]]
    then
        echo "█ Found --no-release flag, skipping AppCenter release..."
    else
        curl -X PATCH --header 'Content-Type: application/json' --header 'Accept: application/json' --header "X-API-Token: $AC_API_TOKEN" -d "{ \"destination_name\": \"$AC_RELEASE_GROUP\", \"release_notes\": \"$AC_RELEASE_NOTES\" }" 'https://api.appcenter.ms/v0.1/apps/'$AC_CORP'/'$AC_APP'/releases/'$RELEASE_ID
        echo "\n"
    fi
fi

echo "█ Finished the upload process."
